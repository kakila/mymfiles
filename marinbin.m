## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2013 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2013-04-13

## -*- texinfo -*-
## @deftypefn {Function File} {@var{c} =} marinbin (@var{nmar}, @var{nbin})
## Returns the position of @var{nmar} in @var{nbin}, allowing the marbles to be in the same bin.
##
## @end deftypefn


function conf = marinbin (nmar,nbin)
  %% This is standard stars and bars.
  numsymbols = nbin+nmar-1;
  stars = nchoosek (1:numsymbols, nmar);

  %% Star labels minus their consecutive position becomes their index
  %% position!
  idx = bsxfun (@minus, stars, [0:nmar-1]);

  %% Manipulate indices into the proper shape for accumarray.
  nr = size (idx, 1);
  a = repmat ([1:nr], nmar, 1);
  b = idx';
  conf = [a(:), b(:)];
  conf = accumarray (conf, 1);

endfunction

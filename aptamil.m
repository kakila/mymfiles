## Copyright (C) 2015 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>


## -*- texinfo -*-
## @deftypefn {Function File} {[@var{spoons} @var{water}] =} aptamil (@var{total})
## Calculates the @var{spoons} of aptamil and @var{water} needed to obtain a
## @var{total} ml of product.
## @end deftypefn

function [s w] = aptamil (ml)

  spoons  = [0 3:7];
  water   = [0 90:30:210]; #ml
  fertig  = [0 100 130 170 200 230]; # ml

  ppw = interp1 (fertig, water, "pp","extrap");
  pps = interp1 (fertig, spoons, "pp","extrap");

  s = ppval (pps, ml);
  w = ppval (ppw, ml);
endfunction

## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

function [N] =magCoilEstimator(F,x,I,param)
%% function [N] =magCoilEstimator(F,x,I,param)
%
%

coilDim = param{3}; % Diameter and length of coil in meters
M = param{4}; % Magnetic dipole of fin permanent magnet in Tesla

Area = (pi * (0.5 * coilDim(1))^2 );

mu0_fact= 6e-7; % 3mu0/(2pi)

fx = 1./(x-x1)^4 + 1./(x-x2)^4; % function of the distance

denom = mu0_fact* M * I * Area * fx;
N= F/denom; 

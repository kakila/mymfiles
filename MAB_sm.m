## Copyright (C) 2015 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>


function tf = MAB_sm(reward, tau, seed)
  N = size (reward,1);
  rand("state", seed);

  ## Policy
  function tf = next (q,b)
    tf = false (2);
    s = exp (b * q);
    s /= sum(s);
    while !any(tf)
      tf = rand() < s;
    endwhile
  endfunction

  ## Decissions
  tf = false (N, 2);

  ## Accumulated changes
  Q = q = [0,0];

  for i=1:N

    ## Chosen bandit based on current state
    if (i > 1)
      nk = sum (tf(1:i,:));
      nonz = nk != 0;
      q = Q;
      q(nonz) = Q(nonz) ./ nk(nonz);
    endif
    beta = tau * i;
    tf(i,:) = next(q, beta);
    if (all (tf(i,:))) # choose only one
      tf(i, randi (2)) = false;
    endif

    ## Update rule
    Q(tf(i,:)) += reward(i,tf(i,:));

  endfor

endfunction

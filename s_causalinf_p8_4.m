## Elements of causal inference
# Solution to problem 8.4 in
#
#    Peters, J., Janzing, D., & Schölkopf, B. (2017).
#    Elements of causal inference: foundations and learning algorithms.
#   Cambridge, MA, USA: MIT Press.
##

# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-07-31

## Item a
# $$ \var(Z) = \mathbb{E}\left[Z^2\right] - \mathbb{E}[Z]^2 $$
# $$ \mathbb{E}[Y] = \mathbb{E}\left[N_Y\right] + \mathbb{E}\left[Z^2\right] = \var(Z) + \mathbb{E}[Z]^2 = 1 $$
# $$ \tilde{\mathbb{E}}[Y] = \mathbb{E}\left[N_Y\right] + \tilde{\mathbb{E}}\left[Z^2\right] = \var(Z) + \tilde{\mathbb{E}}[Z]^2 = 1 + 4 = 5 $$
#
mtrue = 1;
mtrue_ = 5;

## Item b
# The _scm_ functions returns samples from the SCM.
# It takes as arguments the mean and the varaince of the noise terms, as well as
# the sample size.
#
# The _htmean_ function implements the Horvitz-Thompson estimator.
#
pkg load statistics
function [Z Y] = scm (mu, var, n)
  Z = normrnd (mu(1), sqrt (var(1)), [n, 1]);
  Y = Z.^2 + normrnd (mu(2), sqrt (var(2)), [n, 1]);
endfunction

function [m w] = htmean (Y, Z, mu, var)
  # ratio pdf_intervention / pdf
  w = normpdf (Z, mu(2), var(2)) ./ normpdf (Z, mu(1), var(1));
  m = mean (Y .* w);
endfunction

N    = 200;
muZ  = [0 2];  # base, intervetion
varZ = [1 1];  # base, intervetion
muY  = 0;
varY = 1;

##
# Sample the given SCM
#
[Z Y]    = scm ([muZ(1) muY], [varZ(1) varY], N);
Ymean    = mean (Y);
htYmean_ = htmean (Y, Z, muZ, varZ);

##
# Sample the intervention SCM
#
[Z_ Y_] = scm ([muZ(2) muY], [varZ(2) varY], N);
Ymean_  = mean (Y_);

printf ('Means: %d base, %d intervention\n', mtrue, mtrue_);
printf ('Mean intervention data (%d): %.2f\n', N, Ymean_);
printf ('Mean intervention HT estimator (%d): %.2f\n', N, htYmean_);
fflush (stdout);

figure (1), clf
  hold on
  h    = plot (Z, Y, 'o', Z_, Y_, 'o');
  zmin = min([Z;Z_]); zmax = max([Z;Z_]);

  hmtrue = line ([zmin zmax], mtrue_, 'color', get (h(2), 'color'));
  hmY    = line ([zmin zmax], Ymean_);
  hmY_   = line ([zmin zmax], htYmean_, 'linestyle', '--');
  axis tight
  xlabel ('Z');
  ylabel ('Y');
  legend ([h; hmtrue; hmY; hmY_], ...
    {'data','intervetion data', ...
    'true mean', 'intervetion mean', 'HT estimator'});

## Item c
# Stuy the varaince of the HT weights
#
nN   = 10;
N    = round (logspace (log10 (5), log10 (5e5), nN));
varw = zeros (nN, 1);
ht   = zeros (nN, 2);
for i = 1:nN
  [Z Y]   = scm ([muZ(1) muY], [varZ(1) varY], N(i));
  [~, Y_] = scm ([muZ(2) muY], [varZ(2) varY], N(i));
  [m, w]  = htmean (Y, Z, muZ, varZ);
  varw(i) = var (w);
  ht(i, :) = [mean(Y_), m];
endfor

printf ('Means: %d base, %d intervention\n', mtrue, mtrue_);
printf ('Mean intervention data (%d): %.2f\n', N(i), ht(i,1));
printf ('Mean intervention HT estimator (%d): %.2f\n', N(i), ht(i, 2));
fflush (stdout);

figure (2)
  subplot (2,1,1)
  h = semilogx (N, ht, 'o-');
  ylabel ('intervetnion mean')
  axis tight
  v = axis ();
  hl = line (v(1:2), mtrue_);
  legend ([h; hl], {'sample', 'HT estimator', 'True'}, ...
    'location', 'northoutside', 'orientation', 'horizontal');

  subplot (2,1,2)
  semilogx (N, varw, 'o-');
  ylabel ('variance of HT weights')
  xlabel ('Sample size')
  axis tight


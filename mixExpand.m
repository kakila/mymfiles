## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## Test mixed taylor expansion of
## y(t) = N(x(t)) when x(t) is known but y(t) is only data.

t = linspace (0,1,1024).';
x = @(t) sin (2*pi*5*t)/2/pi/5;
dxdt = @(t) cos (2*pi*5*t);

# Fixed relation
b = 2*rand(1,10)-1;
w = randn (10,1);
N = @(x) ((tanh (b.*x) + 1)*w/2).^2;
y = N(x(t));
ppN = interp1 (x(t),y,"pp");
dNdx =  @(x0,n) ppval (ppder(ppN,n), x0);

idx0 = 256;
yh = N(x(t(idx0))) + dNdx(x(t(idx0)),1)*(x(t)-x(t(idx0))) + dNdx(x(t(idx0)),2)*(dxdt(t)-dxdt(t(idx0)));

h = plot(t,y,'-k',t,yh,':g');
set (h,"linewidth",2);




## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {@var{} =} goldseqgen (@var{}, @var{})
## 
## @seealso{}
## @end deftypefn

function s = goldseqgen (varargin)

  # --- Parse arguments --- #
  parser = inputParser ();
  parser.FunctionName = "goldseqgen";
  parser = addParamValue (parser,'FirstPolynomial', [5 2 0], @ismatrix);
  parser = addParamValue (parser,'FirstInitialConditions', [zeros(1,4) 1], @ismatrix);
  parser = addParamValue (parser,'SecondPolynomial', [5 4 3 2 0], @ismatrix);
  parser = addParamValue (parser,'SecondInitialConditions', [zeros(1,3) 1 0], @ismatrix);
  parser = parse(parser,varargin{:});

  P1     = parser.Results.FirstPolynomial;
  P2     = parser.Results.SecondPolynomial;
  IC1    = parser.Results.FirstInitialConditions;
  IC2    = parser.Results.SecondInitialConditions;

  clear parser
  # ------ #
  
  m = P1(1);
  n = 2^m - 1;
  if mod (n,4) == 0
    error ("The length of the gold code should not be multiple of 4.");
  endif

  # preferred polynomials
  s1 = mls (n,P1,IC1);
  s2 = mls (n,P2,IC2);
  s = double (xor (s1, s2));

endfunction

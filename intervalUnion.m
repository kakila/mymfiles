## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

%% Copyright (c) 2011 Juan Pablo Carbajal <carbajal@ifi.uzh.ch>
%% 
%%    This program is free software: you can redistribute it and/or modify
%%    it under the terms of the GNU General Public License as published by
%%    the Free Software Foundation, either version 3 of the License, or
%%    any later version.
%%
%%    This program is distributed in the hope that it will be useful,
%%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%    GNU General Public License for more details.
%%
%%    You should have received a copy of the GNU General Public License
%%    along with this program. If not, see <http://www.gnu.org/licenses/>.

%% -*- texinfo -*-
%% @deftypefn {Function File} @var{u} = intervalUnion (@var{I})
%% Union of the real intervals given in the rows of I.
%%
%% @seealso{}
%% @end deftypefn

function u = intervalUnion(I)
  j=2;
  k=1;
  sw=0;
  nI = size(I,1);       
  
  %% Only one interval
  if nI==1
   u=I;
   return
  end

  while true
      I1 = I(k,:);
      I2 = I(j,:);
      
      %% Check for overlaps
      intersec = any([I1(1)<=I2(2) & I2(1)<=I1(1) , ...
                           I1(2)<=I2(2) & I2(1)<=I1(2) , ...
                           I2(1)<=I1(2) & I1(1)<=I2(1) , ...
                           I2(2)<=I1(2) & I1(1)<=I2(2) ]);

      %% If there are overlaps create a new interval 
      %% spanning the overlapping ones.
      if intersec
         I(k,:) = [ min([I1(1),I2(1)]) max([I1(2),I2(2)])];
         I(j,:) = [];
         nI = size(I,1);
         sw =0;
      else
      %% Otherwise, bring the next interval 
        I(j:end,:)=circshift(I(j:end,:),-1);
        sw = sw +1;
        if sw >= nI
          k=k+1;
          j=k+1;
          sw = 0;
        end
      end
      % Are we done?
      if j>nI 
        u=I;
        return
      end
  end
end

%!demo
%! % Intervals
%! I = [0 1; -.5 .5; .9 1.2; 3 6.2]; 
%! u = intervalUnion(I)


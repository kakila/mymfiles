## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {[@var{s},@var{R}] =} mls (@var{N},@var{ftap})
## @deftypefnx {Function File} {[@dots{}] =} mls (@dots{},@var{R0})
## Shift register sequence using Fibonacci implementation.
##
## Generates @var{N} steps of a shift register sequence @var{s} with feedback taps @var{ftap}.
## If the feedback taps corresponds to the exponents of a primitive polynomial
## over GF(2) the sequence generated is a maximum length sequence. 
## The last element of @var{ftap} must be zero. For example, the polynomial
## @asis{x^5 + x^3 + 1} is described with the vector @asis{[5 3 0]}.
## The length of the register is always the first element of @var{ftap}.
##
## If @code{2^m-1} is a prime number then an irreducible polynomial is primitive.
##
## For registers of length 10 the following feedback taps give maximal lenght sequences (add a zero at the end):
## @example
## [10, 7]
## [10, 3]
##
## [10, 9, 8, 5]
## [10, 9, 7, 6]
## [10, 9, 7, 3]
## [10, 9, 6, 1]
## [10, 9, 5, 2]
## [10, 9, 4, 2]
## [10, 8, 7, 5]
## [10, 8, 7, 2]
## [10, 8, 5, 4]
## [10, 8, 4, 3]
##
## [10, 9, 8, 7, 5, 4]
## [10, 9, 8, 7, 4, 1]
## [10, 9, 8, 7, 3, 2]
## [10, 9, 8, 6, 5, 1]
## [10, 9, 8, 6, 4, 3]
## [10, 9, 8, 6, 4, 2]
## [10, 9, 8, 6, 3, 2]
## [10, 9, 8, 6, 2, 1]
## [10, 9, 8, 5, 4, 3]
## [10, 9, 8, 4, 3, 2]
## [10, 9, 7, 6, 4, 1]
## [10, 9, 7, 5, 4, 2]
## [10, 9, 6, 5, 4, 3]
## [10, 8, 7, 6, 5, 2]
##
## [10, 9, 8, 7, 6, 5, 4, 3]
## [10, 9, 8, 7, 6, 5, 4, 1]
## [10, 9, 8, 7, 6, 4, 3, 1]
## [10, 9, 8, 6, 5, 4, 3, 2]
## [10, 9, 7, 6, 5, 4, 3, 2]
## @end example
## Source: http://www.newwaveinstruments.com/resources/articles/m_sequence_linear_feedback_shift_register_lfsr.htm
##
## TODO: Accept ftaps as coefficients of the polynomial.
##
## @seealso{goldcode}
## @end deftypefn

function [s R] = mls (N,ftap,R=[])

  m = ftap (1);
  n = 2^m - 1;
  
  if isempty (R)
    R = ones (1, m);
  endif
  
  s = zeros (N,1);

  # Fibonacci implementation
  idx = m - ftap(2:end);
  for i = 1:N
    s(i) = R(m);
    R = [mod(sum(R(idx)),2) R(1:m-1)];
  endfor
  
endfunction

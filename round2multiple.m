## Copyright (C) 2014 Juan Pablo Carbajal
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{K} =} round2multiple (@var{A}, @var{I})
## Returns the multiple of @var{I} that is closest to @var{A}.
## @var{I} and @var{A} are integers between 0 and 9.
## @end deftypefn

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2014-10-28

function k = round2multiple (A, I)
  if !mod(A,2)
    i = (0:9) + A/2 - 1;
  else
    i = (0:9) + floor (A/2);
  endif
  k = (i - mod (i,I))(A+1);
endfunction

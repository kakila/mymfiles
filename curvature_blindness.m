## Copyright (C) 2017 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2017-12-15

clear all
close all
## Parameters
#

# Geometric
A    =  0.5;   # oscillation amplitude
D1   =  1;     # small vertical gap
D2   =  5;     # large vertical separation
nP   = 12;     # number of pairs
nS   = 10;     # number of segments
nT   =  5;     # numer of points per segment

# Color
dark = 0.3 * ones (1,3);   # dark segments
lite = 0.7*ones(1,3);      # light segments
lbkg = 0.5*ones(1,3);      # light background

# PNG output
lw   =  1;          # linewidth for png version
sz   = "-S600,660"; # width and height in pixels
#######

f = nS;
t = linspace (0, 0.5/f, nT).';
w = 2 * pi * f;
d = 0:pi:w;
n = length (d);
c1 = A * cos (w*t + d);
c2 = A * cos (w*t + d-pi/2);
x = t + d / w;

C   = [];
sep = 0.0;
k   = 0;
xshift = false;
for j=1:2*nP
  if xshift
    C(:,end+1:end+n) = c1 + sep;
  else
    C(:,end+1:end+n) = c2 + sep;
  endif
  xshift = ifelse (mod(k,2), true, false);
  k   += j;
  sep += ifelse (mod(j,2), D1, D2);
endfor
C = (C - min(C(:))) ./ (max (C(:)) - min (C(:)));
x = (x - min(x(:))) ./ (max (x(:)) - min (x(:)));
x = repmat (x, 1, 2*nP);

function h = plot_set (x, y, lbkg)
  axis off

  patch ([0 0 0.5 1 1 0.5],[0.5 -0.02 -0.02 0.5 1.02 1.02], lbkg, ...
  'edgecolor', 'none');
  patch ([0.5 1 1],[-0.02 -0.02 0.5], zeros(1,3), 'edgecolor', 'none');
  
  hold on
  h = plot (x, y);
  hold off

  axis tight
  set(gca,'Position', get (gca,'OuterPosition'));

endfunction
figure (1)
clf
h = plot_set (x, C, lbkg);
h = reshape (h, n, 2*nP).';
for i=1:2:n
  set (h(:,i),'color', dark, 'linewidth', lw)
  if i < n
    set (h(:,i+1), 'color', lite, 'linewidth', lw);
  endif
endfor
pause(0.1)
print ("-dpng", sz, 'curvature_blindness1.png')
set (h(:), 'linewidth', 2 * lw);
axis tight

disp("Press a key to shift the dark segments in one of the curves")
pause

set (h(:), 'linewidth', lw);
N = numel (h);
for i=1:2:n
  set (h(1:2:end,i),'color', lite, 'linewidth', lw)
  if i < n
    set (h(1:2:end,i+1), 'color', dark, 'linewidth', lw);
  endif
endfor
pause(0.1)
print ("-dpng", sz, 'curvature_blindness2.png')
set (h(:), 'linewidth', 2 * lw);
axis tight



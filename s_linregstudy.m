
nc = 5;
nr = 500;
X_  = randn (nr, nc);
b  = (1:nc).';
y0 = X_ * b;
y  = y0 + 0.1 * sqrt (meansq (y0)) * randn(nr, 1);

X = X_ + 1e-3*(2*rand(size(X_))-1);

Xinv = pinv (X);
b_p  = Xinv * y;

% X' * X != I
v          = X' * y;
normv      = sqrt (sumsq (v));
vhat       = v / normv;
norm2Xvhat = sumsq (X * vhat);
% norm2Xv    = sumsq (X * v);
lambda = normv / norm2Xvhat; % = normv^3 / norm2Xv;
b_l = lambda * vhat;

% X' * X == I
Xhat = orth (X);
% T       = Xhat \ X;
% vh      = Xhat' * y;
% normvh  = sqrt (sumsq (vh));
% vhhat   = vh / normvh;
%% norm2Xhatvhat = sumsq (Xhat * vhhat) == 1
% lambdah = normvh
b_lh = Xhat' * y;
%% v_vh = T' * b_lh  == (Xhat * T)' * y == v; 

meansq ([X*[b_p b_l] Xhat*b_lh]  - y)
meansq ([b_p b_l Xinv*Xhat*b_lh] - b)

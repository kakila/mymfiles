## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

# load the file
% load ("")

# First plot healthy data
pkg load statistics
figure (1)
clf
[sh hh] = boxplot (healthy);
# Plot Epileptic data
hold on
[se he] = boxplot (epilep);
hold off

# Set colors
set ([hh.box; hh.whisker],"color","b"); # Healthy is blue
set (hh.median,"color","b","linewidth",2); # Healthy is blue
if isfield (hh,"outliers");
  set (hh.outliers,"color","b","marker","."); # Healthy is blue
endif
if isfield (hh,"outliers2");
  set (hh.outliers2,"color","b","marker","."); # Healthy is blue
endif

set ([he.box; he.whisker],"color","r"); # Sick is red
set ([he.median],"color","r","linewidth",2); # Healthy is blue
if isfield (he,"outliers");
  set (he.outliers,"color","r","marker","."); # Healthy is blue
endif
if isfield (he,"outliers2");
  set (he.outliers2,"color","r","marker","."); # Healthy is blue
endif


## Copyright (C) 2013 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {[@var{}] =} functio_name (@var{})
##
## @end deftypefn

function y = wiener2 (t,l=1)

  dt     = diff (t);
  [nT,d] = size (t);
  y = zeros (nT,d);
  r1 = 2*rand(nT-1,d)-1; r1 .*= abs(r1);
  r2 = l*(2*rand(nT-1,d)-1);
  for i = 1:nT-1
    yo = y(i,:);
    y(i+1,:) = yo + ((0.3+r1(i,:)).* _vel_ (yo) + r2(i,:)) .* dt(i,:);
  endfor
endfunction

function v = _vel_(y)
  v = -y.*(1-y.^2);
endfunction

% Signal generation
t    = linspace (0,50,5e3).';
freq = [1 3 6 8 10 15 20 22 27 30];
A    = randn (length(freq),1);
y    = sin (2*pi * bsxfun (@times,freq,t)) * A;
% filtering
wc    = 2 * [10 20] / 100;
[b,a] = butter (5,wc);
yf    = filtfilt (b,a,y);

% FFT
[u f] = normFFT (t, detrend ([y yf abs(yf)],0));

%plot
semilogy (f, abs (u));
axis tight
axis ([0.5 50 1e-6 1]);
legend ({'raw','filt','abs'},'Location','NorthOutside', ...
                                 'Orientation','horizontal')
grid on
xlabel("Frequency [Hz]")
ylabel("FFT intensity")
print -dpng "NLFREQGEN.png"

## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

%% Script
% multNoise.m
% Noise
nt = 100;
Nsamples = 50;
W = 2*rand(nt,Nsamples)-1;

%% No noise
function x = noN(n,xn,f)
    x = f(xn,n);
endfunction

%% Additive Noise
function x = addN(n,xn,f,W,ep)
    x = f(xn,n) + ep*W(n);
endfunction

%% Multiplicative noise
function x = mulN(n,xn,f,W,ep)
    x = f(xn,n).*(1 + ep*W(n));
endfunction


% initial value
x = zeros(nt,3,Nsamples);
x(1,:,:)=zeros(1,3,Nsamples);

% forcing
force =@(x,t) 0.1*sin(3*2*pi*t/nt);
for j=1:Nsamples
        ind = (1:nt-1)';
        next_ind = (2:nt)';
        x(next_ind,1,j) = noN(ind,x(ind,1,j),force);
        x(next_ind,2,j) = addN(ind,x(ind,2,j),force,W(ind,j),0.5);
        x(next_ind,3,j) = mulN(ind,x(ind,3,j),force,W(ind,j),0.1);
        x(:,:,j) = cumsum(x(:,:,j));
end

%% Distributon of time series with noise
figure(1)
D1 = squeeze(x(:,2,:))(:);
D2 = squeeze(x(:,3,:))(:);
subplot(1,2,1)
hist(D1,10,1)
xlabel('Additive noise')
subplot(1,2,2)
hist(D2,10,1)
xlabel('Multiplicative noise')


%% Time Series
figure(2)
subplot(3,1,1)
plot(x(:,1,:))
legend('No noise')
subplot(3,1,2)
plot(x(:,2,:))
legend('Additive Noise')
subplot(3,1,3)
plot(x(:,3,:))
legend('Multiplicative Noise')



## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function x = dsdrifter (F, n = 1, varargin)

  # --- Parse arguments --- #
  parser = inputParser ();
  parser.FunctionName = "dsdrifter";
  parser = addParamValue (parser,"Mass", 1, ...
                                    @(x)isscalar(x) || all(size (x)==[n,1]));
  parser = addParamValue (parser,"Pos", rand(2,n), @(x) all(size (x)==[2,n]));
  parser = addParamValue (parser,"Vel", rand(2,n), @(x) all(size (x)==[2,n]));
  parser = addParamValue (parser,"nIter", 100, @isscalar);
  parser = addParamValue (parser,"Damping", 1e-2, @isscalar);

  parser = parse(parser,varargin{:});

  m      = parser.Results.Mass;
  x0     = parser.Results.Pos;
  v0     = parser.Results.Vel;
  nT     = parser.Results.nIter;
  d      = parser.Results.Damping;
  
  clear parser
  # ------ #
  # Check force function
  [s1,s2] = size (F(x0,1));
  if s1 !=2 && s2 != 1
    error ("Force function must return a column vector.\n");
  endif
  
  # Acceleration update
  if n > 1
    Acc = @(x) cell2mat (arrayfun (@(i)F(x,i)./m, 1:n, "unif", 0));
  else
    Acc = F;
  endif
  
  x = zeros (2,n,nT);
  
  dt2 = (1e-2)^2;
  x(:,:,1) = x0;
  x(:,:,2) = x(:,:,1) + v0*sqrt(dt2) + 0.5*Acc(x(:,:,1))*dt2;  
  for t = 2:nT-1
    x(:,:,t+1) = (2-d)*x(:,:,t) - (1-d)*x(:,:,t-1) + Acc(x(:,:,t))*dt2; 
  endfor

  x = permute (x, [3 1 2]);
endfunction

%!demo
%! N = 100;
%! Force = @(x,i) -20*(x(:,i)-[0.5;0.5])./sumsq(x(:,i));
%! x0 = [0.5*ones(1,N); linspace(0,0.4,N)];
%! v0 = [2*(-1).^(rand(1,N)>0.5)+2*randn(1,N); zeros(1,N)];
%! x = dsdrifter (Force,N,"Pos",x0,"Vel",v0);
%! clf 
%! hold on
%! for i=1:N 
%!   plot(x(:,1,i),x(:,2,i),'.','markersize', 2); 
%! endfor 
%! axis([0 1 0 1])
%! axis equal
%! # -------------------------------------------------
%! # Drifters in a circular force field.

## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

% Copyright (C) 2010   Kyle Winfree <kyle.winfree@gmail.com>
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% This program computes the Gramian of a given set of vectors.
% This is found as G = [ <xi, xj> ]
% The function must be supplied either a single matrix with columns
% equal to the vectors in question, or with a series of column vectors
% all the same size.  Linear independence can then be determined if
% det(G) ~= 0.  This is just one use of the Gramian matrix.

function [G, M] = gramian(varargin)
     switch(length(varargin))
     case 1
	  % a matrix has been given such that each column is a vector
	  M = varargin{1};
     otherwise
	  % separate vectors have been given, first check to ensure they all have the same dimension.
	  sz = size(varargin{1});
	  M = zeros(sz(1),length(varargin));
	  for(n=1:length(varargin))
	       if(size(varargin{n},1) ~= sz(1) || size(varargin{n},2) ~= 1)
		    fprintf('Sorry, there was an error with your input (vector %i).  Please ensure the vectors all have the same number of rows.\n', n);
		    M = [];
		    G = [];
		    return
	       end
	       M(:,n) = varargin{n};
	  end
     end
     n = size(M,2);
     for(i=1:n)
	  for(j=i:n)
	       G(i,j) = dot(M(:,i), M(:,j));
	  end
     end
     G = G + G' - eye(size(G)).*G; % mirror the top to the bottom, without doubling the diagonal
end

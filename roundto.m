## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deffn {} {@var{y} =} roundto (@var{x}, @var{r})
## Round @var{x} to the closest number with resolution @var{r}.
##
## Example:
##
## @example
## x = [1.46 1.35 1.76];
## roundto (x, 0.25);
## ans =
##
##   1.5000   1.2500   1.7500
## @end example
##
## @seealso{round}
## @end deffn

function y = roundto (x, r)

  # algo is defined for +tives numbers
  sx = sign (x);
  x  = abs (x);

  # take digits down to resolution
  fx = floor (x) / r; # relevant digits at resolution
  lx = round (round (x / r) - fx); # reminder digits

  # apply resolution
  y   = sx .* (fx + lx) * r;

endfunction

%!shared tol
%! tol = sqrt (eps);

%!assert (roundto (1+[0.24 -0.24 0.26], 0.5), [1 1 1.5], tol);
%!assert (roundto (1+[0.124 -0.124 0.13], 0.25), [1 1 1.25], tol);
%!assert (roundto (1+[0.09 -0.09 0.11],0.2), [1 1 1.2], tol);
%!assert (roundto (1+[0.04 -0.04 0.06], 0.1), [1 1 1.1], tol);
%!assert (roundto (1+[0.02 -0.02 0.03], 0.05), [1 1 1.05], tol);

%!demo
%! N = 25;
%! n = 10;
%! r = [N fliplr(1:n)];
%! y = zeros (N, n+1);
%!
%! y(:,1) = linspace (0, 1, N).';
%! for i = 2:n+1
%!    y(:,i) = roundto (y(:,1), 1 / r(i));
%! endfor
%!
%! h = plot (y(:,1), y, '-o');
%! axis tight
%! grid on
%! cc = jet (n+1);
%! for i=1:n+1; set(h(i),'color',cc(i,:)); endfor
%! txt = strsplit (sprintf(repmat('1/%d ',1, n+1),r))(1:end-1);
%! legend (h, txt, 'location', 'northeastoutside');

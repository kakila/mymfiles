## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (c) 2012 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {@var{} =} function_name (@var{}, @var{})
##
## @seealso{}
## @end deftypefn

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function total_time = serial_oscilloscope (device, varargin)

 # --- Parse arguments --- #
  __data__ = abs (detrend (data,0));

  posscal = @(x)isscalar (x) && x >= 0;
  methcal = @(x)ischar (x) && ismember (x, {"set","refresh"});

  parser = inputParser ();
  parser.FunctionName = "serial_oscilloscope";
  parser = addParamValue (parser,"plotMethod", "refresh",methcal);
  parser = addParamValue (parser,"Ydiv",1,posscal);
  parser = addParamValue (parser,"Xdiv",2,posscal);
  parser = addParamValue (parser,"bufferSize",256, posscal);
  parser = addParamValue (parser,"screenSize",5*bufferSize, posscal);


  parser = parse(parser,varargin{:});

  plotmet   = parser.Results.plotMethod;
  Ydiv      = parser.Results.Ydiv;
  Xdiv      = parser.Results.Xdiv;

  device      = parser.Results.Device;
  buffer_size = parser.Results.bufferSize;
  screen_size = parser.Results.screenSize;

  clear parser posscal methcal
  # ------ #

  # Prepare figure
  close all
  figure (1)
  x = 1:screen_size;
  y = nan (size (x));
  h = plot (x,y,'-');

  axis_span = [0 screen_size 0 Ydiv];
  axis (vax);

  ##############
  # Flush input and output buffers
  srl_flush(device);

  switch plotmeth

    case "set"
      error ("Method not implemented.\n");
      #{
      while true
        tic;
        data = srl_read(device, buffer_size);
        set (h, "xdata", x, "ydata", y);
        axis (vax + t(window(1))*[1 1 0 0])
        drawnow
        window += buffer_size;

        time_elapsed = toc;
        if time_elapsed < sleep_time
          pause (sleep_time - time_elapsed);
        end
        cummt += max(time_elapsed,sleep_time);
      end
      #}
    case "refresh"
      set(h,"ydatasource","y")

      collected = 0;
      while true
%        tic;
        [data n]   = srl_read (device, buffer_size);
        data = double (data);
        if collected <= screen_size
          y(collected+1:collected+n) = data;
          collected += n;
        else
          y(end-buffer_size:end) = data;
          y(1:buffersize) = [];
        end
        refreshdata ()

%{
        time_elapsed = toc;
        if time_elapsed < sleep_time
          pause (sleep_time - time_elapsed);
        endif
%}
      endwhile

  endswitch

endfunction

function hnew = rotate_xtick(ax)

  ## get xtick
  xtick      = get (ax, 'xtick');
  xticklabel = get (ax, 'xticklabel');

  ## get position of current xtick labels
  h              = get (ax,'xlabel');
  xlabelstring   = get (h,'string');
  xlabelposition = get (h,'position');

  ## construct position of new xtick labels
  yposition = xlabelposition(2);
  yposition = repmat (yposition,length(xtick),1);

  ## disable current xtick labels
  set (ax,'xticklabel',[]);

  ## set up new xtick labels and rotate
  hnew = text (xtick, yposition, xticklabel);
  set (hnew,'rotation',90,'horizontalalignment','right');

endfunction

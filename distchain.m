## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-01-23

function p = distchain (xy, dist='euclidean')

  [n dim] = size (baseline (xy));
  
  if ischar(dist)
    [~,t] = sort(squareform (pdist (xy, dist)), 2, 'ascend');
  else
    [~,t] = sort(dist(xy), 2, 'ascend');
  endif
  
  p = NA (n,1);
  p(1) = 1; 
  broot=1;
  for i=2:n
    j = find (all( p(1:i-1) != t(p(i-1),2:end)), 1, 'first');
    if j <= 2
      p(i) = t(p(i-1),j+1); 
    else
      # branch!
      j = find (all( p(1:i-1) != t(broot,2:end)), 1, 'first');
      if j <= 2
        broot = t(broot,j+1);
        p(1:i-1) = flipud (p(1:i-1));
        p(i) = broot;
      else
        error ("both endpoints seem isolated\n");
      endif
    endif
  endfor

endfunction

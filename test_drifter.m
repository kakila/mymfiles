## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>


N     = 10;
function f = Force (x,i,l0)
  dx      = x - x(:,i);
  dx(:,i) = [];
  ndx     = sqrt (sumsq(dx));
  vx      = dx ./ ndx;
  f = (2*pi)^2*sum((ndx - l0).*vx,2);
endfunction
R     = 0.2;
theta = 2*pi*linspace(0,1-1/N,N);
x0    = R*[cos(theta); sin(theta)]+0.5;
v0    = 0.1*randn(2,N);
x     = dsdrifter (@(x,i)Force(x,i,R),N,"Pos",x0,"Vel",v0);

# Put all data in [0,1]²
z = x;
#z = zeros (size(x));
#z(:,1,:)     = 0.5*baseline(squeeze(x(:,1,:)).').'+0.25;
#z(:,2,:)     = 0.5*baseline(squeeze(x(:,2,:)).').'+0.25;

figure(1)
clf;
plot(squeeze(z(1,1,:)),squeeze(z(1,2,:)),'ok')
hold on
col = jet (N);
for i=1:N
  plot (z(:,1,i),z(:,2,i),'.','color',col(i,:));
endfor
hold off
axis ([0 1 0 1])

#[addr,t] = drifter2aer (z);
[addr,t] = drifter2aer (z,"OffEvents");

#saveaerdat ([int32(t),uint32(addr)],"test.aedat");

## Test spike to continuous signal
idx = find (rand (length(t),1) > 0.5);
spka   = addr(idx);
spkpol = 1-2*mod(spka,2);
spkt   = t(idx);
spkid  = bin2dec(dec2bin (spka,16)(:,2:15));

tt = linspace (0,max(t*1e-6),1e3).';
A  = spike2cont (tt, spkt, spkpol, spkid);


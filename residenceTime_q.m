## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>


## A script to implement a residence time algorithm based on
## clustering.

if true
  clear all
  pkg load communications

  # Defie a DS with a modulated pitfor bifurcation
  f = @(t,x,a) a(t).*x - x.^3;

  # Define modulation of the bifurcation
  nw = 10;
  w  = randn(nw,1);
  w  = w / sqrt(sumsq(w));
  a  = @(t) (tanh (10*sin (2*pi*[1:nw]/pi.*t)*w) + 1 ) / 2;

  # Generate a trajectory
  t  = linspace (0, 20, 1e3).';
  dt = t(2) - t(1);
  T  = t(end) - t(1);

  y = lsode (@(x,t)10*f(t,x,a), 0.1, t);
endif

# quantize the singal
N = 50;
parition = linspace (min (y), max (y), N);
p_value = quantiz (y, parition);

# Entry indices
e_idx = [1; find(abs(diff(p_value))>=0.9)+1];
plot (t,p_value, t(e_idx),p_value(e_idx),".g");

# Residence time (time at constant p_value)
resT = t(e_idx(2:end)-1) - t(e_idx(1:end-1));
resT(end+1) = t(end) - t(e_idx(end));

# Recurrence time (time between to occurenc eof the same p_value)
np = length (e_idx);
for i = 1:N
  tmp = find (p_value(e_idx)==i-1);
  j = tmp < np;
  o_tmp = e_idx(tmp(j)+1)(1:end-1);
  i_tmp = e_idx(tmp(j))(2:end);
  recT{i} = t(i_tmp) - t(o_tmp);
endfor


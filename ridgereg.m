## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2013 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {@var{b} =} ridgereg (@var{A}, @var{x}, @var{l})
## @deftypefnx {Function File} {@var{b} =} ridgereg (@dots{}, @var{M})
## Ridge regresssion A*b = x.
##
## @end deftypefn


function b = ridgereg (data,x,lambda,Mats=[])
  [nT n]    = size (data);

  if isempty(Mats)
    [U, S, W] = svd (data);
  else
    U = Mats.U;
    S = Mats.S;
    W = Mats.W;
  endif

  ST = S.';
  U  = U.';

  nl = length (lambda);
  b  = zeros (n,nl);
  for i=1:nl
    b(:,i) = W * ((ST * S + lambda(i)^2 * eye (n)) \ (ST * U * x));
  end
endfunction

function dxdt = wave (x,t)

  Nx = length (x) / 2;
  pos = 1:Nx;
  vel = pos+Nx;
  dxdt      = zeros (length (x), 1);
  dxdt(pos) = x(vel);
  u = x(pos);
  dxdt(vel) = Nx^2*[u(2)-2*u(1); u(3:end)+u(1:end-2)-2*u(2:end-1); u(end-1)-2*u(end)];

  tf = dxdt <= sqrt(eps);
  dxdt(tf) = 0;

endfunction

## Copyright (C) 2020 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2020-08-12

# Example in 3D
dim = 3;
S   = 1;
n   = ones(dim, 1); # perpendicular vector
p   = S * ones(dim, 1) / dim;

# null-space of the perpendicular vector (transposed, i.e. row vector)
# this generates a basis in the plane
V = null (n.');

# These steps are just to reduce the amount of samples that are rejected
# we build a tight bounding box
bb = S * eye(dim); # each column is a corner of the constrained region
# project on the null-space
w_bb = V \ (bb - repmat(p, 1, dim));
wmin = min (w_bb(:));
wmax = max (w_bb(:));

# random combinations and map back
nsamples = 1000;
w        = wmin + (wmax - wmin) * rand(dim - 1, nsamples);
x        = V * w + p;

# mask the points inside the polytope
msk = true(1, nsamples);
for i = 1:dim
  msk &= (x(i,:) >= 0);
endfor

x_in = x(:, msk); # inside the polytope (your samples)
x_out = x(:, !msk); # outside the polytope

# plot the result


scatter3 (x(1,:), x(2,:), x(3,:), 12, double(msk), 'filled');
hold on
plot3(bb(1,:), bb(2,:), bb(3,:), 'xr')
axis image




## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## Signal creation
#
t   = linspace (0, 1, 3e3).';
phi = sin (2*pi * 6 * t) + 0.1 * randn (size (t));

phibnd = [min(phi); max(phi)];   # Bounds
phibnd += eps (phibnd) .* [-1;1];    # Numeric Bounds
phiPP  = diff (phibnd);              # Interval (peak-to-peak)

## Quantization
#
#T                = @(x) interp1 ([0 0.4 0.6 1], [0 0.2 0.8 1], x);
T                = @(x) interp1 ([0 0.1 0.9 1], [0 0.45 0.55 1], x);
Nlvl             = 6;  # Levels
[phi_q idx phic] = quantize (phi, Nlvl, T);
phib             = [min(phi); phic; max(phi)];

figure (1), clf

  subplot (1, 2, 1)
  plot ((0:Nlvl+1), phib, '-',1:Nlvl, phic,'ob')
  axis tight
  set (gca, 'xtick', 1:Nlvl, 'xgrid', 'on');
  ylabel ('signal')
  xlabel ('bin')
  v = axis ();

  subplot (1, 2, 2)
  plot (t, phi,';signal;', t, phi_q, ';quantized;')
  axis tight
  axis ([axis()(1:2) v(3:4)]);
  xlabel ('time')

## Frequency of bins
# Each bin is a symbol in an alphabet
# we cunt how many appear and estimate probabilities
N    = length (idx);
freq = accumarray (idx, ones (N, 1), [Nlvl, 1]);
prob = freq / N;
s    = unique (idx);
figure(2)
bar (s, prob)
axis tight

# Entropy
H = -sum (prob .* log (prob)) / log (2)

# conditional prbabilities
F = crosstab (phi_q(1:end-1), phi_q(2:end));

P = F / (length (phi_q) - 1); # P(a,b)

Pm{1} = sum (P, 2);           # P(a)
Pm{2} = sum (P, 1);           # P(b)
Pcond = P ./ Pm{2};           # P(a | b) = P(a,b) / P(b)

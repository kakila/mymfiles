## Copyright (C) 2015 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

### -*- texinfo -*-
## @deftypefn {Function File} {@var{} =} lorenz (@var{}, @var{})
##
## @seealso{}
## @end deftypefn

## It doesn't work with noise

function [y rsq] = lorenz (N, y0 = [5 -10 3], r  = 28, s  = 10, b = 8/3, f = 100, Nx = 10, lvl = 0)
  global rsq xav xsq
  rsq = xav = xsq = 0;

  #TODO: parsed inputs
  dh  = 5e-4;
  T   = N / f;
  Nr  = T / dh
  Nx  = Nx / dh;
  #try

    odeopt = odeset ("InitialStep", dh, ...
                     "MaxStep", dh, ...
                     "OutputSave", ceil (Nr / N));
    if (Nx > 0)
      # Discard transtient
      odeopt_ = odeset (odeopt,"OutputSave", Nx);

      [t y] = ode45 (@(t,x)lorenz_sys(t,x,s,r,b, 0), 0:dh:Nx*dh, y0, odeopt_);
      y0    = y(end,:);
      clear lorenz_sys;
    endif

    [t y] = ode45 (@(t,x)lorenz_sys(t,x,s,r,b, lvl), 0:dh:Nr*dh, y0, odeopt);

    if (lvl > 0)
      #rlevel = sqrt ( rsq / (xsq - (xav^2 / (Nr+1)) ) ) * 100
      rlevel = sqrt (rsq)/ sqrt (xsq - xav * xav / (Nr+1)) * 100
      printf ("Noise level in percent of x-coordinate: %g\n", rlevel);
      fflush (stdout);
      [rsq xsq xav]
    endif

  #catch
#    error ("Octave:tisean", ...
#      "This function requieres the OctaveForge package odepkg to be loaded.");
  #end

endfunction

function dxdt = lorenz_sys (t,x, sigma, rho, beta, lvl)
  global rsq xav xsq

  persistent r iter
  if (isempty (iter) || iter == 7)
    iter = 1;
  end

  if (lvl > 0 && iter == 1 ) # update with noise only once per RK update.
    r = randn (3,1) * lvl;
    x   += r;
    rsq += r(1)^2;
    xav += x(1);
    xsq += x(1)^2;
  endif

  dxdt(1) = sigma * ( x(2) - x(1) );
  dxdt(2) = x(1) * ( rho - x(3) ) - x(2);
  dxdt(3) = x(1) * x(2) - beta * x(3);

  iter++;
endfunction

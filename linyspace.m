## Copyright (C) 2017 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2017-12-13

## -*- texinfo -*-
## @defun {@var{} =} linyspace (@var{}, @var{})
## 
## @seealso{}
## @end defun

function x = linyspace (y0, yf, nT, f, invf=[])

  y  = linspace (y0, yf, nT);
  x0 = [];
  if is_function_handle (invf)
    x = invf (y);
    return;
  elseif isvector (invf)
    x0 = invf;
  endif
  
  ff = @(u,v) f(u) - v;
  
  x = zeros (size(y));
  if isempty (x0);
    x0 = 0;
  endif
  x(1) = fzero (@(u)f(u)-y(1), x0(1));
  if nT == 1; return; endif
  
  if length (x0) == 1
    x0(2) = x(1);
  endif
  x(2) = fzero (@(u)f(u)-y(2), x0(2));
  
  for i=3:nT
    x(i) = fzero (@(u)f(u)-y(i), 2 * x(i-1) - x(i-2));
  endfor

endfunction

%!assert (linyspace (0,1,10,[],@(x)x), linspace(0,1,10), sqrt(eps));
%!assert (linyspace (0,1,10,@(x)x), linspace(0,1,10), sqrt(eps));



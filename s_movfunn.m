## Numerical Taylor expansion using moving windows
# We use a moving window to extract the local polynomial of a sampled function.
# This script is meant to show a trick to use |movfun| on functions
# that consume several input arguments (herein |polyfit|).
#
# The results are almost equivalent to applying a Savitzsky-Golay smoothing 
# filter. |movfun| is much slower than |sgolayfilt|, but it can be easily 
# parallelized.
##

# Copyright (C) 2019 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2019-03-22

pkg load signal
#pkg load parallel

## Sampled data
# Here we generate samples of a 1D function using a Fourier basis with a finite
# number of frequencies
#
n  = 200;
t  = linspace (0, 1, n).';
w  = 2 * pi .* [1 2 3];
nw = length (w);
B  = @(t)[sin(w.*t) cos(w.*t)];                    # Fourier basis
a  = randn(6, 1); [~,o] = sort(abs (a)); a = a(o); # random growing coefficients
x  = B (t) * a + 0.1;                              # signal
xn = x + 0.5 * randn (n, 1);                       # noisy data

deg  = 4;  # Degree of the (Taylor) polynomial

# Compute noiseless signal derivatives
b    = B (t);              # Fourier basis
swap = [(nw+1):2*nw 1:nw]; # Reordering of the basis when applying derivative
bsig = zeros (deg, 2*nw);  # Sign of the basis when applying derivatives
bsig(:,1:nw)     = repmat ((-1).^cumsum(mod((0:deg-1).', 2)), 1, nw);
bsig(:,nw+1:end) = repmat ((-1).^cumsum(mod((1:deg).', 2)), 1, nw);
dx = zeros (n, deg+1);     # derivatives
for i=1:deg
  if mod (i,2)
    b_ = b(:, swap);
  else
    b_ = b;
  endif
  dx(:,i+1) = repmat(w.^i, 1, 2) .* (b_ .* bsig(i,:)) * a;
endfor
dx(:,1) = x;

figure (1), clf
  plot (t, x, 'o-;signal;', 'markersize', 4, 'markerfacecolor', 'auto');
  hold on
  plot (t, xn, 'o;noisy;', 'markersize', 6);
  hold off
  axis tight
  title ('Sampled signal')
  legend ('location', 'northeastoutside');

## Taylor expansion using |movfun|
# Regressing a polynomial of degree |deg| centered at |t0| provides an 
# approximation of the Taylor coefficients.
# Here we regress on a moving window centered at all the given data |t|
#

function p = movpolyfit (idx, t, x, deg, c)
  # Polynomial regression on a moving window
  #
  # t,x are the given sampled data
  # deg is the degree of the polynomial to be fitted
  # idx are indexes into the data selecting the current window.
  # c is the center of the window. Is left to the caller to compute the center
  # of the window.
  #
  # This function is brittle with respect to the 'Endpoints' option passed to
  # |movfun|
  
  [wlen d] = size (idx);
  p = zeros (d, deg+1);
  if all (idx == 0)
    # FIXME
    # This is a bad behavior of |movfun|, it passes all zeros to check output
    # dimension. It should use part of input.
    return
  endif
  # The regression is centered at the current window center, hence the 
  # coefficient of degree n approximate the n-th derivative, e.g. the
  # intercept of the polynomial approximates the data itself.
  #
  # The index are filtered > 0 to handle boundary conditions. Note that we call
  # |movfun| with 'Endpoints', -1.

  t0 = t(idx(c,:));
  for i=1:d
    jdx    = idx(idx(:,i) > 0, i);
    p(i,:) = polyfit(t(jdx) - t0(i), x(jdx), deg);
  endfor

  # cellfun is not more efficient for small number of data points
  # but movfun allows us to use parcellfun to distribute the computation

  # idx is in general a matrix, each column indicating a window of the data
  # Here we convert to cell because |polyfit| does not maps over columns of
  # its input arguments.
  #
#  IDX = mat2cell (idx, wlen, ones (1, d));
#  p   = cell2mat (cellfun (@(i)polyfit(t(i(i>0))-t(i(c)), x(i(i>0)), deg), ...
#    IDX, 'unif', 0).');
#   p   = cell2mat (parcellfun (nproc, ...
#      @(i)polyfit(t(i(i>0))-t(i(c)), x(i(i>0)), deg), IDX, ...
#      'UniformOutput', 0).');
endfunction

wlen = bitset (deg + 1, 1, 1); # next odd window length
cent = ceil (wlen / 2);        # center of odd window
P    = movfun (@(i)movpolyfit(i,t,x,deg,cent), (1:n).', wlen, 'Endpoints', -1);

##
# *Evaluate on noisy data*
#
# The window is much larger to smooth out noise.
# The higher the noise the larger the window.
#
wlen = bitset (deg + 1 + 20, 1, 1); # next odd window length
cent = ceil (wlen / 2);             # center of odd window

printf ('** movfun: ');
tic
Pn    = movfun (@(i)movpolyfit(i,t,xn,deg,cent), (1:n).', wlen, 'Endpoints', -1);
toc

# Savitsky-Golay smoothing filter with same parameters
printf ('** sgolayfilt: ');
tic
xn_sg = sgolayfilt (xn, deg, wlen);
toc

##
# Plot Taylor coefficients and show that they coincide with the derivatives 
# scaled by the factorial coefficient, in the case of no noise
#
figure (2), clf
  h = plot (t, P);
  hold on
  hd = plot (t, dx ./ factorial(0:deg), 'o');
  hold off
  axis tight
  for i=1:(deg+1)
    set (hd(i), 'markersize', 4, 'markerfacecolor', 'auto');
    set (hd(i), 'color', get (h(deg + 2 - i), 'color'));
  endfor
  legend(hd, strsplit(num2str(0:deg)), 'location', 'northeastoutside');
  title ('Taylor coeffs (noise free data) and derivatives')

##
# The same plot for the noisy data
#
figure (3), clf
  h = plot (t, Pn);
  hold on
  hd = plot (t, dx ./ factorial(0:deg), 'o');
  hold off
  [ym yM] = bounds (get (hd(end), 'ydata'));
  ylim([ym yM]);
  
  for i=1:(deg+1)
    set (hd(i), 'markersize', 4, 'markerfacecolor', 'auto');
    set (hd(i), 'color', get (h(deg + 2 - i), 'color'));
  endfor
  legend(hd, strsplit(num2str(0:deg)), 'location', 'northeastoutside');
  title ('Taylor coeffs (noisy data) and derivatives')

## Build local approximation
# We compute the Taylor approximation of the data at a few selected points
#
t0     = [0.13 0.47 0.81];              # desired approximation centers
nt0    = length (t0);
[~,it] = min (abs (t - t0));
t0     = t(it);                         # closest approximation centers.
                                        # Use interpolation of P for anywhere.
x_app  = xn_app = zeros (n, nt0);
for i = 1:nt0
  dtpow       = (t - t0(i)).^(deg:-1:0);
  x_app(:,i)  = dtpow * P(it(i),:).';
  xn_app(:,i) = dtpow * Pn(it(i),:).';
endfor

##
# Plot the noiseless signal and its local approximations
#
figure (4), clf
  plot (t, x, 'linewidth', 3);
  hold on
  plot(t, x_app);
  plot (t0, x(it), 'xk', 'linewidth', 3)
  hold off
  ylim([min(x), max(x)]);

##
# Plot the noisy data and its local approximations
#
figure (5), clf
  plot (t, x, '--k');
  hold on
  plot (t, xn, 'o', 'markersize', 6);
  plot(t, xn_app);
  plot (t0, xn(it), 'xk', 'linewidth', 3)
  hold off
  ylim([min(xn), max(xn)]);

## Smoother
# The intercept of the polynomial under noisy conditions smooths the data and
# approximates the signal.
# The results away from the boundaries coincide with the results of the
# Savitzsky-Golay smoothing filter.
#
figure (6), clf
  plot (t, x, 'o-;signal;', 'markersize', 4, 'markerfacecolor', 'auto');
  hold on
  plot (t, xn, 'o;noisy;', 'markersize', 6);
  plot (t, Pn(:,end), '-;smoother;', 'linewidth', 3)
  plot (t, xn_sg, '--;SG smoother;', 'linewidth', 3)
  hold off
  axis tight
  legend ('location', 'northeastoutside');


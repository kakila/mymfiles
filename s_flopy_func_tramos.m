clear all

# Definimos la function
function z = F(x,y)
   z = zeros (size(y,1),size(x,2));

   pos     = x > 0;
   z(pos)  = sin (x(pos) ./ y(pos)) ./ x(pos);

   neg    = x < 0;
   z(neg) = ( x(neg).^2 - y(neg)) ./ x(neg);
endfunction

# Generamos valores de X e Y. Notar la forma de cada uno.
x = linspace (-1, 1, 5);
y = linspace (-2, 2, 500).';

[X Y] = meshgrid (x,y);

Z = F(X,Y);

figure (1)
h = plot (y, Z);
legend (h, strsplit(num2str (x)));
axis tight
xlabel ("y")
ylabel ("f(x,y)")

figure (2)
x = linspace (-1, 1, 60);
y = linspace (-2, 2, 60).';

[X Y] = meshgrid (x,y);

Z = F(X,Y);

surf (X, Y, Z, "edgecolor", "w");
xlabel ("x")
ylabel ("y")
zlabel ("f(x,y)")
axis tight

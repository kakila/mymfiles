## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## Two players compete to get combinations HT and HH from a sequence of coin
## tosses. The first to get the combination wins.
## Who wins in average?

n = 1e4;                      # Number of plays
cc_idx = cs_idx = zeros(n,1); # Index of combination
for i=1:n
  s         = rand(100,2) >= 0.5; # Coin toss H=0 T=1
  cs_idx(i) = find (s(1:end-1,1)-s(2:end,1) < 0, 1, "first"); # If prev-next negative then 0-1 HT
  cc_idx(i) = find (s(1:end-1,2)+s(2:end,2) ==0, 1, "first");  # If prev+next zero then 0+0 HH
endfor
# Can be done more efficiently
# with a single random vector

HT = cs_idx < cc_idx; # Player HT wins
HH = cs_idx > cc_idx; # Player HH wins
D  = cs_idx == cc_idx; # Draw

bar ([-1 0 1], sum ([HT HH D]));
set (gca,"xticklabel", {"HT wins", "HH wins", "Draw"})
axis tight
ylabel ("# plays")

print -dpng "azar_casero.png"

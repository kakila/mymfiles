## Script
#
##

# Copyright (C) 2019 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2019-03-24

pkg load signal
rand ('state', double ('abracadabra'));

##
#
# $$ \mathcal{D}(x) := d_1 x + d_2 x^\prime $$
#
d         = [20; 1];
nt        = 100;
t         = linspace (0, 1, nt).';

## inHomogenous
# With source in the finite degree polynomial space
#
u   =@(t) (t - 0.5).^2 .* (t - 0.2) .* (t - 0.6) .* (t - 1).^2 .* t * 1e4;
deg = 7;

# TODO: initial conditions

# Choose deg + 1 random points
ii = randperm (nt, deg + 1 + 20);
ti = t(ii);
ui = u(ti) + 0.5*randn(length(ii),1);
x_ = zeros (nt, deg+1);
for i=1:nt
  x_(i,:)  = basis_D1Taylor (0:deg, ti - t(i), d) \ ui;
endfor

[~,x] = ode45(@(t,x)-x*d(1)/d(2) + u(t), t, x_(1,1));
y     = x + 1e-1*randn(nt,1); # observations

figure (1), clf
  subplot (2,1,1)
  # verify solution
  plot (t, u(t), '-;u(t);');
  hold on
  plot (ti, ui, 'kx;u(t_i);')
  plot (t, x_(:,1:length(d)) * d, 'o;D(x);');

  hold off
  axis tight
  legend ();

  subplot (2,1,2)
  # D(x) = u,  solution
  plot (t, [x_(:,1) cumtrapz(t,x_(:,2))+x_(1,1)], '-', t, x, 'k--');
  axis tight
  legend ({'x\_', 'int dx\_','x'});

## Smoothing
#
# $$ \mathcal(D)(x) = \sum_{i=1}^N \delta(t-t_i) (x(t_i) + \epsilon_t) $$
#

#d = [0.01;1];
u     = @(z) 0.1*sin(2*pi*5*z); # actuation
[~,x] = ode45(@(t,x)-x*d(1)/d(2) + u(t), t, 1);
y     = x + 1e-1*randn(nt,1); # observations

## 
# *Smooth observations*
#

# Use sgolayfilt
x_ = sgolayfilt (y, 1, 11,0);
x_(:,2) = sgolayfilt (y, 1, 11, 1);

#taylordegy = 1;
#ordery = taylordegy + 1;
#[idx c pre pos win] = movslice (nt, ordery + 9);
#nc = length (c);
#x_ = zeros (nc, ordery);
#for i=1:nc
#  x_(i,:)  = polyfit(t(idx(:,i)) - t(c(i)), y(idx(:,i)), taylordegy);
#endfor

# Solve for x consistent with D(x) = u
taylordeg = 1;
order     = taylordeg + 1;
extra     = 0;
wlen      = bitset (order + extra, 1, 1); # next odd window length

[idx c pre pos win] = movslice (nt, wlen);
nc = length (c);
t0 = t(c).';
dt = t(idx) - t0;
X_ = x_(:,1)(idx);
U = u(t(idx));
p  = zeros (nc, order);
for i=1:nc
  P      = basis_D1Taylor (0:taylordeg, dt(:,i), d);
  p(i,:) = P \ U(:,i);
endfor

figure (2), clf
  plot (t, x_(:,1), 'o');
  hold on
  plot (t, x, 'k-');
  plot (t0,p(:,1), '-');
  hold off
  axis tight


## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

profile off;
profile on;
rc = rand( 1, 448 );
cube = rand( 160, 512, 448 );
 
y = zeros(size(cube));
 
tic
for lamda = 1: size( cube, 3 )
   y(:,:,lamda) = rc(lamda)./ cube(:,:,lamda) ;
end
s1 = toc;
 
tic
x = bsxfun(@rdivide, permute(rc, [1,3,2]), cube);
s2 = toc;
 
[s1, s2]
profile off;
T = profile("info");
profshow(T)

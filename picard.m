## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {@var{x} =}  picard (@var{A}, @var{F}, @var{b}, @var{x0})
## Finds the solution to A x + F(x)x = b using Picard (fixed point) iteration.
##
##
## Reference:
##
##
## @end deftypefn

function [x dx err niter] = picard (A, F, b, x_prev)
  # TODO put these as options
  #relchg  = 1e-10;
  #abschg  = 1e-10;
  abstol  = 1e-8;
  reltol  = 1e-6;
  maxiter = 100;

  F_prev = F (x_prev);

  niter  = 0;
  err = dx = x2 = 1;
  #while (dx/x2 > relchg && dx > abschg && err > abstol && err/x2 > reltol && niter < maxiter)
  while (err > abstol && err/x2 > reltol && niter < maxiter && !any(isnan (x_prev)) && !any(isinf (x_prev)))

    #x       = A \ (b - F_prev * x_prev);
    #x      = gmres (A, b - F_prev * x_prev, [], reltol, [], [], [], x_prev);
    #x      = cgs (A, b - F_prev * x_prev, reltol, [], [], [], x_prev);
    x      = pcg (A, b - F_prev * x_prev, reltol, [], [], [], x_prev);
    F_prev = F (x);

    dx  = sqrt (sumsq (x-x_prev));
    x2  = sqrt (sumsq (x));
    err = sqrt (sumsq ( A * x + F_prev * x - b ));

    x_prev = x;
    niter += 1;
  endwhile

  if any(isnan (x)) || any(isinf (x))
    warning("picard: convergence failure.\n", "Picard");
  endif

  if niter >= maxiter
    warning("picard: Maximum iterations reached.\n", "Picard");
  endif

endfunction

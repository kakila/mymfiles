## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {@var{} =} TAES (@var{}, @var{})
## 
## @seealso{}
## @end deftypefn

function  rho = TAES (nT, N, rho0, bc = [false false], mprob = [], store=true)

  % Do we want to store the history
  if store
    nTn = nT;
    t = @(i) i;
  else
    nTn = 1;
    t = @(i) 1;
  endif
  # occupation grid
  rho = false (nTn,N); 

  # Inital state
  if isscalar (rho0)
    n   = min (round (N*rho0),N); % # of particles;
    idx = randperm (N, n);
    rho(1,idx) = true; 
  else
    if length (rho0) != N
      error ("Inital condition and number of particles not compatible.")
    endif
    rho(1,:) = logical (rho0);
    n = sum (rho(1,:));
  endif
  
  # Dynamics
  # motion prbability
  if isempty (mprob)
    mprob = @() 0.5;
  endif
  
  for i=2:nT+1
    rho (t(i),:) = rho(t(i-1),:);
    # Move particles right
    p  = zeros (1,N);
    p(1,rho(t(i),:)) = rand (1,n);
    mR = find ( p < mprob () & rho(t(i),:) & ![rho(t(i),2:N) bc(2)] );
 
    # Move particles left
    mL = find ( p > mprob () & rho(t(i),:) & ![bc(1) rho(t(i),1:N-1)] );
    
    # Prevent anhilitation
    [tf,idx] = ismember(mL-1,mR+1);
    mL(tf) = [];
    mR(nonzeros(idx)) = [];
     
    if mR
      rho (t(i), mR) = false;
      rho (t(i), mR(mR<N)+1) = true;
    endif
    
    if mL    
      rho (t(i), mL) = false;
      rho (t(i), mL(mL>1)-1) = true;
    endif
    # Particle injection
    if rand < abs(sin(pi*10*i/nT));%mprob ()
      rho(t(i),1) = true;
    end
    
    n = sum (rho(t(i),:),2);
    if n == 0
      rho = rho(1:t(i),:);
      break;
    endif
  endfor
  
endfunction

## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

nT   = 1024;
tr   = linspace (0,1,nT).';
t    = linspace (0,1,32).';
Fs   = 1 / (tr(2)-tr(1));
sc   = 1e-4;
nk   = 10;
if regen
  symbols    = "qwertzuiopasdfghjklyxcvbnm";
  pattern    = arrayfun (@str2num, dec2bin (toascii (symbols),8)).';
  np         = size (pattern,2);
  target     = 1:np;
  ns         = length (idx_s);
  nx         = size (Ki,2) - ns;
  net        = struct ("KVL",Kv,"KCL",Ki,"sources",idx_s);
  net.mu     = 1e3*(1e5/25) * (1 + 0.1*randn (1,nx)) .* sign (2*randn(1,nx)-1);
  net.lambda = abs(100*(1+0.1*randn (1,nx)));
  net.x0     = 10 ./ net.mu;
  net.r      = ones (1,nx);
  net.R      = 1e3 * net.r;
  [A B Kiu Kii Kvu Kvv] = kirchhofmat (net);
  %msgr = sigmoid_train (tr, [0.1 0.2; 0.3 0.4; 0.7 0.8],5e-3) - sigmoid_train (tr, [0.5 0.6; 0.8 0.9],1e-2);
endif
if train
  # Train
  clear s sl; 
  l = logspace (-5,-1,20);
  f = zeros (length(t),ns);
  K = L = zeros (np,nk,ns*length(t)+1);
  s = sl = zeros (ns,np);
  for ipp=1:np 
    msgr  = interleave (pattern(:,ipp),(nT-1)/8,true);
    for ik=1:nk; 
      for i=1:ns
        fr = wienerrnd (tr, round(Fs/2), net.lambda(i));
        net.input{i} = data2fun (tr, fr.*(1 + msgr)*sc);
        f(:,i) = net.input{i}(t);
      endfor
      [v gamma phi h] = memristor_net (t,net);
      K(ipp,ik,1:end-1) = v(:).';
      L(ipp,ik,1:end-1) = f(:).';
      
    endfor
  endfor
  K(:,:,end) = mean(abs(K(:,:,1:end-1))(:));
  L(:,:,end) = mean(abs(L(:,:,1:end-1))(:));

  m = kron(target.',ones(nk,1));
  s  = xridgereg(reshape(K,np*nk,ns*length(t)+1), m,l);
  sl = xridgereg(reshape(L,np*nk,ns*length(t)+1), m,l);  

endif

# Test
clear err res; 
for ipp=1:np; 
  msgr  = interleave (pattern(:,ipp),(nT-1)/8,true);
  for i=1:ns
    fr = wienerrnd (tr, round(Fs/2), net.lambda(i));
    net.input{i} = data2fun (tr, fr.*(1 + msgr)*sc);
    f(:,i) = net.input{i}(t);
  endfor
  [v gamma phi h] = memristor_net (t,net);
  res(ipp)  = [v(:).' 1]*s;
  resl(ipp) = [f(:).' 1]*sl; 
  err(ipp,:)= abs([mean(squeeze(K(ipp,:,:))*s) res(ipp)]-target(ipp));
  errl(ipp,:)= abs([mean(squeeze(L(ipp,:,:))*sl) resl(ipp)]-target(ipp));  
endfor

%f = cell2mat ( pararrayfun (8, @(y)net.input{y}(t), 1:ns, ...
%                             "UniformOutput", 0 ));
%msg = interp1 (tr,msgr,t);

#xs = 10 ./ net.mu;
#upsilon0  = (xs + (net.x0 - xs) .* exp (- net.lambda .* t));
#upsilon = upsilon0 + sc*eg;
## Memristances
#h = net.r + (net.R - net.r) .* (tanh (net.mu.*upsilon/2) + 1) / 2;
## internal output flows
#gamma = sc*f * B.';
#V = gamma .* h;
## outputs
#v = V * A.';



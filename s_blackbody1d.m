## Copyright (C) 2015 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

# Script to implement the model in
# "Does equipartition of energy occur in nonlinear continuous systems?"
# in Patrascioiu, A. (1987). The Ergodic Hypothesis. Los Alamos Science, 1(15), 263–279.

%clear all
1;
function dpdt = rope (t,p,opt)
  persistent D1 D2 t_prev
  if ( isempty (D1) || size(D1,1) != opt.nX )
    [D1 D2] = diffOP (opt.nX, opt.dx);
  endif

  if ( isempty(t_prev) )
    t_prev = t;
  endif

  %% Pos. & Vel
  if ( !isempty (opt.X) )
    p0 = [p(opt.ix(opt.iX)) p(opt.iv(opt.iX))];
    f  = (D1 * p(opt.ix))(opt.iX);
    X = opt.X(t,t_prev,p0,f);
    p(opt.ix(opt.iX)) = X(:,1);
    p(opt.iv(opt.iX)) = X(:,2);
  endif

  dpdt(opt.ix) = p(opt.iv);
  dpdt(opt.iv) = D2 * p(opt.ix);

  %% Vel. post
  if ( !isempty (opt.X) )
    dpdt(opt.ix(opt.iX)) = X(:,2);
  endif

  %% Acc.
  if ( !isempty (opt.F) )
    p0 = [p(opt.ix(opt.iF)) p(opt.iv(opt.iF))];
    dpdt(opt.iv(opt.iF)) += opt.F(t,p0);
  endif

  t_prev = t;

endfunction

# Time
T = 10;          % duration of simulation
# Space
nX = 200 + 1;                    % Discretization
x  = linspace (-1,1,nX).';   % Position vector
dx = x(2)-x(1);              % Discretization step

iv = (nX+1):(2*nX);
ix = 1:nX;
xX = 0;
xF = xX;
[~,iX] = min (abs (x-xX));
[~,iF] = min (abs (x-xF));
K = (5*2*pi).^2;
a = 100;
w = 0.5;
F = @(t,x)-K*(x(1)+a*x(1).^3)-0e2*x(2);
X =[];
% Predefined
#X = @(t)0.5*exp(-(t-T./[2; 3]).^2/2/0.1^2) .* ...
#        [[1; 1] -(t-T./[2;3])/0.1^2];
% Autonomous
#X  = @(t,t_prev,x_prev) lsode (@(x,z)[x(2); -K*(x(1) + a*x(1)^3)/m], x_prev, [t_prev t])(end,:);
% Coupled
#X  = @(t,t_prev,x_prev,f) lsode (@(x,z)[x(2); -9e3*f/m - 1e3*x(2)/m-K*(x(1) + a*x(1)^3)/m], x_prev, [t_prev t])(end,:);
opt = struct ("nX",nX,"dx",dx,"iv",iv,"ix",ix,...
              "iX",iX,"iF",iF, ...
              "X",X,"F",F);

a  = 2 * 0.05^2;
p0 = 0.5*sin(2*pi*w*x-pi/2);%0*exp (-x.^2 / a);
v0 = 0*2*pi*w*cos(2*pi*w*x);%(2/a) * x .* p0;

dyn =@(t, z) rope (t,z,opt);

odeopt = odeset ("RelTol", 1e-6, "AbsTol", 1e-6, "NormControl", "on", ...
                 "InitialStep", 1e-3, "MaxStep", 0.1);
[tr X] = ode45 (dyn, [0 T], [p0; v0].', odeopt);

nT = min (length (tr), 100);
t  = linspace (0,T,nT).';
X_ = interp1 (tr,X,t);
p  = X_(:,ix);
dp = X_(:,iv);

h = plot (x,p(1,:),"-r", x(iX),p(1,iX),'.g');
set (h(2),"markersize",12);
axis ([min(x) max(x) min(p(:)) max(p(:))]);
axis manual;
for i=1:nT
  set(h(1), "ydata",p(i,:));
  set(h(2), "ydata",p(i,iX));
  pause (0.05);
  %print ("-dpng", sprintf("movie_%03d",i));
endfor

## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-01-26

## Covariance from weak Laplacian operator
# The fininte element representation of a differential operator
# is built to solve the problem 
#
# $ A(X_i, X_i) U_i = F_i $
# 
# where $U_i$ are the nodal values of the solution in the mesh
# $X_i$ and $F_i$ the nodal sources corresponding to the sources 
# in the differential equation.
# $A$ is a positive definite matrix by the properties of the
# assembly process.
# $A$ maps from nodal values to nodal sources.
# The unique FEM solution is given by
#
# $ U_i = A(X_i,X_i)^{-1} F_i $
#
# if we interpret this as a GP interpolation problem we will write it
# 
# $ U_i = K(X_i,X_i) F_i $
#
# Hence the inteprolation solution would take the form
#
# $ u(x) = K(x, X_i) \left( K(X_i,X_i) F_i \right) $
#
# That is
#
# $ u(x) = A^{-1}(x, X_i) \left( A(X_i,X_i) F_i \right) $
#
# We explore this solution here, when A is the FEM laplacian.

pkg load bim
randn ('state', double('FixIt!'))

## Asemble the laplacian matrix
#
nX = 100;
x  = linspace (0, 1, nX).';
nX = length (x);
A  = bim1a_laplacian (x, 1, 1);
u  = zeros (nX,1);

## Boundary conditions
# We set Dirichtlet boundary contions
tf     = ismember (x, [0 1]);
nodes  = 1:nX;
GammaD = nodes(tf);  # Dirichlet nodes
Omega  = nodes(!tf); # Internal nodes

##
# Dirichlet datum
u(GammaD) = [0.5; 0.3];

## Sources in the open domain
#
xi  = x(Omega);
Phi = exp(-(x - xi.').^2 / 2 / 0.05.^2);
g   = 10 * Phi * randn (length(Omega), 1);

## Assemble right hand side
#
rhs = zeros (nX, 1);
if any (abs(g) > eps)
  rhs = bim1a_rhs (x, 1, g);
endif
b = rhs(Omega) - A(Omega, GammaD) * u(GammaD);

## FEM Solution in the open domain
#
u(Omega) = cholinv (A(Omega, Omega)) * b;

## GP interpolation
# Analitic inverse
function K = covLaplacian (x,y)

  [Y X] = meshgrid (y, x);
  
  xley  = X <= Y;
  Omega = (X < 1 & X > 0) & (Y < 1 & Y > 0);

  K = zeros (size (X));

  tf    = Omega & xley;
  K(tf) = X(tf) ./ Y(tf);
  
  tf    = Omega & !xley;
  K(tf) = (1 - X(tf)) ./ (1 - Y(tf));
  
  tf    = Omega;
  K(tf) = K(tf) .* Y(tf) .* (1 - Y(tf));

endfunction

##
# Homogenous solution dicated by the BC on the orgiinal one
# same as cholinv -(A(Omega, Omega)) * A(OMega,GammaD) * u(GammaD)
mGP = @(z)interp1(x(GammaD),u(GammaD),z(:));

##
# GP Solutio in the whole domain
K  = covLaplacian (x, x(Omega));
uGP = K * rhs(Omega) + mGP(x);

figure(1)
clf
subplot(2,1,1)
h = plot(x, u, 'o-;FEM;', ...
         x(GammaD), u(GammaD), ...
         'or;BC;', ...
         x,uGP,'--;GP;');
axis tight
subplot(2,1,2)
hg = plot(x, g);
axis tight

## Sub.domain
# select a few points fomr the original domain
i   = [1:10:nX nX];
xo  = x(i);
uo  = u(i);
go  = g(i);
nXo = length (xo);

## BC
# Extract the BC in the new domain
tf     = ismember (xo, [0 1]);
nodes  = 1:length(xo);
GammaDo = nodes(tf);  # Dirichlet nodes
Omegao  = nodes(!tf); # Internal nodes

## FEM in sub-domain
# Solve the FEM problem as if the sub-domain was the whole mesh
# To be able to do this step we need that the sub-domian contains
# the borders of the whole, i.e. 
# $\Gamma_{\Omega} = \Gamma_{\Omega\prime}$
Aog = bim1a_laplacian (xo, 1, 1);
Ao = Aog(Omegao, Omegao);
ro = bim1a_rhs (xo, 1, go);
bA = ro(Omegao) - Aog(Omegao, GammaDo) * uo(GammaDo);

uAo = uo;
uAo(Omegao) = cholinv (Ao) * bA;

## Extrapolation domain
# Build a domain where GP will be used for extrapolation
nXt = 110;
xt  = sort (union (xo, linspace (0, 1, nXt).'));
nXt = length (xt);

tf     = ismember (xt, [0 1]);
nodes  = 1:nXt;
GammaDt = nodes(tf);  # Dirichlet nodes
Omegat  = nodes(!tf); # Internal nodes
Omegato = nodes(ismember (xt, xo(Omegao)));

##
# Numerical inverse should be the same as cholinv(Ao)
Ko  = covLaplacian (xo(Omegao), xo(Omegao));
Koi = Ao; % == cholinv (Ko); 

##
# Assemble extrapolation matrices
Kto  = covLaplacian (xt, xo(Omegao));

## Solve extrapolation
# Using source terms in sub-domain
uGPo_ro = Kto * ro(Omegao) + mGP(xt);

##
# using temperature data in subdomian and homogenous solution
duo  = uo(Omegao) - mGP(xo(Omegao));
roGP = Koi * duo; # point sources given by FEM info
uGPo = Kto * roGP + mGP(xt);

printf ('** ell_2-norm in GP sol. using datum and source\n');
printf ("%.3f\n", sumsq (uGPo_ro - uGPo));

## Extrapolation using prior info
# Do extrapolation using the sub-domain and 
# prior info about the sources
Kt = covLaplacian (xt, xt);

##
# Extend FEM point sources with prior info
s = 4e-2;
KRto = exp (-(xt - xo.').^2 / s^2 / 2);
KRoo = exp (-(xo - xo.').^2 / s^2 / 2);
g_p = KRto * cholinv (KRoo) * go;

# This is a hack to make bim1a_rhs work with uneven meshes
tmp = linspace (min(xt),max(xt), numel(xt));
rt_tmp = bim1a_rhs (tmp, 1, g_p);
rt_p = interp1 (tmp,rt_tmp,xt);
#rt_p = KRto * cholinv(KRoo) * rhs(i)(Omegao);

uGPo_p = Kt * rt_p + mGP(xt);
# Is this equiv. to? :
# UGPo_p = Kto * Koi * (uo - mGP(xo) + f(xo)) + mGP(xt)
# UGPo_p = Kto * Koi * (duo + f(xo)) + mGP(xt)
# UGPo_p = Kto * (roGP + Koi * f(xo)) + mGP(xt)

## Extrapolation using full info
# Do extrapolation using the whole domain and FEM information
Ktx  = covLaplacian (xt, x(Omega));
Kxoi = A(Omega,Omega); % == cholinv(covLaplacian(x(Omega),x(Omega)))
uGPA = Ktx * Kxoi * (u(Omega) - mGP(x(Omega))) + mGP(xt);
uGPA_rhs = Ktx * rhs(Omega) + mGP(xt);

printf ('** ell_2-norm in FEM sol. using datum and source\n');
printf ("%.3f\n", sumsq (uGPA_rhs - uGPA));

figure(2)
clf
h = plot (x, u, 'k-;FEM;',...
          x, uGP,'k--;GP;', ...
          xo, uo,'ro;datum;', ...
          xo, uAo,'ro;FEM\_o;', ...
          xt, uGPo,'-;GP;', ...
          xt, uGPo_ro,'-;GP\_ro;', ...
          xt, uGPo_p,'-;GP\_p;', ...
          xt, uGPA,'ko;GPA;');
set (h(end), 'markersize', 0.3*get(h(2),'markersize'), 'markerfacecolor', 'auto');
axis tight


## Copyright (C) Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2019-03-24

## -*- texinfo -*-
## @defun {@var{p} =} basis_D1Taylor (@var{n}, @var{t}, @var{d})
## Basis of the Taylor expansion of a linear differential operator of degree 1
##
## @end defun

function p = basis_D1Taylor (n, t, d)

  p = zeros(length(t), length(n));

  tf = (n != 0);
  n  = n(tf);
  if !isempty (n)
    p(:,tf) = t.^(n-1) .* ( d(1) .* (t ./ n) + d(2) );
  endif

  i = find (!tf);
  if d(1) != 0
    p(:,i) = d(1);
  else
    p(:,i) = [];
  endif
endfunction

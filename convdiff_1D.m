## Copyright (C) 2020 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2020-01-10

pkg load bim

# Water
cp  = 1.91e3;  # J/kg/K
rho = 3.31e-1; # kg/m³
k   = 6e-1;    # J/s/m/K
h   = 40;      # J/s/m²/K
L   = 10;       # m
v   = 20;      # m/s

Ti  = 1;      # inlet water temp
Ts  = 60;     # pipe surface temp
Tsf = @(x)  Ti + (Ts -  Ti) * exp(-(x-0.5).^2 / 2 / 0.2^2);
#Tsf = @(x)  Ti + (Ts -  Ti) * exp(-sin((x-0.5)*pi*5).^2 / 2 / 0.2^2);

PI_h = true;   # Use adimensional parameters with h --> 1
if PI_h
  tau = rho * cp / h;         # characteristic transfer time
  a   = k / h / L^2;          # diffusion coef
  c   = 1;                    # transfer coef (sources)
  tf  = 40;                   # final time for non-stationary
else
  tau = rho * cp * L^2 / k;   # characteristic diffusion time
  a   = 1;                    # diffusion coef
  c   = h / k * L^2;          # transfer coef (sources)
  tf  = 2.4e-4;               # final time for non-stationary
endif
b = v * tau / L;              # convection coef

Nx  = 150;
msh = [linspace(0, 1, Nx).'; 1.01];

Tini = @(x)  Ti * ones(size (x));
#Tini = @(x)  Ti + (Ts -  Ti) * x;
#Tini = @(x)  Ti + 10*exp(-(x-0.5).^2/2/0.05^2);

TS = Tsf(msh);

if !exist('T', 'var')
  T0 = Tini (msh);
else
  T0 = T(end,:).';
  TS(TS < T0) = T0(TS < T0);
endif

ConvDiff = bim1a_advection_diffusion (msh, 1, 1, a, (b ./ a) * ones (Nx, 1));
Diff     = bim1a_advection_diffusion (msh, 1, 1, a, 0);
Conv     = bim1a_advection_upwind (msh, b * ones (Nx, 1));
Reaction = bim1a_reaction (msh, 1, c);
Source   = bim1a_rhs (msh, 1, c .* TS);

#A = ConvDiff + Reaction;
A = Diff + Conv + Reaction;

function dT = dTdt(T, t, b, A, msh, Source)
  dT      = Source - A * T;
  dT(1)   = 0; #b * (T(2) - T(1)) / (msh(2) - msh(1));
  dT(end) = - b * (T(end) - T(end-1)) / (msh(end) - msh(end-1));
endfunction

Nt = 50;
t  = linspace (0, tf, Nt);
lsode_options ("integration method", "adams");
lsode_options ("maximum step size", min(diff(msh)) / (b*1.05));
[T, istate] = lsode (@(T, t) dTdt(T, t, b, A, msh, Source), T0, t);

#lsode_options ("integration method", "stiff");
#T  = lsode ({@(T, t) dTdt(T, t, b, A, msh, Source), @(T, t) -A}, T0, t);
#[t, T] = ode45 (@(t, T) dTdt(T, t, b, A, msh, Source), t, T0);

clf;
h_ = plot(msh(1:end-1), T(:,1:end-1).');
c = flipud(gray(Nt+3))(3:end,:);
for i=1:Nt
  set(h_(i), 'color', c(i,:));
end
xlabel ('x/L [-]')
ylabel ('Temp [K]')

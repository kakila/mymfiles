## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function [z chi] = karman_trefftz (t, r, x0, a)
  if a > pi
    error ('angle should be lower than pi.');
  endif

  chi = r * exp (- i * t) + complex(x0(:,1), x0(:,2));
  n   = 2 - a / pi;

  ichi = 1 ./ chi;
  m    = (1 - ichi).^n;
  M    = (1 + ichi).^n;

  z = n .* (M + m) ./ (M - m);

endfunction

%!demo
%! n     = 5;
%! [x y] = meshgrid (linspace(2e-2, 1.1e-1, 5));
%! t     = linspace (0, 1, 60).' * 2 * pi;
%! for i = 1:n^2
%!   subplot(n,n,i)
%!   [z c] = karman_trefftz (t,1.1,[x(i),y(i)],2e-2);
%!   plot(z);
%!   hold on; plot(x(i),y(i),'xr'); hold off
%!   axis image
%!   set(gca, "box","off");
%!   set (gca,"xaxislocation", "origin");
%!   set (gca,"yaxislocation", "origin")
%!   set(gca, "xticklabel",{});
%!   set(gca, "yticklabel",{});
%! endfor

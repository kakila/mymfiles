## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-05-02

clear all

sample_dist =@(x,s) normpdf (x, 0, s);
sampler =@(s, sz) normrnd (0, s, sz);

Sigma = {1e-4, 10}; # sigma domain

function [s, p_sx] = bayes_update (x_obs, p_xs, p_sx, x_mesh, s_mesh)
    p_sx_val = p_xs(x_obs, s_mesh) .* p_sx(s_mesh);
    # normalization
    p_sx_pp = interp1 (s_mesh, p_sx_val, 'pp');
    p_x_val = diff (ppval (ppint (p_sx_pp), [min(s_mesh) max(s_mesh)]));
    # update prior
    p_sx = @(s) ppval (p_sx_pp, s) / p_x_val;
    # update best estimate
    [~,i] = max (p_sx (s_mesh));
    s = s_mesh(i);
endfunction

N = 3;

s_true = 2.5;
x_obs  = sampler (s_true, [N,1]);
s_obs  = std (x_obs);

prior_dist = struct ('norm', @(s) normpdf (s, s_obs, 5*s_obs), 
                     'jeff', @(s) unifpdf (s, Sigma{:}) ./ s);

x_mesh =@(s) linspace (-10*s, 10*s, 1e3).';
s_mesh = linspace (Sigma{:}, 1e3);

s = s_obs;
p_xs = sample_dist;
p_sx = prior_dist.norm;

figure (1); clf
ax_s = subplot (2,1,1);
col = spring (N+1);
hp_s = plot (s_mesh, p_sx(s_mesh));
set (hp_s, 'color', col(1,:))
axis tight
v = axis ();
axis ([v(1:3) min(v(4), 3)]);
hl_s = line (s_true, [0 axis()(4)], 'color', 'k', 'linestyle', '--');
xlabel ('sigma')
ylabel ('p(sigma | X)')
hold on

ax_x = subplot (2,1,2);
hp_x = plot(x_mesh (s), p_xs(x_mesh (s), s),'b-', NA, 0, 'xr', x_mesh (s_true), p_xs(x_mesh (s_true), s_true),'r-');
axis tight
xlabel ('x')
ylabel ('p(X | s)')

# Plot one iteration
for k=1:N
    [s, p_sx] = bayes_update (x_obs(k), p_xs, p_sx, x_mesh, s_mesh);
    # update plot
    hp_s(k+1) = plot (ax_s, s_mesh, p_sx(s_mesh));
    set (hp_s(k+1), 'color', col(k+1,:));
    axis tight
    v = axis ();
    axis ([v(1:3) min(v(4), 3)]);
    set(hl_s, 'ydata', [0 axis()(4)]);

    set(hp_x(1), 'xdata', x_mesh (s), 'ydata', p_xs (x_mesh (s), s));
    set(hp_x(2), 'ydata', zeros(k,1), 'xdata', x_obs(1:k));

    pause(0.1)
endfor

# Perfomr several iterations and look at results
replicas = 100;
S = struct ('norm', zeros (replicas, 1), 'jeff', zeros (replicas, 1));
P = struct ('norm', {cell(replicas, 1)}, 'jeff', {cell(replicas, 1)});
for j=1:replicas
  x_obs  = sampler (s_true, [N,1]);
  s_obs  = std (x_obs);
  s_norm = s_jeff = s_obs;

  prior_dist.norm =@(s) normpdf (s, s_norm, 5*s_norm);

  p_sx_norm = prior_dist.norm;
  p_sx_jeff = prior_dist.jeff;
  for k=1:N
      [s_norm, p_sx_norm] = bayes_update (x_obs(k), p_xs, p_sx_norm, x_mesh, s_mesh);
      [s_jeff, p_sx_jeff] = bayes_update (x_obs(k), p_xs, p_sx_jeff, x_mesh, s_mesh);
  endfor
  S.norm(j) = s_norm;
  P.norm{j} = p_sx_norm;
  S.jeff(j) = s_jeff;
  P.jeff{j} = p_sx_jeff;
endfor

printf ('\nInference on sigma (= %.f)\n', s_true);
printf ('Normal prior: s=%.1f, iqr=%.1f\n', median(S.norm), iqr(S.norm))
printf ('Jeffries prior: s=%.1f, iqr=%.1f\n', median(S.jeff), iqr(S.jeff))

figure(2); clf
[sn,o] = sort (S.norm, 'descend');
plot([sn S.jeff(o)],'o');
legend ('Normal prior', 'Jeffries prior')
line([1 replicas], s_true, 'linestyle', '--');
axis tight
xlabel('sigma rank')
ylabel('sigma')
figure(3); clf
hold on; 
for i=1:replicas; 
  hn = plot (s_mesh, P.norm{i}(s_mesh),'r-', ...
             s_mesh, P.jeff{i}(s_mesh),'b-'); 
endfor 
hold off
xlabel ('sigma')
ylabel ('p(sigma | X)')
legend ('Normal prior', 'Jeffries prior');
axis tight


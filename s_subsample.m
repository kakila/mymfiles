x = linspace (0,1,102).';
y = cos (2*pi*x); 

function [d2y chi] = linterperr (x, y)
  nT = length (x);

  prev = 1:(nT-2); curr = 2:(nT-1); next = 3:nT;

  # linear interpolation weight
  chi = (x(curr) - x(prev)) ./ (x(next) - x(prev));

  # Error in linear interpolation if (x(curr),y(curr)) was removed
  # It is propoertional to second derivative
  d2y       = Inf (nT, 1);
  d2y(curr) = y(curr) - y(prev) .* (1 - chi) - y(next) .* chi;
endfunction

# Clean up all exactly inteproable points
keeptf = abs (linterperr (x, y)) >= sqrt (eps);
xs = x(keeptf);
ys = y(keeptf);

x_ = xs; y_ = ys;
tol = 1e-2;
figure (1), clf
  h = plot (xs, ys, 'ko', x_, y_, 'bo', NA, NA, 'go');
  sz = get (h(1), 'markersize') * 0.5;
  set (h(2), 'markerfacecolor', 'auto', 'markersize', sz);
  set (h(3), 'markerfacecolor', 'auto', 'markersize', sz*4);

# The following iteration is not aware that changes in err are local
err = 0;
while err < tol
  err = abs (linterperr (x_, y_));
  idx = find (err < tol);

  if length (idx) > 1
    # ramdom
    #i = randi (length(idx));
    # greedy
    [~,i] = min (err(idx));
  else
    disp ('no err below tol')
    break
  endif
  idx = idx(i);
  
  set(h(2), 'ydata', y_, 'xdata', x_);
  set(h(3), 'ydata', y_(idx), 'xdata', x_(idx));
  pause(0.25)

  x_(idx) = y_(idx) = [];

  err = max (err (isfinite (err)));
endwhile

# Dynamic programming
# TODO

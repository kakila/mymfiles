## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-02-08


pkg load nurbs

T_ctrl = [0 0.15 0.3 0.97 1; ...
          1 0.95 0.62 0.4 0];
knots  = [0 0 0 1 2 3 3 3]/3;

dT_ctrl = 0.05 * randn(2, 3);
T_ctrl(:,2:4) += dT_ctrl;
crv = nrbmak (T_ctrl, knots);
nrbctrlplot (crv);

## Copyright (C) 2015 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function expr = randexpr (n,msiz=2)
 pm = toascii('+-').'.*[1 1 1]; pm(:,1) = " "; pm(:,3) = " ";
 md = toascii('*/');

 s = pm(randi(2,n-1,1),:).'(:);

 term_size = randi(msiz,n,1);
 idx = cumsum (upsamplefill (term_size(1:end-1)+1, [1 1]));

 expr = randchar(1,sum(term_size)+(n-1)*3,"yes");
 expr(idx) = s;
endfunction

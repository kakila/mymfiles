## Copyright (C) 2019 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2019-03-07

## -*- texinfo -*-
## @defun {[@var{P @var{Pm} @var{Pcond} @var{F}] =} naryjointseq (@var{x}, @var{y}, @var{cls})
## Sequence probabilities of discrete variables
##
## @end defun

function [P Pm Pcond F] = naryjointseq (x, y=[], cls=[])

  if isempty (cls)
    cls = x;
  endif

  [~, ix] = ismember (unique (x), cls);

  if !isempty(y)
    [~, iy] = ismember (unique (y), cls);
  else
    iy = ix;
  endif

  nc        = length (cls);
  F         = zeros (nc);
  F(ix, iy) = crosstab (x, y);

  P = F / length (x);

  Pm{1}   = sum (P, 2);
  Pm{2}   = sum (P, 1);
  Pcond   = P ./ Pm{1};

endfunction

c = [2,2,2,4,4,4,4,ones(1,16)];
L = [];
i = 1;
idx{1} = NA;
L(i)   = i-1;
PLD(i) = 1;
i = 2;
idx{i} = 1;
L(i)   = i-1;
PLD(i) = sum (c(idx{i-1}));

while idx{i}(1) <= length (c)
  i++;
  idx{i} = idx{i-1}(end) + (1:PLD(i-1));
  PLD(i) = sum (c(idx{i-1}));
  L(i)   = i-1;
endwhile

treeplot(cent2pred(c))

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

pkg load geometry

d = 1;
r = 5e-1;

theta = 15*pi/180;
np = 80;
v = pb = pf = zeros (np,2);
v(1,:) = [sin(theta) cos(theta)];
pb(1,:)= [0 -d];
pf(1,:)= [0 0];
for i=2:np
  pf(i,:) = pf(i-1,:) + r*v(i-1,:);
  l = createLine(pf(i,:),pb(i-1,:));
  C = [pf(i,:) d];
  tmp = intersectLineCircle (l,C);
  [~,idx] = min (sumsq(pb(i-1,:)-tmp,2));
  pb(i,:) = tmp(idx,:);
  v(i,:) = rotateVector (pf(i,:)-pb(i,:),-theta);
endfor
C = [pf d*ones(np,1)];
E = [pf pb];

figure (1)
col = jet (np);
#clf
hold on
plot (E(1,1),E(1,2),'.g',E(1,3),E(1,4),'.r')
plot (C(:,1),C(:,2),'.k',"markersize",1)
h = drawCircle (C,"linewidth",1);
arrayfun(@(i)set (h(i),"color",col(i,:)), 1:np);
h = drawEdge (E,"k");
arrayfun(@(i)set (h(i),"color",col(i,:)), 1:np);
axis tight
axis square
hold off

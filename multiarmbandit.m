## Copyright (C) 2015 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

N = 1e3+1; #number of plays

## Bandit definition
u = [0.5 0.6];
d = [0.2 0.2];

pkg load parallel

rand("state", double("abracadabra"));
Nruns = 500;
## Rewards
for i=1:Nruns
  reward{i}    = d .* randn (N-1,2) + u;
  tf_o{i}      = reward{i} == max (reward{i}, [], 2);
  optim_reward{i} = cumsum (reward{i}(tf_o{i}));

  tf = false (N-1,2);
  idx = randi(2,N-1,1);
  tf(idx==1,1) = true;
  tf(idx==2,2) = true;
  rand_reward{i} = cumsum (reward{i}(tf));
endfor

seed  = arrayfun(@(i) 1e9*rand (1, i),randi(625,Nruns,1), "UniformOutput", false);

redo = true;
if redo

  tau = 0.30338; #0.3;
  tf_sm = pararrayfun (nproc-1,@(k)MAB_sm(reward{k}, tau, seed{k}), 1:Nruns, "UniformOutput", false);
  T_sm = cell2mat(arrayfun (@(x)cumsum(reward{x}(tf_sm{x})), 1:Nruns, "UniformOutput", false));

  T_h = 0;
  K   = 0.50527; #mean (u);
  X0  = [1 0];
  tf_kim = pararrayfun (nproc-1,@(k)MAB_kim(reward{k}, X0, K, T_h,seed{k}), 1:Nruns, "UniformOutput", false);
  T_kim = cell2mat(arrayfun (@(x)cumsum(reward{x}(tf_kim{x})), 1:Nruns, "UniformOutput", false));

  ## Take mean values
  kim  = [mean(T_kim, 2) std(T_kim,0,2)];
  sm   = [mean(T_sm, 2) std(T_sm,0,2)];
  opt  = cell2mat(optim_reward);
  rnd  = cell2mat(rand_reward);
endif

## Plot
figure (1);
clf
t = (1:N-1).';
#Prepare confidence intervals
X = [t; flipud(t)];
Y = [kim(:,1)-kim(:,2); flipud(kim(:,1)+kim(:,2))];
patch (X,Y, 'g',"edgecolor","none", "edgealpha", 0,"facealpha",0.3);
Y = [sm(:,1)-sm(:,2); flipud(sm(:,1)+sm(:,2))];
patch (X,Y, 'b',"edgecolor","none","facealpha",0.3);
hold on
h = plot(t,kim(:,1),'-g',"linewidth",2,t,sm(:,1),'-b',"linewidth",2, t, mean(opt,2),'-r',"linewidth",2, t, mean(rnd,2),'-k',"linewidth",2);

xlabel ("Play #");
ylabel ("Cumulative reward");
legend (h, {"ASDM","SoftMax","Mean Optimal", "Random"}, "Location", "NorthWest")
axis tight
v = axis ();
axis([950 v(2) mean(rnd,2)(950) v(4)]);

print -dpngcairo "reward.png"

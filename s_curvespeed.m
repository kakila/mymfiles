## Traveling time along a parametrized curve driven by gravity
#
# The scripts sets the ODE of a point mass traveling along a
# parametrized curve in the parameter interval [-1, 1].
# It uses the ODE to compute the time needed to go from one extreme of the curve
# to the other.
#
##

# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-06-29

## Paramterized curve
# 
# Define the parametric representation of the curve and its derivative w.r.t. the
# parameter.
#
# $$ \vec{r}(s): [-1,1] \rightarrow \mathbb{R}^n $$
# $$ \vec{r}(s) = \begin{bmatrix} r_1(s) & r_2(s) \end{bmatrix} $$
# $$ \vec{r}\prime(s) = \begin{bmatrix} r_1\prime(s) & r_2\prime(s) \end{bmatrix} $$
#
##

% Circle
#R    =@(s) [sin(pi*s) -cos(pi*s)] + [0 1];                   % Parametric curve
#dRds =@(s) [cos(pi*s) sin(pi*s)];                  % Derivative w.r.t. parameter
#s0  = 0.5;                               % Initial value of the parameter
#vT0 = 0;                             % Initial value of tangent velocity

% Polynomial curve
p    = 1.7;
R    =@(s) [s 2*s.^4-p*s.^2];                   % Parametric curve
dRds =@(s) [ones(size(s)) 8*s.^3-p*2*s];      % Derivative w.r.t. parameter
s0  = -1;                                   % Initial value of the parameter
vT0 = 0;                             % Initial value of tangent velocity

% Exponential curve
#R    =@(s) [s exp(-2*(s+1))];                   % Parametric curve
#dRds =@(s) [ones(size(s)) -2*exp(-2*(s+1))];      % Derivative w.r.t. parameter
#s0  = -1;                               % Initial value of the parameter
#vT0 = 0;                             % Initial value of tangent velocity

## Helper functions
#
# $$ \hat{T} = \frac{\vec{r}\prime(s)}{\Vert \vec{r}\prime(s)\Vert} $$
# 
function [That dRds_norm] = tangent_versor (dRds)
  dRds_norm = sqrt (sumsq (dRds, 2));
  That      = dRds ./ dRds_norm;
endfunction

##
# The following function assemes that the accceleration field is in the 
# direction of the last component, e.g. $\vec{a} = -g\hat{y}$ in 2D.
# Then it computes the projection of the acceleration field along the tangential
# direction.
# It can be generailzed to $a_T = \vec{a} \cdot \hat{T}$.
#
function aT_T = tangent_acceleration (T, g)
  aT_T = -g * T(:,end);
endfunction

## Visualize the curve
# Plot the curve, the tangent versor, and the tangent acceleration.
#
v_fmt = {'maxheadsize', 0.2, 'autoscalefactor', 0.4};
figure (1), clf
  s100 = linspace (-1, 1, 100).';
  r = R(s100);
  
  s10 = linspace (-1, 1, 10).';
  r_ = R(s10);
  That = tangent_versor (dRds (s10));
  aT = tangent_acceleration (That, 1) .* That ;
  plot (r(:,1), r(:,2), '-k', 'linewidth', 2)
  hold on
  quiver (r_(:,1), r_(:,2), That(:,1), That(:,2), 'color', 'b', ...
    'linewidth', 1, v_fmt{:});
  quiver (r_(:,1), r_(:,2), aT(:,1), aT(:,2), 'color', 'r', ...
    'linewidth', 2, v_fmt{:});
  hold off
  axis equal

## Motion ODE
# Next we define the ODE that describes the motion of a point-mass
#
# $$ \dot{s} = \frac{v_T}{\Vert \vec{r}\prime(s)\Vert} $$
# $$ \dot{v_T} = a_T$$
#
function dqdt = curve_ode (vT_T, dRds, g=1)

  [That dRds_norm] = tangent_versor (dRds);

  aT_T = tangent_acceleration (That, g);
  
  % Parameter derivative w.r.t. time
  dsdt = vT_T ./ dRds_norm; % vT is vertical!

  dqdt = [dsdt; aT_T];
endfunction

dynsys = @(t, q) curve_ode ( q(2), dRds (q(1)) );

# Event function to detect if the point reached the end of th ecurve
function [value, isterminal, direction] = endpoint_event (t, q, tol=1e-3)
  value      = q(1) - (1 - tol);
  isterminal = 1;
  direction  = 0;
endfunction

## ODE integration 
# 
# Execute 
#
#   warning ('off', 'integrate_adaptive:unexpected_termination')
#
# to get rid of the event function warning.
#
t   = [0 10];       % Time vector

T0     = tangent_versor (dRds (s0));
q0_T   = [s0; vT0]; 

opt = odeset ('NormControl', 'on', 'RelTol', 1e-8, 'AbsTol', 1e-8, ...
  'Events', @endpoint_event);
[tq q te qe] = ode45 (dynsys, t, q0_T, opt);
s_ode  = q(:,1);
v_ode  = q(:,2) .* tangent_versor (dRds (s_ode));
nT     = length (s_ode);

## Plot the resulting motion
#
#
figure (2), clf
  tmp    = linspace (min (s_ode), max (s_ode), nT).';
  colour = interp1 (tmp, summer (nT), s_ode);
  
  subplot (2,2,1)
  r  = R (s_ode);
  r_ = R (s100);
  plot (r_(:,1), r_(:,2), '-k', 'linewidth', 2);
  hold on
  scatter (r(:,1), r(:,2), [], colour, 'filled');
  hold off
  axis off
  axis equal
  lims = axis ();
  
  subplot (2,2,2)
  plot (tq, r(:,2));
  axis tight
  set (gca, 'ylim', lims(3:4))
  ylabel ('r_2(s(t))')
  xlabel ('Time')

  subplot (2,2,3)
  plot (r(:,1), tq);
  axis tight
  set (gca, 'xlim', lims(1:2), 'xaxislocation', 'top')
  xlabel ('r_1(s(t))')
  ylabel ('Time')

  subplot (2,2,4)
  plot (tq, s_ode);
  axis tight
  ylabel ('s(t)')
  xlabel ('Time')


# random pred list
n = 3e3;
p = zeros (1, n);
for i=2:n
  p(i) = randi (i-1);
endfor
p = sort (p, 'ascend');
#p = [0 1 2 2 3 3 4];
#n = length (p);
m  = 1; # mode
mu = 1;
s  = sqrt (mu - log (m));
w = [0 lognrnd(mu, s, 1, n-1)];
#w = [];

pr = cell(n,1);
clear W
for i=1:n
  pred = i;
  do
    pr{i}(end+1) = pred;
    pred = p(pred);
  until pred == 0
  if !isempty (w)
    W(i) = sum (w(pr{i}));
  else
    W(i) = length (pr{i});
  endif
endfor

figure (1)
clf
subplot(1,6,1:2)
if isempty (w)
  hist (W(2:end), 1:max(W));
else
  hist (W(2:end), floor (0.1*n));
#  set (gca,'xscale','log');
endif
ylabel ('#')
xlabel ('Path length')
axis tight
subplot (1,6,3:6)
treeplot (p);
axis tight
axis off

#figure (1)
#clf
#treeplot(p);
#ht = get (gca,'children');
#nx = get(ht(2),'xdata');
#ny = get(ht(2),'ydata');
#for i=1:n
#  text (nx(i)+0.1,ny(i), num2str(i));
#endfor
#for i=2:n;
#  idx = [i p(i)];
#  text (mean(nx(idx))-0.1,mean(ny(idx)), num2str(w(i)));
#endfor

#px = nx(pr{1});
#py = ny(pr{1});
#hold on
#hp = plot (px,py,'-g');
#hold off
#disp ('Press a key to see next path ...')
#for i=2:n
#  set (hp, 'xdata', nx(pr{i}), 'ydata', ny(pr{i}));
#  disp (sprintf('path from %d to 1, weigth=%d.',i,W(i)));
#  pause
#endfor

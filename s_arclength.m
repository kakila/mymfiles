## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-08-16

# integral of a posivie function
# e.g. a cdf is such one function
pseed = randn (1, 5);
pspeed = conv (pseed, pseed);
psigma = polyint (pspeed);
sigma =@(t) polyval (psigma, t);

function t = polyinv (s, p)
  t = zeros (size (s));
  for i=1:length(s)
    r = roots ([p(1:end-1) p(end)-s(i)]);
    tf = (abs (imag (r)) < sqrt(eps)) & (r <= 1) & (r >= 0);
    if sum (tf) >= 1
      t(i) = min (r(tf));
    else
      t(i) = NA;
    endif
  endfor
endfunction

sigmainv =@(s) polyinv(s, psigma);

figure (1), clf
subplot (2,1,1)
  t  = linspace (0, 1, 100).';
  tl = linspace (0, 1, 10);
  hold on
  plot (t, sigma (t), 'r-', 'linewidth', 2);
  ZERO = zeros (size (tl));
  STL  = sigma (tl);
  plot ([tl; tl], [ZERO; STL], 'k-')
  plot ([ZERO; tl], [STL; STL], 'k-');
  hold off
  axis tight
  xlabel ('t');
  ylabel ('\sigma')

subplot (2,1,2)  
  s  = linspace (0, sigma(1)-eps, 100).';
  sl = linspace (0, sigma(1)-eps, 10);
  hold on
  plot (sigmainv (s), s, 'r-', 'linewidth', 2);
  ZERO = zeros (size (sl));
  TTL  = sigmainv (sl);
  plot ([TTL; TTL], [ZERO; sl], 'k-')
  plot ([ZERO; TTL], [sl; sl], 'k-');
  hold off
  axis tight
  xlabel ('t');
  ylabel ('\sigma')



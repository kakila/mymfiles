function dxdt = convection_coupled (x,t,Dx)

  Nx = size (Dx,1);
  idx = 2:Nx-1;
  x(end) = interp1 (x(idx), Nx, "extrap");

  dxdt      = zeros (length (x), 1);
  dxdt(1)   = -3*x(1) + x(end);
  dxdt(idx) = (Dx*x(1:Nx))(idx);
  dxdt(Nx+1:end) = x(2:Nx);

  tf = dxdt <= sqrt(eps);
  dxdt(tf) = 0;

endfunction

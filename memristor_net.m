## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function [gamma V] = memristor_net (t,net)

  nT = length (t);
  dt = [0 diff(t)];

  ns = length (net.sources);
  [nC n] = size (net.KCL);
  [nV n] = size (net.KVL);
  [C_i C_gamma V_v V_phi] = kirchhofmat (net);
  
  # Linear part of network
  A = zeros (nC+nV, n);
  A(1:nC,1:n-ns) = C_gamma;
  A(nC+1:end,:) = [ V_phi.*net.R(:).' V_v ];
  # Inverse 
  # TODO: Consider itrative solver like gmres
  iA = inv (A);
  nx = size (A,1);
  nm = nx - ns;
  
  b_zero = zeros (size(V_phi,1),1);
  b  = @(y) [-C_i*y; b_zero];

  F_zero_up = zeros (nC, nx);
  F_zero_rt = zeros (nV, ns);
  F =@(x,t) [F_zero_up; V_phi*diag(net.nonlin(x(1:nm),t,[],[])) F_zero_rt];
   
  x = zeros (nx, nT);
  net.nonlin([],[],net.z0(:),0);
  x(:,1) = picard (A, @(y)F(y,t(1)), b(net.input(:,1)), b(net.input(:,1)), iA);

  for i = 2:nT
    x(:,i)    = picard (A, @(y)F(y,t(i)), b(net.input(:,i)), x(:,i-1), iA);
    [~,ze] = net.nonlin(x(1:nm,i),t(i),[],[]);
    net.nonlin([],[],ze,t(i));
    
    text_waitbar(i/nT);
  endfor

  gamma = x (1:nm,:).';
  V     = x (nm+1:end,:).';
endfunction


## Planar slices of Gaussians
#
##

# Copyright (C) 2019 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2019-06-18

pkg load statistics
pkg load geometry

## Sample the plane
#
x = y = linspace (-5, 6, 50).';
[X Y] = meshgrid (x, y);

## Sample the line
#
n   = [1; 3]; n /= sqrt (sumsq (n)); % perpendicular to the line
P   = null (n.');                  % parallel to the line
xy0 = [0.0; 0.0];                    % point on the line
rng = sqrt (max (sumsq([x y], 2)));
xl  = linspace (-rng, rng, 250).';
yl  = (n.' * xy0 - n(1) * xl) / n(2);

##
# 3D plane with support on the line
plane = createPlane ([xy0.' eps], [n.' eps]);

## Gaussian on the plane
#
mu = n * 1.2 + P * 0;
theta = 0.5;
K  = 5 * (theta * n * n.' + (1 - theta) * P * P.') + 2 * (n * P.' + P * n.');
Ki = cholinv (K);
N  = reshape (mvnpdf ([X(:) Y(:)], mu, K), size (X));
Zi = mvnpdf (mu.', mu, K);

## Gaussian on the line
# By evaluation
Nl_ = mvnpdf ([xl yl], mu, K);

##
# By transformation
w    = [xl yl] * P;               % line parameter
Pt   = P.';
wmu  = Pt * mu;                   % mean on the line
nmu  = n.' * mu;                  % mean perp. to the line
Kppi = Pt * Ki * P;               % inverse covariance on line
Kpni = Pt * Ki * n;               % inverse cross-covariance
Knni = n.' * Ki * n;              % inverse covariance on the perp. to the line

w0        = w - wmu;
logp(:,1) = -1/2 * w0 .* (Kppi * w0);
logp(:,2) = w0 .* Kpni * nmu;
logp(:,3) = -1/2 * nmu * Knni * nmu;
Nl        = Zi * exp (sum (logp, 2));

wmean = Kppi \ (Kpni * nmu);
wvar  = cholinv (Kppi);

## Plot
#
figure (1); clf
  view (3)
  hold on
  % Gaussian
  mesh (X, Y, N, 'facecolor', 'none', 'linewidth', 1.5);
  axis tight
  % Slicing plane
  drawPlane3d (plane, 'facecolor', 'w', 'facealpha', 0.9);
  % Gaussian on the line
  plot3 (xl, yl, Nl, '-b', 'linewidth', 6)
  plot3 (xl, yl, Nl_, ':w')

  xlabel ('x')
  ylabel ('y')
  zlabel ('pdf')

  set (gcf, 'color', [0.9 0.9 0.9]);
  set (gca, 'color', [0.7 0.7 0.7]);
  hold off
  cmp = get(gcf, 'colormap');

Zw = trapz (Nl, w);
wm = trapz (w .* Nl, w) / Zw;
wv = trapz ((w - wm).^2 .* Nl, w) / Zw;

figure (2); clf
  hold on
  plot (w, Nl / Zw, '-', 'linewidth', 4);
  axis tight
 % plot (w, Zi/Zw * exp (logp(:,1) + logp(:,3)), '-');
 % plot (w, Zi/Zw * exp (logp(:,2) + logp(:,3)), '-');
  plot (w, normpdf(w, wmean, sqrt (wvar)), '--w');
  xlabel ('w')
  ylabel ('pdf')

figure (3); clf
  set (gcf, 'colormap', flipud (cmp))
  hold on
  contour (X, Y, N, 10);
  plot (xl, yl, '-b', 'linewidth', 3)
  %xyp = w * n.';
  %plot (xyp(:,1), xyp(:,2), '-r', 'linewidth', 3)
  quiver (0, 0, mu(1), mu(2))
  axis equal

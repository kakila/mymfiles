## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-05-02

clear all
pkg load statistics

function p_sx = bayes_update (x_obs, p_xs, p_sx, s_mesh)
    llik = zeros (size (s_mesh));
    for i=1:length (x_obs)
      llik += log (p_xs (x_obs(i), s_mesh));
    endfor
    lp_sx_val = llik + log (p_sx (s_mesh));
    # normalization
    p_sx_pp = interp1 (s_mesh, exp (lp_sx_val), 'pp');
    p_x_val = diff (ppval (ppint (p_sx_pp), [min(s_mesh) max(s_mesh)]));
    # update prior
    p_sx = @(s) ppval (p_sx_pp, s) ./ p_x_val;
endfunction

x_dist =@(x,s) normpdf (x, 0, s);
x_sampler =@(s, sz) normrnd (0, s, sz);

Sigma = {1e-4, 10}; # sigma domain

prior = @(s) unifpdf (s, Sigma{:});
prior_sampler = @(sz) unifrnd (Sigma{:}, sz);

mu_mesh = linspace (Sigma{:}, 1e3);

N = ceil (logspace (0,2,10)).' + 1; % number of samples
nN = length (N);
utility = zeros (nN,1);
p_xs = x_dist;
p_sx = prior;
nreplicas = 20;
mu_mean = mu_var = zeros (nreplicas, 1);
for k = 1:nN
  for i = 1:nreplicas
    mu = prior_sampler (1);
    x_obs  = x_sampler (mu, [N(k),1]);
    posterior = bayes_update (x_obs, p_xs, p_sx, mu_mesh);
    mu_post = posterior(mu_mesh);
    mu_mean(i) = trapz (mu_mesh, mu_mesh .* mu_post);
    mu_var(i) = trapz (mu_mesh, (mu_mesh - mu_mean(i)).^2 .* mu_post);
  endfor
  utility(k) = max (sqrt(mu_var) ./ mu_mean);
endfor

plot(N, utility,'-o');
line ([1 N(end)], 0.1);

Nsamp = N(find (utility <0.1, 1, 'first'));
mu_true = 1.25;
x_obs = x_sampler (mu_true, [Nsamp, 1]);
post = bayes_update (x_obs, p_xs, p_sx, mu_mesh);
mu_post = post(mu_mesh);
mu_mean = trapz (mu_mesh, mu_mesh .* mu_post);
mu_var = trapz (mu_mesh, (mu_mesh - mu_mean).^2 .* mu_post);

printf ('mean mu: %.1f (%.1f)\n', mu_mean, 1.69*sqrt(mu_var));


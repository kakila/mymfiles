## Copyright (C) 2013 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {[@var{table}] =} Zntable (@var{n},@var{k})
## Creates the input table for integer modulo @var{n} with @var{k} inputs
## @end deftypefn

function tab = Zntable (n, k)
  n_samples = n^2;

  if exist('OCTAVE_VERSION')
    pkg load statistics
  endif

  tab = combnk (0:n-1, k);
  tab = [tab; tab(:,k:-1:1)];
  tab = [tab; (0:n-1).'.*ones(1,k)];

  tab = sortrows (tab);
  
endfunction

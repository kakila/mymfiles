## Copyright (C) 2015 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function [A B] = arx (y, u, deg, xreg=false)

  [nT dim] = size (y);
  if length (deg>1)
    na = deg(1);
    nb = deg(2);
  else
    na = nb = deg;
  endif

  p = 1 + max (na,nb);

  idx = {1:na, 1:nb};
  X   = cell2mat (arrayfun (@(i)[y(i-idx{1}); u(i-idx{2})].', ...
                            (p:nT).', "UniformOutput", false));

  if xreg
    lambda = logspace (-6,3,50);
    Theta  = xridgereg(X,y(p:nT),lambda);
  else
    Theta = X \ y(p:nT);
  endif

  A = Theta(idx{1}).';
  B = Theta(idx{1}(end)+idx{2}).';
endfunction

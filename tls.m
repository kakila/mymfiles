## Copyright (C) 2015 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function B = tls(xdata,ydata)

  m       = length(ydata);       %number of x,y data pairs
  X       = [xdata];
  Y       = ydata;
  n       = size(X,2);          % n is the width of X (X is m by n)
  Z       = [X Y];              % Z is X augmented with Y.
  [U S V] = svd(Z,0);           % find the SVD of Z.
  VXY     = V(1:n,1+n:end);     % Take the block of V consisting of the first n rows and the n+1 to last column
  VYY     = V(1+n:end,1+n:end); % Take the bottom-right block of V.
  B       = -VXY/VYY;

end

## Copyright (C) 2018 juanpi
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## Means fo different groups of random variables
# This script illustrates that the global mean of a dataset
# compose dof smaples form two very different random varibales
# is not very significative
pkg load statistics

N1 = 1e6;
N2 = 1e5;
N = N1 + N2;

# random samples
x1 = normrnd (25, 5, N1, 1);
x2 = lognrnd (log(150), log(2), N2, 1);

# composite dataset
X = [x1; x2];

global_mean  = mean (X);
x1_mean = mean (x1);
x2_mean = mean (x2);

figure (1)
boxplot ({X,x1,x2},1,'o',1,10);
set (gca, 'xtick', 1:3, 'xticklabel', {'global', 'sector 1', 'sector 2'})
set (gca, 'yscale', 'log')
axis tight

##
# reuse the sample mean for estimations of means in a new experiment 
# with n < N samples, where n = n1 + n2 and n1 != n2
# n1 and n2 DO NOT reflect the underlying pupolations of x and x2.
n1 = 25;
n2 = n1 * 3;
n = n1 + n2;

estimated_mean = (n1 * x1_mean + n2 * x2_mean) / n;
# random samples
x1 = normrnd (25, 5, n1, 1);
x2 = lognrnd (log(150), log(2), n2, 1);
sample_mean = mean ([x1;x2]);


printf ("Global mean: %.1f\nGroup mean: %.1f\t%.1f\n", ...
  global_mean, x1_mean, x2_mean);
printf ("Samples sizes: %d\t%d\n", n1, n2);
printf ("Sample mean: %.1f\nEstimated mean: %.1f\n", ...
  sample_mean, estimated_mean);

figure (2)
boxplot ({x1,x2},1,'o',1,10);
set (gca, 'xtick', 1:2, 'xticklabel', {'sector 1', 'sector 2'})
set (gca, 'yscale', 'log')
axis tight
lg = line ([0 3], global_mean, 'color', 'r', 'linestyle', '--');
ls = line ([0 3], sample_mean, 'color', 'g', 'linestyle', '--');
le = line ([0 3], estimated_mean, 'color', 'g');
legend ([lg,ls,le],{'global avg.', 'sample avg.', 'estimated avg.'})





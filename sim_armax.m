## Copyright (C) 2015 - Juan Pablo Carbajal
## Copyright (C) 1995-2013 Friedrich Leisch
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Derived from arma_rnd.m

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2015-03-16

## -*- texinfo -*-
## @deftypefn {Function File} {@var{retval} =} armax (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn



function [y yend] = sim_armax (u, a, b, y0=[], u0=[], c=[], r=[], r0=[])

  nT = length (u);
  ny = length (a);
  nu = length (b);

  % Extend u if u0 is not given
  if isempty (u0)
    u = [zeros(nu,1); u(:)];
  else
    if length (u0) != nu
      error ("Octave:invalid-input-arg",...
             "Wrong length of previous inputs: %d != %d\n",length(u0), nu);
    endif
    u = [u0(:); u(:)];
  endif

  % Set inital state if not given
  if isempty (y0)
    y = zeros (ny,1);
  else
    if length (y0) != ny
      error ("Octave:invalid-input-arg",...
             "Wrong length of previous states: %d != %d\n",length(y0), ny);
    endif
    y = y0(:);
  endif

  idxr = [];

  if !isempty (c)
    nr = length (c);
    if isempty (r)
      r = randn (nT+nr,1);
    else
      if !isempty (r0)

        if length (r0) != nr
          error ("Octave:invalid-input-arg",...
                 "Wrong length of previous noise: %d != %d\n",length(r0), nr);
        endif
        r = [r0(:); r(:)];

      else
        r = [randn(nr,1); r(:)];
      endif

    endif
    idxr = nr - (1:nr);
  endif

  idxy = ny - (1:ny);
  idxu = nu - (1:nu);
  for i = 1:nT
    sy = y(idxy + i);
    su = u(idxu + i);
    y(ny+i) = a * sy + b * su;

    if !isempty(idxr)
      sr = r(idxr + i);
      y(ny+i) += c * sr;
    endif

  endfor

  y(1:ny) = [];
  y = y(:);

endfunction

%!shared a,b,u,y0,u0,c,r,r0
%! a = [1 -0.5 0.1];
%! b = [1 0.1];
%! c = [1 0.1 0.1];
%! u = [1;0;1;0;1];
%! y0 = [0; 1; 1];
%! u0 = 1;
%! r = randn (5,1);
%! r0 = randn (3,1);

%!test
%! sim_armax (u, a, b);
%! sim_armax (u, a, b, y0);
%! sim_armax (u, a, b, [], u0);

%!test
%! sim_armax (u, a, b, [], [], c);
%! sim_armax (u, a, b, [], [], c, r);
%! sim_armax (u, a, b, [], [], c, r, r0);

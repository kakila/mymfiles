## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

# Exact integration 1D linear system
a = -10;  # rate
y0 = 1;  # ic
u = @(t) sigmoid_train(t, [0.1 0.2; 0.3 0.5], 1e-2); # input
T = 1; # duration
t = unique([0; sort(T*rand(100,1)); T]); # observation

# Try fft
maxF = 1e3; # in Hz
Fs   = 2 * maxF;
dt   = 1/Fs;
nT   = 2 ^ nextpow2 (T/dt);
t_   = linspace (0,T,nT).';
f_   = Fs^2 / nT * (t_ - t_(nT/2 + 1));

n   = 2 ^ nextpow2 (2*nT - 1);
U   = fft (u(t_), n);

yp_a = [];
#tic
#Hf  =@(f) 1 ./ (i*2*pi*f - a);
#H   = circshift (postpad(Hf(f_),n), -nT/2+1);
#yp_a = ifft (H.*U)(1:nT);
#toc

tic
H   = fft ((t_ == 0)*0.5 + (t_ > 0).*exp (a*t_), n);
yp_ = ifft (H .* U)(1:nT) / nT;
toc

yp_c = [];
#tic
#yp_c = fftconv (exp(a*t_).', u(t_).')(1:nT).' / nT;
#toc

yp  = interp1 (t_,[yp_ yp_a yp_c],t);
y   = y0*exp (a * t) + yp;

# lsode
tic
yr = lsode (@(x,t)a*x+u(t), y0, t);
toc

#Hr = fft(exp(-a*t_));
#h = semilogy (abs([H Hr]));
#legend (h,"calc","fft")

subplot(2,1,1)
h = plot(t,yp,'.;conv;',t,yr-y0*exp(a*t),'-b;lsode;', t, ana(t), '-g;anal.;');
axis tight
subplot(2,1,2)
plot(t,y,'-r;conv;',t,yr,'-b;lsode;');
axis tight

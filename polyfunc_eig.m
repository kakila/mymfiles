## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function [x lx idx] = polyfunc_eig (p, l, N, n=1, idx=[])
  x = lx = c = NA(N,1);

  # Functional
  func = @(i,x) sum (polyval (p, x(1:i)));

  # Root finder
  r = @(c) roots([p(1:n) p(n+1)-l p(n+2:end-1) p(end)+c]);

  tmp = r(0);
  for i=1:N
    tmp_old = tmp;
    if i != 1
      tmp  = r(lx(i-1));
    endif

    tf  = abs (imag (tmp)) <= sqrt(eps); # Real root
#    tf_z    = abs(tmp) > eps;
#    [~,i_m] = min (abs (tmp - tmp_old));
#    tf      = tf_z & tmp==tmp(i_m);      # Closest nonzero root
    if all (tf)
      idx(i) = 1;
    elseif any (tf)
      idx(i) = find(tf);
    else
      break
    endif
#    tmp = tmp(idx(i));         # Closest nonzero root
    tmp = real (tmp(idx(i))); # Choose real root

    x(i)  = tmp;
    lx(i) = func (i, x);

  endfor
endfunction

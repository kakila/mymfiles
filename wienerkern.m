## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {@var{} =}  (@var{}, @var{})
## 
## @seealso{}
## @end deftypefn

function kern = wienerkern (t,x,y,l)
  
  Fx = data2fun(t,x);
  Fy = data2fun (t,y);
  T = t(end) - t(1);

  A  = quadgk (@(t)Fx(t).^2,t(1),t(end))/T;

  kern.h0 = quadgk (@(t)Fy(t),t(1),t(end))/T;
  
  kern.h1 = arrayfun(@(z)quadgk(@(t)Fy(t).*Fx(t-z).*(t-z>0),t(1),t(end)),l) / A / T;

  [s1 s2] = meshgrid (l);
  n  = length (s1(:));
  w = zeros(n,1);

  w = pararrayfun (7,@(t1,t2)quadgk(@(t)(Fy(t)-kern.h0).*Fx(t-t1).*(t-t1>0).*Fx(t-t2).*(t-t2>0),t(1),t(end)),s1(:),s2(:));
#  for i=1:n
#    t1 = s1(i);
#    t2 = s2(i);
#    w(i) = quadgk(@(t)(Fy(t)-kern.h0).*Fx(t-t1).*(t-t1>0).*Fx(t-t2).*(t-t2>0),t(1),t(end));
#  endfor
  kern.h2 = reshape (w,size (s1)) / A^2 / 2;
  kern.input = Fx;
  kern.output = Fy;
endfunction

#  % Quick approx
#  nT = length (t);
#  dt = mean (diff (t));
#  T = t(end) - t(1);
#  A = sumsq (x) / nT;

#  kern.h0     = mean(y);
#  
#  n = length (l);
#  w = zeros(n,1);
#  xd = zeros(nT,1);
#  for i=1:n
#    xd(1:(nT-l(i))) = x(1+l(i):end);
#    w(i) = mean (y .* xd);
#  endfor
#  kern.h1 = w / A;

#  [l1 l2] = meshgrid (l);
#  n  = length (l1(:));
#  w = zeros(n,1);
#  xd = zeros(nT,2);
#  y0 = y-kern.h0;
#  for i=1:n
#    xd(1:(nT-l1(i)),1) = x(1+l1(i):end);
#    xd(1:(nT-l2(i)),2) = x(1+l2(i):end);
#    w(i) = sum (y0 .* xd(:,1) .* xd(:,2));
#  endfor
#  kern.h2 = reshape (w,size (l1))*dt/A^2/2;

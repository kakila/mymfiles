## Copyright (C) 2015 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function [A B C] = armax (y, u, deg, varargin)

  [nT dim] = size (y);
  if length (deg>1)
    na = deg(1);
    nb = deg(2);
    nc = deg(3);
  else
    na = nb = nc = deg;
  endif

  p   = 1 + max (deg);
  idx = {1:na, 1:nb, 1:nc};
  T   = (p:nT).';

  #### ARX initial estimate
  X   = cell2mat (arrayfun (@(i)[y(i-idx{1}); u(i-idx{2})].', ...
                            T, "UniformOutput", false));

  Theta = X \ y(T)
  r  = y(T)-X*Theta;
  r0 = std(r)*randn(p-1,1);
  r  = [r0; r];
  ####


  lambda = logspace (-5,3,50);
  MaxIter = 500;
  param_tol = 1e-3;
  res_tol = 1e-3;
  Theta = [Theta; zeros(nc,1)];
  for i=1:MaxIter
    Theta_prev = Theta;
    r_prev     = r;

    R = cell2mat (arrayfun (@(i)r(i-idx{3}).', T, "UniformOutput", false));
    P = [X R];

    %Theta = xridgereg(P,y(T),lambda);
    Theta = P \ y(T);
    r     = [r0; y(T)-P*Theta];

    err1 = sum (abs ((Theta - Theta_prev) ./ Theta));
    err2 = sqrt (sumsq (r-r_prev) / sumsq(r));

    if err1 <= param_tol || err2 <= res_tol
      printf ('Convergence! %d\n',i);
      err1
      err2
      break;
    endif
  endfor
  if i == MaxIter
    warning ('MaxIter!');
  endif

  A = Theta(idx{1}).';
  ind = idx{1}(end)+idx{2};
  B = Theta(ind).';
  ind = ind(end)+idx{3};
  C = Theta(ind).';
endfunction

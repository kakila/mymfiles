## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

function m = permMagnetDipole(dim,Br)
%% function m = permMagnetDipole(dim,Br)
% For permanent magnets, magnetized in a single direction the equivalent
% magnetic dipole can be estimated with the formula
%
% $$ M = \frac{B_i V}{\mu_0}
%
% where, $B_i$ is the "intrinsic induction" of the magnet, in teslas,
% $V$ is the volume of the magnet, in meters cubed $m^3$.
% Bi can be calculated for many fully magnetized rare earth (Neodymium Iron
% Boron or Samarium Cobalt) ceramic or ferrite magnets, $B_i$ is 
% approximately equal to $B_r$, which is available from the magnet 
% manufacturer. For Alnico magnets, $B_i$ is approximately equal to $B_r$ 
% only if the magnet's length to width ratio (magnetized lengthwise) is 
% much greater than 1. Typical values for $B_r$ are, 0.4 teslas for ceramic
% or ferrite magnets, 1.0-1.5 teslas for Alnico and rare earth magnets.
%
% INPUT
%   dim is a 3 element vector with the dimensions of the magnet in meters
%   Br is the value of the intrinsic induction from the manufacturer in
%   Teslas.
% 
% OUPUT
%  m is the equivalent magnetic moment to be used in calculations where the
%    values are observed far away from the magnet. Far away means that the
%    distance to the magnet is much greater then the length in the
%    direction of the magnetization.
%
% Juan Pablo Carbajal
% carbajal@ifi.uzh.ch
% 02.2009

mu0= 4*pi*1e-7;

V = prod(dim);
m = Br*V/mu0;

## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2013 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {[@var{x},@var{y},@var{beta}]} =} regress_exploc (@var{A}, @var{b})
## @deftypefnx {Function File} {[@dots{}]} =} regress_exploc (@dots{}, @var{beta},@var{n})
## Data fitting with local exponential weighting.
##
## Solves the problem @command{A*x = b} using weights based on the errors of
## subsets of the matrix @var{A} and data @var{b}.
##
## @end deftypefn

function [xl y beta xk x] = regress_exploc (A, b, beta=[], nk=[])

  [nT N] = size (A);
  nb     = length (b);
  r      = rank (A);

  # If the matrix can solve the probem, then solve it
  if r >= nb
    x = A \ b;
    return;
  endif

  # If the number of submatrices is not given take a guess.
  if isempty (nk)
    nk = 100*floor (nb / (r-1));
  endif

  # Take random submatrices from the data matrix and data vectors
  ind = mat2cell (randi (nT, r-1,nk), r-1, ones (1,nk));

  # Solve each sub problem
  func =@(n) A(n,:) \ b(n);
  xk   = cellfun (func, ind, "UniformOutput",false);
  xk = cell2mat (xk);

  # For each subproblem calculate the estimation over all data.
  bk = A*xk;

  # Calculate local and global errors
  ek_loc  = (bk - b).^2;

  b_norm2 = sumsq (b);
  ek      = sum (ek_loc) / b_norm2;

  # If the parameter beta is not given take a guess.
  if isempty (beta)
    beta = 10 / mean (ek);
  endif

  # Calculate the solution by weighting each solution separatelly, globally.
  weight  = exp (-beta * ek);
  weight /= sum (weight);
  x       = sum (weight.*xk, 2);

  # Calculate the output of the different locally weighted models
  b_norm2 /= nT;
  weight   = exp (-beta * ek_loc / b_norm2);
  weight ./= sum (weight,2);
  y        = sum (weight.*bk,2);
  # Estimate the solution by projecting into the data matrix
  xl       = A \ y;

endfunction

%!demo
%! nT=1e2;
%! t=linspace(0,1,nT)';
%! Pl = @(t,n) cell2mat(arrayfun (@(x)legendrepoly(x,t),0:n,"unif",0));
%! A = Pl(t,6);
%! b = polyval([1 -3.3 4.24 -2.682 0.8665 -0.13125 0.00675 0],t);
%! b = sgolayfilt(2*randn(nT,1)-1,3,11);
%! tr=1:round(0.5*nT);
%! te = time();
%! [wl y beta wk w] = regress_exploc (A(tr,:),b(tr),1e-3);
%! disp(time()-te)
%! te = time();
%! wb = A(tr,:)\b(tr);
%! disp(time()-te)
%! h = plot(t,b,t,A*[w wl wb],":");
%! set (h,"linewidth",2);
%! axis([0 1 min(b) max(b)]);
%! legend (h,{"True","global weighted","local weighted","backslash"});
%! # -------------------------------------------------
%! #

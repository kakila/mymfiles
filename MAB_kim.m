## Copyright (C) 2015 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## Bandit definition
function tf = MAB_kim(reward,X0,K,T_h, seed)
  N = size (reward,1);
  rand("state", seed);
  randn("state", seed);

  ## Policy
  next =@(x) x > T_h;

  ## Decissions
  tf = false (N, 2);

  ## Accumulated changes
  Q  = [0,0];
  dX = [0,0]; # Change in occupancy

  for i=1:N
    ## Chosen bandit based on current state
    tf(i,:) = next(X0 + dX);
    if (all (tf(i,:))) # choose only one
      tf(i, randi (2)) = false;
    endif

    ## Update rule
    Q(tf(i,:)) += reward(i,tf(i,:)) - K;
    dX         += (Q(1) - Q(2) + 0.0*randn()) * [1 -1];
    #X(i+1,:) = X0 + dX;
  endfor
endfunction

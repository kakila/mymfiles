## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

%% Create a moving plot
t    = linspace (0,4,1e4)';
Fs   = 1/(t(2)-t(1));
data = sin (2*pi*5*t) - cos(2*pi*4.3*t) + 0.1*randn(length(t),1);

buffer_size = 1e2;

close all
figure (1)
h = plot (0,0,'-');

%% Predefined axes
time_div = 1;
out_div  = 2;

vax = [0 time_div -out_div out_div];
axis (vax);

qeue_size  = round (time_div*Fs);
window     = 1:qeue_size;
sleep_time = buffer_size / Fs;

cummt = 0;
while window(end) <= length(t)
  tic;
  set (h, "xdata", t(window), "ydata", data(window));
  axis (vax + t(window(1))*[1 1 0 0])
  drawnow
  window += buffer_size;

  time_elapsed = toc;
  if time_elapsed < sleep_time
    pause (sleep_time - time_elapsed);
  end
  cummt += max(time_elapsed,sleep_time);
end
cummt

pause (0.5)

close all
figure (1)
h = plot (0,0,'-');
axis (vax);

set(h,"xdatasource","x","ydatasource","y")

window     = 1:qeue_size;
cummt = 0;
while window(end) <= length(t)
  tic;
  x = t (window);
  y = data (window);
  refreshdata ()
  axis (vax + t(window(1))*[1 1 0 0])

  window += buffer_size;

  time_elapsed = toc;
  if time_elapsed < sleep_time
    pause (sleep_time - time_elapsed);
  end
  cummt += max(time_elapsed,sleep_time);
end
cummt

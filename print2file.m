## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

function print2file(fig,filename)
% print2file(fig,filename)
% prints the figure "fig" to "filename"
%
%!demo
%! sty = {'-','-o','o'};
%! figure(1);
%! for i=1:3
%!     subplot(1,3,i);
%!     plot(rand(10,3),sty{i})
%!     axis tight
%! end
%! print2file(1,'test.pdf')
%
% Copyright 2011 Juan Pablo Carbajal
% carbajal@ifi.uzh.ch
% Wednesday, June 08 2011
%
%
%    print2file is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    any later version.
%
%    print2file is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with print2file. If not, see <http://www.gnu.org/licenses/>.

f=figure(fig);
ax=get(f,"children");
for i=1:numel(ax)
	ca=ax(i);
	set(ca,"xgrid","on","ygrid","on");
	% Ticks fonts
	set(ca,"fontweight","bold");
	set(ca,"fontsize",20);
	% Label fonts
	xl = get(gca,'xlabel');
	yl = get(gca,'ylabel');
	set(xl,"fontweight","bold",	"fontsize",20);
    set(yl,"fontweight","bold",	"fontsize",20);
    
	plt=get(ca,"children");
	for ip = 1:length(plt)
	    cp = plt(ip);
	    if isfield(cp,'marker')
	        if ~strcmpi(get(cp,'marker'),'none')
	            set(cp,'markerfacecolor',get(cp,'markeredgecolor'));
		        set(cp,'markersize',8)
		    end
        end
	    if isfield(cp,'linestyle')
	        if ~strcmpi(get(cp,'linestyle'),'none')
	            set(cp,'linewidth',4)
		    end
		end
	end
	set(findall(f,"-property","fontsize"),"fontsize",22);
end
%% Get desired device
device=filename(end-2:end);
% EPS print always in color
if strcmp(device,'eps')
device(end+1:end+2) = 'c2';
end
print(filename,['-d' device ],"-solid")


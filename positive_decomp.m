## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

# Actuations
nT = 25; % time samples
t  = linspace (0,1,nT);
u  = [sin(2*pi*t*2); 2*exp(-t).*exp(-((t-0.25)/0.2).^2)+tanh(4*(t-0.6))];

ACT_dim = size(u,1);   % actuation dimension

# Input matrix
B = getNijhofLeverarm ();
INP_dim = size (B, 2);          % input dimesion
#INP_dim = 10;          % input dimesion
#B = round (rand (ACT_dim, INP_dim)*10) / 10;
% make sure input matrix has negative elements in each row
#neg_elem = round (0.5 * INP_dim);
#neg = [ones(1,INP_dim-neg_elem) -ones(1,neg_elem)];
#for i = 1:ACT_dim
#  B(i,:) = B(i,:) .* neg(randperm (INP_dim,INP_dim));
#endfor



Bi = pinv (B);
NB = Bi * B;
NB = eye (size (NB)) - NB;

m0 = Bi * u;

func = @(x)reshape (x, INP_dim, nT);
cost = @(x) sumsq ((m0 + NB*func(x))(:));
pos_cons = @(x) (m0 + NB*func(x))(:);

[X, OBJ, INFO, ITER, NF, LAMBDA] = sqp (zeros (size (m0(:))), cost, [], pos_cons);
w = func(X);
m = m0 + NB*w;

figure (1)
subplot (2,1,1) 
plot (t,u.','.;actuations;',t,B*m,'-;reconstruction;')
axis tight
subplot (2,1,2) 
h = plot (t,m.','.');
legend (h(1),"inputs")
axis tight


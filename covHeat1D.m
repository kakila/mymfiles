function K = covHeat1D(hyp, x, z=[], i)

% 1D Heat kernel function . The
% covariance function is parameterized as:
%
% k((x,t),(x',t')) = exp (-(x-x')^2 / 4 / s / (t - t')) / sqrt (4 * pi * (t -t'))
%
% where the s^2 is is the signal variance. The hyperparameters are:
%
% hyp = log(s)
%
% For more help on design of covariance functions, try "help covFunctions".

  if (nargin < 2)
    K = '1';
    return;
  end                  % report number of parameters

  dg   = strcmp (z,'diag');                       % determine mode

  s   = exp (hyp(1));                             % characteristic length scale

  if isempty (z)
    dt  = x(:,2).' - x(:,2);
  else
    dt  = x(:,2).' - z(:,2);
  end
  H = dt > 0;

  if dg                                                            % vector kxx
    u = ones ( size (x,1), 1);
  else
    u = eye (size (H));
    if isempty (z)                                        % symmetric matrix Kxx
      dx2 = (x(:,1).' - x(:,1)).^2;
    else                                                 % cross covariances Kxz
      dx2 = (x(:,1).' - z(:,1)).^2;
    end
    u(H) = exp (- dx2(H) ./ dt(H) / 4 / s) ./ sqrt (dt(H) * s * 4 * pi) ;
  end

if nargin<4                                                        % covariances
  K = u;
else                                                               % derivatives
  if i==1
    K = - 0.5 * u .* ( s - dx2 ./ dt / 2 ) ./ (s^3 * dt * 4 * pi);
  else
    error('Unknown hyperparameter');
  end
end

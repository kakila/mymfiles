## Copyright (C) 2015 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

pkg load odepkg

K = [1 2]*(2*pi)^2;
b = (2*pi)^2*0.1;

L = [0 1; -K(1) -b];

T = 2;
nT = 100*T;
t = linspace (0,T,nT).';

A  = 10*[1 1 1 1];
t0 = [0.1 0.3 0.5 0.7]*T;
s  = [0.1 0.1 0.1 0.1];
u  =@(t,i) A(i).*exp(-(t-t0(i)).^2/2./s(i));

y = zeros (nT,2,2);
dynsys  = @(t,x,i) L*x + [zeros(1,size(x,2)); -K(2)*x(1,:).^3+ u(t,i)];
for i = 1:4
  [t_ y_] = ode45(@(t,x)dynsys(t,x,i),[0 T], [0 0]);
  y(:,:,i) = interp1 (t_,y_,t);
endfor

DY = U = Y = zeros(nT*4,2);
Dy = zeros (nT,2,4);
for i=1:4;
  idx=nT*(i-1)+[1:nT];
  Y(idx,:) = y(:,:,i);
  U(idx,:) = [zeros(size(t)) u(t,i)];
  Dy(:,:,i) = dynsys(t.',y(:,:,i).',i).';
  DY(idx,:) = Dy(:,:,i);

end
L_ = Y \ (DY - U);

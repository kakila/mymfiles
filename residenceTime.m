## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>


## A script to implement a residence time algorithm based on
## clustering.

if false
  clear all
  pkg load fuzzy-logic-toolkit

  # Defie a DS with a modulated pitfor bifurcation
  f = @(t,x,a) a(t).*x - x.^3;

  # Define modulation of the bifurcation
  nw = 10;
  w  = randn(nw,1);
  w  = w / sqrt(sumsq(w));
  a  = @(t) (tanh (10*sin (2*pi*[1:nw]/2.*t)*w) + 1 ) / 2;

  # Generate a trajectory
  t  = linspace (0, 1, 1e2).';
  dt = t(2) - t(1);
  T  = t(end) - t(1);

  y = lsode (@(x,t)10*f(t,x,a), 0.1, t);
endif

# Calculate soft clusters
Nc = 5;
[c, softp] = fcm (y, Nc, [nan(1,3) 0]);
#[c, softp] = gustafson_kessel (y, Nc);
[c ci]     = sort (c);
softp      = softp (ci,:);

# Calculate sojourn time (residence) based on soft partition
sojtime = sum (softp,2) * dt / T;


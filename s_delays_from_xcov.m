pkg load signal
pkg load statistics

function Xd = delaycols (X, d)
  % Apply each (circular) delay d to each column of X
  Xd = X;
  nd = length (d);
  for i = 1:nd
    Xd(:,i) = circshift (X(:,i), d(i));
  end%for
end%function

t  = linspace(0, 1, 360).';
dt = t(2) - t(1);

%% Inputs
%
X      = cos (2 * pi * [2.7 1.3] .* t) .* sin (2 * pi * [1.1 2.2] .* t);

%% Outputs
%
delay  = [0.17 0.25];                 % delay in time units
idelay = round (delay / dt);          % delay in samples
Xd     = delaycols (X, idelay);
w      = [0.7; 0.3];
theta  = 0.99;
outp   =@(X, w, o, s=0.05) o * (X * w) + (1 - o) * tanh(5*(X.^3 * w)) + s*randn(rows(X),1);
y      = outp (Xd, w, theta);

%% Pairwise maximum absolute covariance
%
function [d sc c l] = maxcovdelay (X, y)
  % delay of maximum absolute cross-covariance and sign of cross-covariance
  nX = columns (X);
  d = sc = zeros (1, nX);
  for i=1:nX
    [c_ l_]    = xcov (y, X(:,i));
    lpos       = l_ >= 0;
    c(:,i)     = c_(lpos);
    l          = l_(lpos);
    [~, idx]   = max (abs (c(:,i)));
    d(i)       = l(idx);
    sc(i)      = sign (c(idx,i));
  end%for
end%function

[delay_indep sign_indep c l] = maxcovdelay (X, y);

%% Joint maximumm absolute covariance
%
function [d sc C l] = maxjointcovdelay (X, y)
  % delay of maximum absolute cross-covariance and sign of cross-covariance
  % this function only works if nX == 2
  [nT nX] = size (X);
  assert (nX == 2)

  l       = unique (round (linspace (0, nT-1, 20)));
  nl      = length (l);
  [L1 L2] = meshgrid (l);
  L       = [L1(:) L2(:)];
  nL      = length (L);
  c       = zeros (nL, 1);
  for i=1:nL
    Xd   = delaycols (X, L(i,:));
    w    = Xd \ y;
    c(i) = corr (Xd * w, y);
  end%for
  ac    = abs (c);
  mac   = max (ac);
  idx   = find (abs(ac - mac) <= sqrt(eps));
  d     = L(idx,:);
  sc    = sign (c(idx));
  C     = reshape (c, [nl nl]);
end%function

[delay_joint sign_joint C L] = maxjointcovdelay (X, y);

%% Sequential
%
function [d sc c l] = maxseqcovdelay (X, y)
  % delay of maximum absolute cross-covariance and sign of cross-covariance
  % this function only works if nX == 2
  nX = columns (X);
  d = sc = zeros (1, nX);
  for i=1:nX
    [c_ l_] = xcov (y, X(:,i));
    c       = c_(l_ >= 0);
    cmax(i) = max (abs (c));
  end%for

  [~, order] = sort(cmax, 'descend');

  c = zeros (length(c), nX);
  yres = y;
  for i=1:nX
    j = order(i);
    [c_ l_]  = xcov (yres, X(:,j));
    lpos     = l_ >= 0;
    c(:,j)   = c_(lpos);
    l        = l_(lpos);
    [~, idx] = max (abs (c(:,j)));
    d(j)     = l(idx);
    sc(j)    = sign (c(idx,j));

    Xd = circshift (X(:,j), d(j));
    yres = yres - Xd * (Xd \ yres);
  end%for
end%function

[delay_seq sign_seq cs ls] = maxseqcovdelay (X, y);

%% Distance correlation
%
function [d c l] = maxdcovdelay (X, y)
  % delay of maximum absolute distance correlation
  [nT nX] = size (X);
  l = 0:(nT-1);
  nl = length (l);
  c = zeros (nl, nX);
  d = zeros (1, nX);
  yres = y;
  for i=1:nX
    for j=1:nl
      c(j,i) = dcov (yres, circshift (X(:,i), l(j)));
    endfor
    [~, idx] = max (abs (c(:,i)));
    d(i)     = l(idx);

    Xd = circshift (X(:,i), d(i));
    yres = yres -  Xd * (Xd \ yres);
  end%for
end%function

[delay_dc dc ldc] = maxdcovdelay (X, y);

figure (1)
  subplot (5,5,([1;6;11;16]+[0:3])(:))
  #surf (L, L, abs (C), 'edgecolor', 'none')
  #view (2)
  imagesc ([min(L) max(L)], [min(L) max(L)], abs (C).');
  colormap (summer)
  line ((delay_joint(1)-0.5)*[1 1], ylim (), 'color', 'k')
  line (xlim (), (delay_joint(2)-0.5)*[1 1], 'color', 'k')
  axis xy
  axis tight
  xt = [get(gca, 'xtick') delay_joint(1)];
  yt = [get(gca, 'ytick') delay_joint(2)];
  set (gca, 'xtick', xt, 'ytick', yt)
  xax = xlim;

  subplot (5,5,[5 10 15 20])
  plot (baseline (abs (c(:,2))), l, '-r', ...
        baseline (abs (cs(:,2))), ls, '-b', ...
        baseline (dc(:,2)), ldc, '-m')
  axis tight
  line (xlim (), (delay_joint(2)-0.5)*[1 1], 'color', 'k')
  ylabel ('x2')
  ylim (xax)

  subplot (5,5,21:24)
  plot (l, baseline (abs (c(:,1))), '-r', ...
        ls, baseline (abs (cs(:,1))), '-b', ...
        ldc, baseline (dc(:,1)), '-m')
  axis tight
  line ((delay_joint(1)-0.5)*[1 1], ylim (), 'color', 'k')
  xlabel ('x1')
  xlim (xax)

Xd_indep = delaycols (X, delay_indep);
y_indep  = Xd_indep * (Xd_indep \ y); %outp (Xd_indep .* sign_indep, w) ;
Xd_joint = delaycols (X, delay_joint);
y_joint  = Xd_joint * (Xd_joint \ y); %outp (Xd_joint, w) * sign_joint;
Xd_seq = delaycols (X, delay_seq);
y_seq  = Xd_seq * (Xd_seq \ y); %outp (Xd_seq .* sign_seq, w);

Xd_dc = delaycols (X, delay_dc);
y_dc  = Xd_dc * (Xd_dc \ y); %outp (Xd_dc, w, o=theta, s=0);

printf ('True : %5d %5d\n', idelay)
printf ('Indep: %5d %5d %5.2f\n', delay_indep, corr (y_indep, y))
printf ('Joint: %5d %5d %5.2f\n', delay_joint, corr (y_joint, y))
printf ('Seq  : %5d %5d %5.2f\n', delay_seq, corr (y_seq, y))
printf ('dCov : %5d %5d %5.2f\n', delay_dc, corr (y_dc, y))
figure (2)
  plot (t, y, 'ok;output;', ...
        t, y_indep, '-r;indep;', ...
        t, y_joint, '-g;joint;', ...
        t, y_seq, '-b;seq;', ...
        t, y_dc, '-m;dc;')

## Copyright (C) 2015 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

# Single term
N         = 10;
ausdrucke = cell(3*N,1);
idx       = 1:N;
maxCoeff = 25;

p = arrayfun(@(x,y)sprintf("%d%s",x,y), ...
                              ((-1).^randi(2,N,1)).*(randi(maxCoeff,N,1)+1), ...
                              randchar(N,1,"yes"),"unif",0);

ausdrucke(idx) = arrayfun (@(i)sprintf ("%s",p{i}), idx, "unif", 0);

# Two terms Addition
idx += N;
p = arrayfun(@(x,y)sprintf("%d%s",x,y), ...
                              ((-1).^randi(2,N,1)).*(randi(maxCoeff,N,1)+1), ...
                              randchar(N,1,"yes"),"unif",0);
q = arrayfun(@(x,y)sprintf("%d%s",x,y), ...
                            randi(maxCoeff,N,1)+1,randchar(N,1,"yes"),"unif",0);
s = char([43 45](randi(2,N,1)));

ausdrucke(idx) = arrayfun (@(i)sprintf ("%s %s %s",p{i},s(i), q{i}), 1:N, "unif", 0);

# Two terms Multiplication
idx += N;
p = arrayfun(@(x,y)sprintf("%d%s",x,y), ...
                              ((-1).^randi(2,N,1)).*(randi(maxCoeff,N,1)+1), ...
                              randchar(N,1,"yes"),"unif",0);
q = arrayfun(@(x,y)sprintf("%d%s",x,y), ...
                            randi(maxCoeff,N,1)+1,randchar(N,1,"yes"),"unif",0);
s = char([42 47](randi(2,N,1)));

ausdrucke(idx) = arrayfun (@(i)sprintf ("%s %s %s",p{i},s(i), q{i}), 1:N, "unif", 0);

# Print to file
fid = fopen("eraseme.txt","w");
cellfun(@(x)fprintf(fid,"%s\n",x), ausdrucke);
fclose(fid);

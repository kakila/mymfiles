## Model based sub-sampling using dynamic programming
#
##

# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-11-16

## Synthetic data
# Generate a time series from a concrete model
x0 = [0;0.3;0.4;0.42;1];
y0 = [1;0.4;0.4;0;2];
nX = 50;
x  = unique([linspace(0,1,nX).';x0]);
nX = length (x);

## 
# We can use |interp1| to generate different type of time series.
# Try |'nearest'|, |'linear'|, |'cubic'|, |'spline'|.
# 
# Alternatively we could use a Kernel for the inteprolation
#
#   s2 = (0.05 * min (abs (diff (x0)))).^2;
#   K =@(x,y) exp (-(x - y.').^2/2/s2);
#   a = K(x0,x0) \ y0;
#   y = K(x, x0) * a;
#
# For the sake of clarity we stick to the |interp1| method.
y = interp1(x0, y0, x, 'linear');

figure (1), clf
  h = plot (x, y, 'o-', 'markerfacecolor', 'auto', 'linewidth', 2, ...
    x0, y0, 'o', 'markersize', 12);
  xlabel ('X');
  ylabel ('Y');
  axis tight
  legend (h, {'data', 'sources'}, 'location', 'northwest')

## Define cost to go
# The important parameter here is |method|. When it matches the one
# used for the generation of the data, we should obtain the lowest number of 
# samples for the same error.
#

% error fuction should take a list of indexes, a background list of indexes and 
% return the error of adding the each data in the index to the background index
% set
method  = 'linear';
fe      =@(r) sqrt(meansq (r));
cost2go =@(K, dK) -add1interperror (K, dK, x, y, method, fe);

## Subsample the original data
#
I  = (1:nX).';
dI = [1;nX];
tol = 5e-2;
tic
[J F] = subsampleDPind (I, dI, cost2go, tol);
toc
% Compute the total error
F(3:end) = -(sum (F(3:end)) - cumsum (F(3:end)));

## Plot the evolution
# To avoid  pauses,  set |interactive| to |false|
interactive = false;

nJ = length (J);
if interactive
  n = 0; 
  i = 1:(2+n);
  k = J(i);
  figure (2), clf
    subplot(2,1,1)
    h = plot (x, y, 'o-', ...
      x(k), y(k), 'o', 'markerfacecolor', 'auto', ...
      x, interp1 (x(k), y(k), x, method), '-');

    xlabel ('X');
    ylabel ('Y');
    axis tight
    subplot(2,1,2)
    hf = plot (i, F(i), 'o-', 'markerfacecolor', 'auto');
    xlabel ('# points');
    ylabel ('Error');
    xlim ([1 nJ]);
    ylim ([min(F) max(F)]);

  printf ('Press any key to add a point\n'); fflush (stdout); pause
  for n=3:nJ
    i = 1:n;
    k = J(i);
    set (h(2), 'xdata', x(k), 'ydata', y(k));
    set (h(3), 'ydata', interp1 (x(k), y(k), x, method));
    set (hf, 'xdata', i, 'ydata', F(i));
    pause
  endfor
else
  OBJ = F - min (F);
  i   = find (OBJ < tol, 1, 'first');
  idx = J(1:i);
  figure (2), clf
    subplot(2,1,1)
    h = plot (x, y, '-', 'linewidth', 2, ...
      x0, y0, 'o', 'markersize', 12, ...
      x, interp1 (x(idx), y(idx), x, method), '--', ...
      x(idx), y(idx), 'o', 'markerfacecolor', 'auto');
    xlabel ('X');
    ylabel ('Y');
    axis tight
    legend (h, {'data', 'sources', 'approximation', 'chosen'}, ...
      'location', 'northwest')

    subplot(2,1,2)
    hf = semilogy (1:nJ, OBJ + eps, 'o-');
    set (hf, 'linewidth', 2, 'markerfacecolor', 'auto');
    xlabel ('# points');
    ylabel ('Error');
    line (length (idx), ylim ());
endif

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>


po = [0.5, 1.5]; # punto donde se juntan
# Generacion de datos
l     = 2 + 4*rand(10,1) + 0.3*randn(1,100); # parametro sobre la recta
theta = pi/4 * randn(10,1) + 0.01*randn(1,100); # angulo de la recta

p(:,:,1) = po(1) + l.*cos(theta); # puntos X
p(:,:,2) = po(2) + l.*sin(theta); # puntos Y

data = [p(:,:,1)(:) p(:,:,2)(:)];

## search space
function [c M] = cost (x, data)
  M = data-x(1:2);
  M = atan2 (M(:,2),M(:,1));
  M = hist (M,1e3,1);
  c = sum (M>1e-4);
endfunction

#function [c M m s] = cost (x, data, n)
#  M = data-x(1:2)(:).';
#  M = atan2 (M(:,2),M(:,1));
#  [m s] = gmm (M,n);
#  c = 1e3*sum (s) + sumsq(x);
#endfunction

#p0 = [0 0];
[po_ c_] = fminsearch (@(x)cost(x,data,10),p0);
#[po_ c_] = sqp (p0, @(x)cost(x,data,3));
po_ = po_(:).';
figure(1)
clf
plot(data(:,1),data(:,2),'.');
hold on
plot(po(1,1),po(1,2),'og',po_(1,1),po_(1,2),'xk')
hold off
axis normal

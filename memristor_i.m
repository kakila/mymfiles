## Copyright (C) 2013 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {@var{} =} memristor_i (@var{}, @var{})
##
## @seealso{}
## @end deftypefn

function [dxdt M] = memristor_i (x, t, p)
  R = (p.R + p.r)/2;
  r = (p.R - p.r)/2;

  M =  R - r .* tanh (p.a/2 .* (x - p.qmin));

  dxdt = p.input(t);

endfunction

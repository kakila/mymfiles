## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defun {@var{h} =} violin (@var{x})
## @defunx {@var{h} =} violin (@var{x}, @var{c})
## @defunx {@var{h} =} violin (@var{hax}, @dots{})
##
## @seealso{boxplot}
## @end defun

function h = violin (ax, x = [], c = "y", varargin)

  if ~ishandle (ax)
    if ~ishold()
      clf
    endif

    if ~isempty (x)
      c  = x;
    endif
    x  = ax;
    ax = gca ();
  endif

  if ~iscell (x)
    [N Nc] = size (x);
    x      = mat2cell (x, N, ones (1, Nc));
  endif

  nb = 50; # Number of bins in histogram
  sf = 4;  # smoothing factor for kernel estimation
  [px py mx] = cellfun (@(y)build_polygon(y, nb, sf), x, "unif", 0);

  Nc    = 1:numel (px);
  Ncc   = mat2cell (Nc, 1, ones(1,Nc(end)));
  width = 0.5; # width of violins
  tmp   = cellfun (@(x,y,z)patch(ax,(width*x+z)(:),y(:),c), px, py, Ncc);
  h.violin = tmp;

  hold on
  tmp = cellfun (@(z,y)plot(ax,z,y,'.k'), Ncc, mx);
  set (tmp, "markersize", 6);
  h.mean = tmp;

  Mx = cellfun(@median, x, "unif", 0);
  tmp = cellfun (@(z,y)plot(ax,z,y,'ok'), Ncc, Mx);
  h.median = tmp;

  LUBU = cellfun(@(x,y)abs(quantile(x,[0.25 0.75])-y), x, Mx, "unif", 0);
  tmp = cellfun (@(x,y,z)errorbar(ax, x, y, z(1),z(2)), Ncc, Mx, LUBU)(:);
  # Flatten errorbar output handles
  tmp2 = allchild (tmp);
  tmp = mat2cell (tmp, ones(length(tmp),1),1);
  tmp = cellfun (@vertcat, tmp, tmp2, "unif", 0);
  h.quartile = cell2mat (tmp);

  hold off

  hor = false; # whether it is horizontal
  if hor
    structfun (@swap_axes, h);
    set (ax, "ytick", Nc);
  else
    set (ax, "xtick", Nc);
  endif

  if (nargout < 1);
    clear h;
  endif
endfunction

function k = kde(x,r)
  k  = mean (stdnormal_pdf (x / r)) / r;
  k /= max (k);
endfunction

function [px py mx] = build_polygon (x, nb, sf)
  N = size (x, 1);
  mx = mean (x);
  sx = std (x);
  X = (x - mx ) / sx;

  [count bin] = hist (X, nb);
  count /= max (count);

  r0 = 1.06 * N^(1/5);
  X  = X - bin;
  r  = sqp (r0, @(r)sumsq (kde(X,r) - count), [], [], 1e-3, 1e2);

  v  = kde (X, sf * r).';

  px = [v -flipud(v)];
  bin = bin * sx + mx;
  py = [bin; fliplr(bin)].';

endfunction

function tf = swap_axes (h)
    tmp  = mat2cell (h(:), ones (length (h),1), 1);
%    tmp  = cellfun (@(x)[x; allchild(x)], tmp, "unif", 0);
    tmpy = cellfun(@(x)get(x, "ydata"), tmp, "unif", 0);
    tmpx = cellfun(@(x)get(x, "xdata"), tmp, "unif", 0);
    cellfun (@(h,x,y)set (h, "xdata", y, "ydata", x), tmp, tmpx, tmpy);
    tf = true;
endfunction

%!demo
%! x = zeros (9e2, 10);
%! for i=1:10
%!   x(:,i) = (0.1 * randn (3e2, 3) * (randn (3,1) + 1) + ...
%!          2 * randn (1,3))(:);
%! endfor
%! h = violin (x, "c");
%! axis tight
%! set (h.violin, "linewidth", 2);
%! set (gca, "xgrid", "on");
%!
%! # -------------------------------------------------
%! # Violin plots

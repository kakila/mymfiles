## Copyright (C) 2019 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2019-03-06

## -*- texinfo -*-
## @defun {[@var{yq} @var{idx} @var{yc}]=} quantize (@var{y}, @var{N}, @var{bins})
## Quantize a signal @var{y} in @var{N} levels using user defined @var{bins}.
##
## @end defun

function [yq idx yc] = quantize (y, Nlvl, binfun=[])

  ybnd  = [min(y); max(y)];         # Bounds
  ybnd += eps (ybnd) .* [-1;1];     # Numeric Bounds
  yPP   = diff (ybnd);              # Interval (peak-to-peak)

  if isempty (binfun)
    binfun = @(z) z;
  elseif isnumeric (binfun)
    nbins = length (binfun);
    if (nbins == Nlvl) # bin centers given
      yc = binfun;
    else
      error ('Octave:invalid-input-arg', ...
        'Inconsistent number of levels and bins.\n');
    endif
  endif

  if is_function_handle (binfun)
    zb  = linspace (0, 1, Nlvl + 1).';
    yb  = yPP * binfun (zb) + ybnd(1);     # Y-bins or symbols
    yc  = (yb(1:end-1) + yb(2:end)) / 2;   # centers of bins
  endif
  # find values of signal in bins
  idx = interp1 (yc, (1:Nlvl).', y, 'nearest', 'extrap');
  # quantize signal
  yq  = yc(idx);

endfunction

%!demo
%! t    = linspace (0, 1, 5e2).';
%! phi  = sin (2*pi * 6 * t) + 0.1 * randn (size (t));
%! Nlvl = 5;
%! [phiq idx yc] = quantize (phi, Nlvl);
%! yb = [min(phi); yc; max(phi)];
%! figure (1)
%! subplot (1, 2, 1)
%! plot((0:Nlvl+1), yb, '-',1:Nlvl, yc,'ob')
%! ylabel('signal')
%! axis tight
%! set(gca, 'xtick', 1:Nlvl, 'xgrid', 'on');
%! v = axis ();
%! subplot (1, 2, 2)
%! plot (t, phi,';signal;', t, phiq, ';quantized;')
%! xlabel ('time')
%! axis tight
%! ylim (v(3:4));
%! ################################################
%! # Lineary binning a signal. This is the default behavior when
%! # no binnig function is provided

%!demo
%! t    = linspace (0, 1, 5e2).';
%! phi  = sin (2*pi * 6 * t) + 0.1 * randn (size (t));
%! Nlvl = 5;
%! binfun = @(x) 4 * (x - 0.5).^3 + 0.5;
%! [phiq idx yc] = quantize (phi, Nlvl, binfun);
%! yb = [min(phi); yc; max(phi)];
%! figure (1)
%! subplot (1, 2, 1)
%! plot((0:Nlvl+1), yb, '-',1:Nlvl, yc,'ob')
%! ylabel('signal')
%! axis tight
%! set(gca, 'xtick', 1:Nlvl, 'xgrid', 'on');
%! v = axis ();
%! subplot (1, 2, 2)
%! plot (t, phi,';signal;', t, phiq, ';quantized;')
%! xlabel ('time')
%! axis tight
%! ylim (v(3:4));
%! legend ()
%! ################################################
%! # Binning a signal with an monotonic function [0,1] --> [0,1]

%!demo
%! t    = linspace (0, 1, 5e2).';
%! phi  = sin (2*pi * 6 * t) + 0.1 * randn (size (t));
%! Nlvl = 10;
%! binfun = quantile (phi, linspace (1/Nlvl, 1 - 1/Nlvl, Nlvl)).';
%! [phiq idx yc] = quantize (phi, Nlvl, binfun);
%! yb = [min(phi); yc; max(phi)];
%! figure (1)
%! subplot (1, 2, 1)
%! plot((0:Nlvl+1), yb, '-',1:Nlvl, yc,'ob')
%! ylabel('signal')
%! axis tight
%! set(gca, 'xtick', 1:Nlvl, 'xgrid', 'on');
%! v = axis ();
%! subplot (1, 2, 2)
%! plot (t, phi,';signal;', t, phiq, ';quantized;')
%! xlabel ('time')
%! axis tight
%! ylim (v(3:4));
%! legend ()
%! ################################################
%! # Binning a signal with monotonic bin centers obtained from
%! # the signal's quantiles.


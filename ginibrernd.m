## Copyright (C) 2017 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2017-12-24

## -*- texinfo -*-
## @defun {@var{[x y]} =} ginibrernd (@var{N}, @var{s}=0.5)
##  Samples from a Ginibre point process
##
## @seealso{randn}
## @end defun

function [x y] = ginibrernd (N, s = 0.5)

  l  = s  * eig (complex (randn (N), randn (N)));
  x  = real (l);
  y  = imag (l);

endfunction

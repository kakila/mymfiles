nT = 100;
t  = linspace (0,1,nT);
h  = t(2)-t(1);
sh = sqrt (h);
Sk = (-1).^(rand(1,nT-1) > 0.5);
Wk = sh * randn(1,nT-1);
Dm = (Wk - Sk * sh);
Dp = (Wk + Sk * sh);

tau = 1;
var = 1e-6;
kap = 1e-6;
del = 1/30;

a =@(t,s) - s / tau;
b =@(t,s,u) sqrt (2 / tau * (var + (kap*u(t-del)).^2));
u =@(k) sin(pi*k).^4;

x = zeros (nT,1);
for k=1:nT-1
  A  = a (t(k), x(k)); B = b (t(k), x(k), u);
  K1 = h * A + Dm(k) * B;
  x_ = x(k) + K1;
  t_ = t(k) + h;
  A  = a (t_, x_); B = b (t_, x_, u);
  K2 = h * A + Dp(k) * B;

  x(k+1) = x(k) + 0.5 * (K1 + K2);
endfor

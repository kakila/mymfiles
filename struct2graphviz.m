## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {@var{str} =} struct2graphviz (@var{s}, @var{file})
## Converts a structure into a graphviz file.
##
## @var{file} defines the fullname of the file to create.
## The content of @var{file} is returned in @var{str}.
##
## Example:
## @example
## s = struct("cell",[],"array",[],"string","hola","substruct",struct("double",1,"array",zeros(2)));
## s.cell={1,2};
## str = struct2graphviz(s,"test.gv");
## system("dot -Tpng test.gv > test.png");
## imshow ("test.png");
## @end example
##
## @end deftypefn

function str = struct2graphviz (s,file)

  fout = fopen (file,"w");
  
  fldname = fieldnames (s);
  nf      = numel (fldname);
  
  # Parse options
  fontsize = 12;
  
  body_str = sprintf (["## Created by GNU Octave %s\n" ...
  "## Command to get the layout: " ...
  "'dot -Tpng thisfile > thisfile.png'\n\n", ...
  "digraph G {\n\n## Configuration\n%%s}"], ...
                      version );
  g_config_str = sprintf (["graph [ " ...
                         'fontsize=%d ' ...
                         'labelloc="t" ' ... 
                         'label="" ' ...
                         'splines=true ' ...
                         'overlap=false ' ...
                         'rankdir="TB" '...
                         "];\n%%s"], ...
                         fontsize);
  body_str = sprintf (body_str, g_config_str);
  
  n_config_str = sprintf (["node [" ...
                         'style="filled,bold" ' ...
                         'penwidth=1 ' ... 
                         'fillcolor="white" ' ...
                         'fontname="Courier New" ' ...
                         'overlap=false ' ...
                         'margin=0' ...
                         "];\n%%s" ...
                         ]);
  
  body_str = sprintf (body_str, n_config_str);
  
  e_config_str = sprintf (["edge [" ...
                         'penwidth=1 ' ...
                         'arrowType="none" ' ...
                         "];\n%%s" ...
                         ]);
  
  body_str = sprintf (body_str, e_config_str);

  body_str = sprintf (body_str, "ratio=compress;\n\n%s");
  
  # FIXME: should be variable name?
  name = "struct";
  # Node configuration
  body_str = sprintf (body_str, ...
               sprintf(['"%s" ' "[%s];\n%%s"], ...
               name, struct_str (s,name) ) );
  body_str = node_format_str (s,name,body_str);
  
  # Graph structure
  body_str = sprintf (body_str, "\n\n## Graph content\n%s");
  body_str = graph_structure_str (s, name, body_str);

  str = sprintf (body_str,"\n");
  
  fprintf (fout,"%s", str);
  fclose (fout);
endfunction

function str = node_format_str (s, parent, str)
  fldname = fieldnames (s);
  nf      = numel (fldname);

  for f=1:nf
    fld = s.(fldname{f});
    node_name = [parent "_" fldname{f}];
    switch class (fld)
    
      case "struct"
        str = sprintf (str, ...
                       sprintf(['"%s" ' "[%s];\n%%s"], ...
                       node_name, struct_str (fld,fldname{f}) ) );
        str = node_format_str (fld, node_name, str);
      otherwise
        # FIXME: check within cell for structs?
        str = sprintf (str, ...
                       sprintf(['"%s" ' "[%s];\n%%s"], ...
                       node_name, basic_str (fld,fldname{f}) ) );
    endswitch
  
  endfor # over fields

endfunction

function s = fld_fmt ()
  s = ['label=<' ...
       '<table border="0" cellborder="0" cellpadding="3">' ...
       '<tr><td bgcolor="black" align="center" colspan="2">' ...
       '<font color="white">%s</font></td></tr>' ...
       '<tr><td align="left">class: %s </td></tr>' ...
       '%s' ...
       '</table>' ...
       '> '];
endfunction

function s = basic_str (d,name)
  s   = fld_fmt ();
  typ = class (d);
  if !isempty (d)
    siz = sprintf ("%dx",size (d));
    siz = siz(1:end-1);
  else
    siz = "undefined";
  endif

  str = sprintf('<tr><td align="left">size: %s </td></tr>',siz);

  s = sprintf (s, name, typ, str);
  s = [s 'shape=box'];
endfunction

function s = struct_str (d,name)
  s   = fld_fmt ();
  typ = class (d);

  if !isempty (d)
    siz = sprintf ("%dx",size (d));
    siz = siz(1:end-1);
  else
    siz = "undefined";
  endif

  str = sprintf('<tr><td align="left">fields: %d </td></tr>', numel(fieldnames(d)));
  if !isscalar (d)
    typ = [typ " array"];
    str = [str ...
    sprintf('<tr><td align="left">size: %d </td></tr>', siz)];
  endif

  s = sprintf (s, name, typ, str);
  s = [s 'shape=folder fillcolor=gray'];
endfunction


function str = graph_structure_str (s, parent, str)
  fldname = fieldnames (s);
  nf      = numel (fldname);

  for f=1:nf
    child = [parent "_" fldname{f}];
    fld = s.(fldname{f});
    if isstruct (fld)
      str = sprintf (str, ...
                     sprintf ("%s -> %s;\n%%s", ...
                     parent, child));
      str = graph_structure_str (fld, child, str);
    else
      str = sprintf (str, ...
                     sprintf ("%s -> %s;\n%%s", ...
                     parent, child));
    endif
    
  endfor # over fields

endfunction

%!demo
%! s = struct("cell",[],"array",[],"string","hola","substruct",struct("double",1,"array",zeros(2)));
%! s.cell={1,2};
%! str = struct2graphviz(s,"test.gv");
%! system("dot -Tpng test.gv > test.png");
%! imshow ("test.png");


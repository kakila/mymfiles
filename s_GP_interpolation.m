## Copyright (C) 2015 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

clear all

len     = 2; # correlation length
pow     = 2; # exponent of argument of exponential
covfun  = @(t,r) exp (- ((t-r)/len).^pow);
meanfun = @(t) 1 - 2*t; # Hypothetical mean function

# Observations
t  = [0.2 0.3 0.4 0.6 0.7 0.9].';
tf = t < 0.5;
y  = sin (t*2*pi) .* tf + tanh (10 * (0.5 - t)) .* !tf;
N  = length(y);

sigmaObs = 0e0; # Regularization: in case we want to approximate
                # instead of inteprolate

y   = y + sigmaObs * randn (N,1); # Noisy observations
dy  = y - meanfun (t);            # data withtout the hypothetized mean function
Kxx = covfun (t, t.');

# prediction
tp   = sort ([t; linspace(0,1,100).']);
Kxxp = covfun (t, tp.');

Ky       = Kxx + sigmaObs * eye (N);
L        = chol (Ky, "lower");
alpha    = L' \ (L \ dy);
v        = L \ Kxxp;

yp    = Kxxp.' * alpha;
Kxpxp = covfun (tp, tp.');
K     = Kxpxp - v'*v; # to see variance

ypm = meanfun (tp);
#plot (t,y,'x;Obs;',
plot(tp,yp + ypm,sprintf('-;Pred l=%.1g;',len));#, tp, ypm, ':;mean;');
axis ([0 1 -1 1]);

xlabel ("input");
ylabel ("output");

st_names = {'s/Js/P',
            's/Js/-Ps*',
            's/-Js*/P',
            'h/Js/-Ph*',
            'h/-Jh*/P'};
            # 'h/-Jh*/-Ph*' Weekends
nS = numel(st_names);
N  = nS^5;
weeks = cell2mat ( nthargout (1:5, @ind2sub, nS * ones (1,5), (1:N).') );
assert (size(weeks,1),N);

### filter impossible weeks

#Only weeks with one 1st state (Mom day)
tf = arrayfun(@(i) sum(weeks(i,:)==1)==1,1:N);
weeks(!tf,:) = [];
N = size (weeks,1);

#Jeannete must not work 2 days
tf = arrayfun(@(i) sum(weeks(i,:)==3 | weeks(i,:)==5)==2,1:N);
weeks(!tf,:) = [];
N = size (weeks,1);

#JPi must not work 2 days
tf = arrayfun(@(i) sum(weeks(i,:)==2 | weeks(i,:)==4)==2,1:N);
weeks(!tf,:) = [];
N = size (weeks,1);

# Jeannette doesn't stays in sargans when she takes care of her
tf = arrayfun(@(i) any(weeks(i,:)==3),1:N);
weeks(tf,:) = [];
N = size (weeks,1);

## No Friday or Monday OR three in a row in middle of week
tf_m = arrayfun(@(i)any(weeks(i,1)==[1,2,4]),(1:N).');
tf_f = arrayfun(@(i)any(weeks(i,5)==[1,2,4]),(1:N).');
W_mf = weeks(!(tf_m|tf_f),:);

# No 3 in a row for Jeannette
tochar = @(x) arrayfun(@(i)char(x(i,:)),1:size(x,1),"unif",0);
inarow3 = tochar([perms([1,2,4]); [2,2,2]; [4,4,4]; perms([2 2 4]); perms([4 4 2]);
                  perms([4 4 1]); perms([2 2 1])]);
tf = arrayfun(@(i) any(cellfun(@(x)!isempty(x),strfind(char(weeks(i,:)),inarow3))) ,1:N);
weeks(tf,:) = [];
N = size (weeks,1);

function idx = week2idx (week)
  n = size (week,1);
  idx = [week(1:end-1); week(2:end)].';
endfunction

function [c, cost, T] = weekcost(week)
  persistent T;
  if isempty (T)

#    T = [NA, 1, 0, 6, 5, 5;
#         0, 0, 1, 4, 3, 3;
#         0, 0, 0, 6, 5, 5;
#         0, 0, 0, 0, 2, 2;
#         0, 0, 0, 0, 0, 0;
#         0, 0, 0, 0, 0, 0;];
    T = [NA, 1, 0, 6, 4, 4;
         0, 0, 1, 4, 1, 1;
         0, 0, 0, 6, 4, 4;
         0, 0, 0, 0, 1, 1;
         0, 0, 0, 0, 0, 0;
         0, 0, 0, 0, 0, 0;];
    T += T';
  endif

  idx = week2idx(week);
  idx = [6 week(1); idx; week(end) 6];
  n = size (idx, 1);
  cost = zeros (1,n);
  for i=1:n
    cost(i) = T(idx(i,1),idx(i,2));
  endfor

  c = sum(cost);

endfunction

function str = week2str (week)
  for i=1:5
    switch(week(i))
      case 1
        str{i} = 'Mami (Sargans)';
      case 2
        str{i} = 'JPi has her (Sargans)';
      case 3
        str{i} = 'Jeannette has her (Sargans)';
      case 4
        str{i} = 'JPi has her (Zürich)';
      case 5
        str{i} = 'Jeannette has her (Zürich)';
    endswitch
  endfor
endfunction

function str = who_travels (week)
  idx = week2idx(week);
  idx = [6 week(1); idx; week(end) 6];
  n = size (idx, 1);
  nn = {'JPi','Jeanni'};
  dr = {'(ZH -> Sg)', '(Sg -> ZH)'};
  for i=1:n
    str{i} = 'Nobody';
    ff = NA;
    if (any(idx(i,1)==[1,3,4,5,6]) && idx(i,2)==2)
      ff = 1;
    elseif (idx(i,1)==2 && any(idx(i,2)==[1,3,4,5,6]))
      ff = -1;
    elseif (any(idx(i,1)==1:4) && any(idx(i,2)==[5,6]))
      ff = -2;
    elseif (any(idx(i,1)==[5,6]) && any(idx(i,2)==1:4))
      ff = 2;
    endif
    if (idx(i,1)==2 && any(idx(i,2)==5:6))
      str{i} = 'Both (Sg -> ZH)';
      continue;
    endif
    if (any(idx(i,1)==[1,3]) && idx(i,2)==4)
      str{i} = 'Stupid!';
      continue;
    endif
    if (idx(i,1)==4 && any(idx(i,2)==[1,3]))
      str{i} = 'Stupid!';
      continue;
    endif

    if (any(idx(i,1)==5:6) && idx(i,2)==2)
      str{i} = 'Both (ZH -> Sg)';
      continue;
    endif

    if !isna (ff)
      ff2 = (ff>0)*1 + (ff<0)*2;
      str{i} = sprintf("%s %s",nn{abs(ff)},dr{ff2});
    endif
  endfor
endfunction

function [jj, jp] = countravel(week)
  str = who_travels(week);
  ## Jeannette
  jj = sum(cellfun(@(x) strcmpi(x(1:2),'je') | any(x(1)==['B','S']) , str));
  jp = sum(cellfun(@(x) strcmpi(x(1:2),'jp') | any(x(1)==['B','S']), str));
endfunction
## travel based cost
c = arrayfun(@(i)weekcost(weeks(i,:)),(1:N).');
[C,order] = sort(c,'ascend');
# Ordered weeks
W = weeks(order,:);

## Order by time in sargans
s_cost = [1, 2, 1, 1, 0];
s = sum (s_cost(W), 2);
[S,s_order] = sort(s,'ascend');
W_s = W(s_order,:);

## Order by equal travels
idx_e=[];
for i=1:N
  [j, p] = countravel(weeks(i,:));
  if (j==p)
    idx_e(end+1,1:3) = [i j p];
  endif
endfor
W_e = weeks(idx_e(:,1),:);
ne = size(W_e,1);
c = arrayfun(@(i)weekcost(W_e(i,:)),(1:ne).');
[C_e,order_e] = sort(c,'ascend');
## Top 10
W_e = W_e(order_e,:);

## Order by Sargans cost
s = sum (s_cost(W_e), 2);
[S_e,order_es] = sort(s,'ascend');
W_e = W_e(order_es,:);

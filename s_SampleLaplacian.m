## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-01-26

pkg load bim
pkg load stk
#pkg load gpml
#pkg load statistics

nXd = 20;
xd = linspace (0, 1, nXd).';

nX = 100;
x  = unique([linspace(0, 1, nX).'; xd]);
nX = length (x);

tf     = ismember (x, xd);
nodes  = 1:nX;
GammaD = nodes(tf);
Omega  = nodes(!tf);
xd     = x(nodes(GammaD));

ifix = ismember (xd, [0 1]);
GammaDfix = GammaD(ifix);
GammaDrnd = GammaD(!ifix);

nT = 1;
T = zeros (nX,nT);
# BC
T(GammaDfix,:) = [ones(1,nT); zeros(1,nT)];

# Random internal Dirichlet values
#T(GammaDrnd,:) = rand (length(GammaDrnd), nT);
#g   = zeros (nX, nT);

# Random Gaussian sources at xD
# release rnd nodes
Omega  = union (Omega, GammaDrnd);
xrnd = x(GammaDrnd);
dim = length (xrnd);
sig =@(a,b) 0.5 * ( 1 + tanh (b * (xrnd - a)) );
abox = 0.15*[zeros(1,dim); sig(0.25,-10).'];
alpha = stk_sampling_maximinlhs (nT, dim, abox).data.';
%alpha = unifrnd (-30,40,[length(xrnd), nT]);

#Phi = normpdf(x - xrnd.', 0, 0.1);
#g   = Phi * alpha;
g = (0*(x < 0.2) - 2*(x > 0.0 & x < 0.3) + 2.0*x)*0.3;
 
A   = bim1a_laplacian (x, 1, 0.1);
rhs = zeros (nX, nT);
if any (abs(g(:)) > eps)
  for i=1:nT
    rhs(:,i) = bim1a_rhs (x, 1, g(:,i));
  endfor
endif
#b = rhs(Omega,:) - A(Omega, GammaD) * T(GammaD,:);
b = rhs(Omega,:) - A(Omega, GammaDfix) * T(GammaDfix,:);

T(Omega,:) = A(Omega, Omega) \ b;
nTneg = sum(any(T<0),2);
nTovr = sum(any(T>1),2);
if nTneg != 0 || nTovr !=0;
  warning (sprintf ("T outside feasible range! (%d < 0, %d > 1)\n",...
           nTneg, nTovr));
endif

clf
subplot(2,1,1)
if nT == 1
  h = plot(x, T, 'o-', x(GammaDfix), T(GammaDfix,:),'or', ...
         x(GammaDrnd), T(GammaDrnd,:),'og');
  set(h(1),'markersize', 0.5 * get(h(2),'markersize'));
else
  h = plot(x, T, '-');
endif
axis tight
subplot(2,1,2)
hg = plot(x, g);
axis tight
for i=1:nT
  set(hg(i), 'color',get(h(i),'color'));
endfor



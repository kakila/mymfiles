## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-05-09

## -*- texinfo -*-
## @defun {@var{alpha} =} central_angle (@var{alpha})
## Maps the interval [0, n*2*pi] into [0, pi] U [-pi, 0) 
## @end defun

function alpha = central_angle (alpha)

  % add eps(pi) to pi to make sure that pi --> pi
  n     = floor (alpha / (pi + eps (pi)));
  s     = ((-1).^n - 1) / 2;
  alpha = alpha + (s - n) * pi;

endfunction

%!demo
%! t = linspace (0, 3 * 2*pi, 100).';
%! tc = central_angle (t);
%! plot (t, tc, 'o');
%! axis([-eps 3*2*pi -pi+eps pi+eps]);
%! grid on
%! xlabel ('angle')
%! ylabel ('central angle')
%! set (gca, 'xtick', 0:pi:max(t), 'xticklabel', ...
%! {0,arrayfun(@(x)sprintf('%.1f x \\pi', x), 0.5:0.5:3, 'unif', 0)})
%! set (gca, 'ytick', -pi:pi/2:pi, 'yticklabel', ...
%! arrayfun(@(x)sprintf('%.1f x \\pi', x), -1:0.5:1, 'unif', 0))


## Binarization of continous signal
# Study how the binrization of a continous signal affects the
# joinr probability distribution of the binary variables
##

# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-08-24

pkg load statistics
close all

## Binarize function
#
function b = binarize (y, lvl, t = [])
  if is_function_handle (lvl)
    lvl = lvl (y, t);
  endif
  b = lookup (lvl, y);
endfunction

## Plot joint probability
#
function h = jointplot (P, sz=20)
  [nr, nc] = size (P);

  szlim    = [0 3] * sz.^2;
  P        = P(:);
  markersz = 1 + szlim(1) + P * szlim(2);

  x = 0:(nr-1);
  y = 0:(nc-1);
  [X Y] = meshgrid (x, y);

  ax = gca ();
  htmp = scatter (ax, X(:), Y(:), markersz, 'k', 'filled');
  set (ax, 'xtick', x, 'ytick', y);
  axis ([x([1 end]) y([1 end])] + 0.2 * [-1 1 -1 1], 'ij');
  grid on
  if (nargout > 0)
    h = htmp;
  endif
endfunction

## Signal
#
nT = 1e3;
%y  = unifrnd (0, 1, [nT,1]);
%y  = normrnd (0, 1, [nT,1]);
t  = linspace (0, 1, nT).';
dt = t(2) - t(1);
y = ones (nT,1);
for i=2:nT
  y(i)  = (1 - 2*dt) * y(i-1);
endfor

# sequence
stept = 0;
step = min (max (floor (stept / dt), 1), floor (nT / 2));
cur = 1:(nT-step);
nex = cur + step;

nl = 10;
ylimit = [min(y), max(y)];
lvl = linspace (ylimit(1), ylimit(2), nl).';
[l1 l2] = meshgrid (lvl);
l1 = vech (tril (l1, -1) + diag (NA (nl,1))); l1 = l1(!isna (l1));
l2 = vech (tril (l2, -1) + diag (NA (nl,1))); l2 = l2(!isna (l2));
lvl = [l1(:) l2(:)];
[nl diml] = size (lvl);

P = Pnexcur = zeros (nl, diml+1, diml+1);
Pcur = zeros (nl, diml+1, 1);
Pnex = zeros (nl, 1, diml+1);
cf = dep = zeros (nl, 1);
for i=1:nl
  b        = binarize (y, lvl(i,:));
  [p pm pycondx] = naryjointseq (b(cur), b(nex), 0:diml);
  P(i,:,:)       = p;
  Pcur(i,:,1)    = pm{1};
  Pnex(i,1,:)    = pm{2};
  Pnexcur(i,:,:) = pycondx;
  cf(i)          = cov (b(cur), b(nex));
  dep(i)         = norm (p - pm{1}*pm{2});
endfor
cf(isnan(cf)) = 0;
d = (diml+1) * (dep - min(dep)) / (max(dep) - min(dep));
p = sum(sum(P > 1e-2, 3), 2);
[~,i] = max (d + p);
b = binarize (y, lvl(i,:));
Pm = squeeze(P(i,:,:));

if diml == 1
  figure (1)
    subplot (2, 2, 1)
    plot (lvl, dep, lvl(i), dep(i), 'sr');
    axis tight
    ylabel ('dependence')
    xlabel ('level')

    subplot (2, 2, 2)
    plot (lvl, cf, lvl(i), cf(i), 'sr');
    axis tight
    ylabel ('covariance')
    xlabel ('level')

    subplot (2, 2, 3)
    jointplot (Pm);
    xlabel ('x(n)')
    ylabel (sprintf('x(n+%d)', step))

    subplot (2, 2, 4)
    by = ylimit(1) + (ylimit(2) - ylimit(1)) * b;
    hold on
    area (t, by, lvl(i));
    plot (t, y, 'o-')
    hold off
    xlabel ('t')
    ylabel ('signal')
    axis tight
else
  figure (1)
    subplot (1, 2, 1)
    jointplot (Pm);
    xlabel ('x(n)')
    ylabel (sprintf('x(n+%d)', step))

    subplot (1, 2, 2)
    by = ylimit(1) + (ylimit(2) - ylimit(1)) * b / (diml+1);
    hold on
    plot (t, by);
    plot (t, y, 'o-');
    for j=1:length(lvl(i,:))
      line (t([1 end]), lvl(i,j)*[1 1])
    endfor
    hold off
    xlabel ('t')
    ylabel ('signal')
endif

Nx  = 150;
msh = [linspace(0, 1, Nx).'; 1.01];

a = 1e-4;
b = 100;
f = @(x)10 * exp(-(x-0.5).^2 / 2 / 0.05^2);

ConvDiff = bim1a_advection_diffusion (msh, 1, 1, a, b * ones (Nx, 1));
Diff     = bim1a_advection_diffusion (msh, 1, 1, a, 0);
Conv     = bim1a_advection_upwind (msh, b * ones (Nx, 1));
Reaction = bim1a_reaction (msh, 1, 1);
Source   = bim1a_rhs (msh, 1, f(msh));

Ta = (ConvDiff + Reaction) \ Source;
Tb = (Conv + Diff + Reaction) \ Source;

figure(1)
plot(msh(2:end-1), Ta(2:end-1), '-', msh(2:end-1), Tb(2:end-1,:), '--')

ConvDiff2 = bim1a_advection_diffusion (msh, 1, 1, a, (b/a) * ones (Nx, 1));

Tc = (ConvDiff2 + Reaction) \ Source;

figure(2)
plot(msh(2:end-1), Tc(2:end-1), '-', msh(2:end-1), Tb(2:end-1,:), '--')

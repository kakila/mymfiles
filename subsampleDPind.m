## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-11-09

## -*- texinfo -*-
## @defun {@var{} =} subsampleDPind (@var{}, @var{})
## 
## @seealso{}
## @end defun

function [J F] = subsampleDPind (I, dI, cost, tol=sqrt(eps), direction=1)

  if direction != 1
    fun =@(k,dk) -fun(k,dk);
  endif
  R = setdiff (I, dI);
  N = length (R);
  M = length (dI);
  J = F = zeros (N+M, 1);

  F(1:M) = NA; 
  J(1:M) = dI;
  for n =1:N
    k      = M + n;
    kprev  = 1:(k-1);
    [j, c] = subsampleDPind_step (R, J(kprev), cost);
    J(k) = j;
    F(k) = c;
    if abs (c) < tol
      J = J(1:k);
      F = F(1:k);
      break; 
    endif

    R = setdiff (R, j);
    if isempty (R); break; endif
  endfor

endfunction

function [j, c] = subsampleDPind_step (K, dK, cost)

  [c, j] = min (cost(K, dK));
  j      = K(j);

endfunction

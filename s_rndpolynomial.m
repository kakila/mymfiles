## Copyright (C) 2020 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2020-05-01

pkg load symbolic
pkg load stk

# Compute coefficients formula
syms r1 r2 r3 r4 r5 x
polysym = (x - r1) * (x - r2) * (x - r3) * (x - r4) * (x - r5);
psym = coeffs (polysym, x);

# Evaluate at random roots
deg = 5; 
np  = 10;                     # number of polynomaisl to sample
#r   = sort(rand (np, deg), 2); 
r   = sort ( (stk_sampling_randomlhs(np, deg).data + 1) / 2, 2);
p   = zeros (np, deg+1);
S   = zeros (np, 1);
for i = 1:np 
  [r1 , r2, r3, r4, r5] = mat2cell (r(i,:), 1, ones (1, deg)){:}; 
  p(i,end:-1:1)         = eval (psym);                              # order for polyval
  # shift it to zero min
  pval(:,i) = polyval (p(i,:), t); 
  p(i,end)  = p(i,end) - min (pval(:,i)); 
  # compute integral
  S(i) = diff (polyval (polyint (p(i,:)), [0 1]));
endfor # over roots

# plot the polynomials
nt   = 25;
t    = linspace (0, 1, nt);
pval = zeros(nt, np);
for i = 1:np 
  pval(:,i) = polyval (p(i,:), t);
endfor # over poylnomial coefficients

plot(t, pval, 'k')

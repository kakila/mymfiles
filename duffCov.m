## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-

#clear all;
#pkg load odepkg

g = 2;
d = 10;
k = 100;

N = 100;
x0 = randn (2,N);
nT = 500;
t = linspace(0,5,nT).';
dt = t(2)-t(1);

if regen

  function dx = duff(x,t,k,g,d,f=[])
    dx = zeros(size(x));

    dx(1,:) = x(2,:);
    dx(2,:) = - ( k*(x(1,:) + g*x(1,:).^3) + d*x(2,:) );
    if !isempty(f)
     dx(2,:) += f(t);
    endif

  endfunction

  y = zeros (nT,2,N);
  Kr = zeros (nT,nT);
  mr = zeros (nT,1);
  dm = dKr = zeros (N,1);

  for i=1:N
    G = 10*sqrt(2)*randn(10,1);
    u =@(x) sin(pi*(1:length(G)).*x/t(end)) * G;

    [t,y(:,:,i)] = ode45 (@(t,x)duff(x,t,k,g,d,u),t,x0(:,i));
    r = squeeze (y(:,1,i));
    
    mrn = (mr*(i-1) + r) ./ i;
    dm(i) = sqrt(sumsq (mrn-mr));
    mr = mrn;
    
    Krn = (Kr.*(i-1) + (r - mr) * (r-mr).') ./ i;
    dKr(i) = norm (Krn-Kr);
    Kr = Krn;
  end

  r = squeeze (y(:,1,:));
  v = squeeze (y(:,2,:));
endif

if test
  dimX = 1;

  # Conform mean matrix
  # ??
  # Sample
  n = 10;
  r_gp = mvnrnd (mr,Kr,n)';

  # Re-arrange
  if dimX != 1
    tmp = r_gp;
    r_gp = zeros (nT,dimX,n);
    for i = 1:dimX
      r_gp(:,i,:) = tmp(i:dimX:end,:);
    endfor
  endif

  # Simulate with same inital conditions
  y = zeros (nT,2,n);
  for i=1:n
    v = (r_gp(2,i) - r_gp(1,i)) / dt;
    [t,y(:,:,i)] = ode45 (@(t,x)duff(x,t,k,g,d),t,[r_gp(1,i) v]);
  endfor
  r = squeeze (y(:,1,:));
  v = squeeze (y(:,2,:));

  # Compute error
  ERR = sqrt (sumsq (r-r_gp))/nT;
endif



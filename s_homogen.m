## Copyright (C) 2015 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

if ~exist('N','var')
  N = 2;
endif

if ~exist('nT','var')
  nT = 10;
endif

# n > 0
n = logspace (-log10(3),-log10(0.5),N).';
# t > 0
t = linspace (0,1,nT).';

# Sol dx/dt = u.^2
x = - 1 ./ (n.' + t);

idx = nchoosek (1:N,2);
Ni = size(idx,1);
B = zeros (nT, Ni);

for i = 1:Ni
  B(:,i) = prod(x(:,idx(i,:)),2);
endfor

plot (t,B)

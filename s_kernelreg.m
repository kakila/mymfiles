## Copyright (C) 2014 Juan Pablo Carbajal
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <juanpi@AsusK55VJ-ubuntu>
## Created: 2014-09-16


%% Copyright (c) 2012 Juan Pablo Carbajal <carbajal@ifi.uzh.ch>
%%
%%    This program is free software: you can redistribute it and/or modify
%%    it under the terms of the GNU General Public License as published by
%%    the Free Software Foundation, either version 3 of the License, or
%%    any later version.
%%
%%    This program is distributed in the hope that it will be useful,
%%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%    GNU General Public License for more details.
%%
%%    You should have received a copy of the GNU General Public License
%%    along with this program. If not, see <http://www.gnu.org/licenses/>.

if ~exist("redo")
  redo=true;
endif
if redo
  pkg load drd
  clear all
  close all
  ## Construction
  M = dynsys();
  %% ------ Mechanical system ------ %%
  % Dimension of the configuration space.
  % Order of ODE
  % ODE
  function u = D(q,k,d)
    u = q{3} + polyval([k 0],q{1}) + d*q{2};
  end
  w0   = 3*2*pi;
  k    = w0^2*[-10 0 1];
  #k    = w0^2*[-0.8 0 1];
  damp = 2*w0*0.3;
  M    = set(M,'IEM', @(q)D(q,k,damp), 'Qdim', 1, 'Dorder', 2);

  % Dynamical system
  function [dotx varargout] = duffing (t,x,px,pv,f)

    coef      = -[px 0];
    damp      = -[pv 0];
    [nT nP]   = size (x);
    dotx      = zeros (nT,nP);
    dotx(1,:) = x(2,:);
    ac        = polyval (coef, x(1,:)) + polyval (damp, x(2,:));
    F         = zeros (1, size (x,2));

    if !isempty (f)
       if !isnumeric (f);
         F = f (t).';
       end
    end
    dotx(2,:) =  ac + F;
  end

  F = @(t,x,f)duffing(t,x,k,damp,f);
  M = set(M,'EEM',F,'Sdim',2);

  ## Apendding synergies
  for i=0.5:0.1:1.5
    f = @(t)3.5*sin(i*w0*t);
    M = appendSynergy(M,f);
  end

  t = linspace (0,1,100).';

  ## Getting synergy
  Synergy = get (M, 'Synergy');
  Phi     = squeeze(getSynergySampled(M,t));

  ## Generating responses
  nSyn = numel (Synergy);
  s0 = {0.1*randn(nSyn,1),0.5*randn(nSyn,1)};
  M = genResponse (M,s0,t);

  ## Getting responses
  DR      = get (M, 'DR');
  Theta   = squeeze(sampled (DR, t));

  # The exploration corresponds to the following tasks
  z = [Theta(1,:,1).' Theta(end,:,1).' Theta(1,:,2).' Theta(end,:,2).'];
  # With synergy combinators
  w = eye(nSyn,nSyn);

  # Solve a couple of examples
  #neg = 2; 
  #x_0 = randn (neg,1);
  #x_T = randn (neg,1);
  #z = [x_0 x_T];
  #%% Options for YALL1
  #addpath("~/Resources/YALL1_v1.4");
  #opts.tol     = 1e-3;  % Stopping tolerance
  #opts.rho     = 1e-3;  % L2 norm for the error is weighted with 1/(2*rho)
  #opts.nonorth = 1;     % Design matrix is non orthogonal
  #opts.print   = 0;       % No verbose
  #w = zeros (ndim,neg);
  #for i = 1:neg
  #  w(:,i) = yall1 (Theta([1 end],:), z(i,:).', opts);
  #endfor
  redo = false;
endif
# Do kernel regression
#dist2=@(z,x) sum(reshape(cell2mat(arrayfun(@(i)(z(:,i)-x(:,i).').^2, 1:size(x,2),"unif",0)),size(z,1),size(x,1),size(x,2)),3);
#K    =@(z,x) exp (-dist2(z,x)/2/1e3^2);
#Kzz  = K(z,z);
#a    = inv (Kzz + sqrt(eps)*eye(size(Kzz))) * w.';

K    =@(z,x) z*x.';
Kzz  = K(z,z);
a    = inv (Kzz + sqrt(eps)*eye(size(Kzz))) * w.';

# Predict new combination
n_test = 20;
amp = 0.9*max(roots ([k,0]));
z_ = [std(z(:,1)).*randn(n_test,1) amp*linspace(-1,1,n_test).' std(z(:,3)).*randn(n_test,1) zeros(n_test,1)];
w_ = a.'*K(z,z_);

## Simulation with solution
warning ("off","OdePkg:InvalidArgument");
figure (1)
clf
hold on
col = jet (n_test);
Err = inf (n_test,2);
for i=1:n_test
  f       =  data2fun (t,Phi*w_(:,i));
  try
    [tf yf] = sim (M, t([1 end]), z_(i,[1 3]),'actuation',f);
    h = plot(tf,yf{1},t([1,end]),z_(i,1:2),'.');
    set(h,"color",col(i,:));
    Err(i,1) = sqrt((yf{1}(end)-z_(i,2))^2);
    Err(i,2) = sqrt((yf{2}(end)-z_(i,4))^2);
    z_new(i,:) = [yf{1}(1) yf{1}(end) yf{2}(1) yf{2}(end)];
  catch
    fprintf("ups!\n"); fflush(stdout);
  end
endfor
axis tight



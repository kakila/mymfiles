## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2013 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {@var{} =} reservoir (@var{}, @var{})
##
## @seealso{}
## @end deftypefn

function dxdt = reservoir (x, t, p, JAC=0)
  if !JAC
    dxdt = (tanh (p.epsilon*p.Ws*x + p.kappa*p.Wi*p.input(t) + p.bias) - p.alpha*x) / p.tau;
  else
    dxdt = 0*(( 1 - tanh (p.epsilon*p.Ws*x + p.kappa*p.Wi*p.input(t) + p.bias).^2 ) .* full(p.epsilon*p.Ws) - ...
           p.alpha*eye (length (x)) ) / p.tau;
  endif

endfunction

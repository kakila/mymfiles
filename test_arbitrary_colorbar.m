## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

%% Check octave/3.6.1/m/plot/waitbar.m
% For fancy creation of axes
close
h = figure(1);

% do an histogram
[c x] = hist(randn(100,1));
subplot(10,1,[1 9])
xh = bar(x,c);

% get the axes
bx = get(xh,'parent');

set(bx,'xticklabel',[]);
% get sizes and pos
bar_pos = get(bx,'position');
xlim = get(bx,'xlim');
xtick = get(bx,'xtick');

% Colorbar
ax = axes ("parent", h, "xlimmode", "manual", "ylimmode", "manual",
      "position", [bar_pos(1) bar_pos(2)-bar_pos(4)/9 bar_pos(3) bar_pos(4)/9]);

%% Got this from octave/3.6.1/m/plot/colorbar.m
% Look there to see how to connect properly to the parent figure
clen = rows (get (h, "colormap"));
cext = xlim;
cdiff = (cext(2) - cext(1)) / clen / 2;
cmin = cext(1) + cdiff;
cmax = cext(2) - cdiff;

hi = image (ax, [cmin, cmax], [0,1], [1 : clen]);
set (ax, "xtick", xtick, "ytick", []);

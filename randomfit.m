## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2013 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
Fbasis =@(t,w) [sin(2*pi*w.*t) cos(2*pi*w.*t)];
Lbasis =@(t,n) cell2mat(arrayfun (@(x)legendrepoly(x,t),n,"unif",0));
Hbasis =@(t,n) cell2mat(arrayfun (@(x)hermitepoly(x,t),n,"unif",0));

randn ("state",1:625);
nT  = 1e3;
u = linspace (-1,1,nT)';
u = mvnrnd (zeros (1,nT), exp (-((u-u.')/0.05).^2)).';
u = u/max(abs(u));
z = u.^2;

N = 2:20;
hz = randn(1,floor(max(N)/2));

nN = length (N);
co = zeros (nN,10);
er = inf (nN,10);
for i=1:nN

  #X = Fbasis (u,hz(1:floor(N(i)/2)));
  #X = Lbasis (u,0:N(i)-1);
  #X = Hbasis (u,0:N(i)-1);
  if i==1
    X = mvnrnd (zeros (max(N),nT), exp (-((u-u.')/0.5).^2)).';
  endif

  for j=1:20
    [~,iM] = max(u);
    [~,im] = min(u);
    tr = [randperm(nT, N(i)-2) im iM];
    a  = X(tr,1:N(i)) \ z(tr);
    y  = X(:,1:N(i))*a;

    co(i,j) = corr (y,z);
    er(i,j) = sqrt ( sumsq (y-z) / sumsq (z));
  endfor

endfor

figure (1)
[su ind] = sort(u);
plot (su,z(ind),'-k', ...
      su,y(ind),'-r', ...
      u(tr),y(tr),'.g');
axis ([min(u) max(u) min(z) max(z)]);

## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2017-12-08

pkg load gpml

## create actual function
covfunc =@(x,y) feval (@covSEiso, [log(1e-1);0],x,y);
pp = [2 0 -1 0.5];
af = @(u) polyval (pp, u);
ff = @(t) arrayfun(@(r)quadgk (@(u)covfunc(u(:),r) .* af(u),-1,1),t);
C  = quadgk (ff, -1,1);

## Create data
ti = sort (2 * rand (100, 1) - 1);
fi = ff (ti);
Ci = trapz (ti, fi);

## compute normal
phi = @(u,n) u.^n;
ku  = @(u)arrayfun (@(z)quadgk (@(t) covfunc(t(:),z), -1, 1),u);
clear kp
for n = 5:-1:0;
  kp(n+1) = quadgk (@(u)ku(u).*phi(u,n),-1,1);
endfor
kpn = sqrt (sumsq (kp));
e   = kp.' / kpn;
eT  = null (e.');
ppi0 = Ci * e;

af0 =@(t,s)polyval((ppi0+eT*s).',t);
f0  =@(t,s) arrayfun(@(r)quadgk (@(u)covfunc(u(:),r) .* af0(u,s),-1,1),t);

# fit
s0 = zeros (size (eT,2),1);
cost =@(s) sumsq (f0(ti,s) - fi);
so = fminsearch (cost,s0);
figure (1)
clf
fplot(ff,[-1,1]); hold on; fplot(@(t)f0(t,so),[-1,1]); plot(ti,fi,'o');hold off

figure (2)
clf
fplot(af,[-1,1]); hold on; fplot(@(t)af0(t,so),[-1,1]);


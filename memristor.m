## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function [V M h0 h1] = memristor (t,p)
  # We make a stable equilibrium in 10/mu which gives a saturated memristor.
  # Input is current
  
  h0 = p.xs + (p.x0 - p.xs).*exp(-p.l.*t);
  h1 = h0;
  m = size(h1,2);
  if m > 1 && length(p.l)==m
    for i=1:m
      h1(:,i) = pararrayfun (7,@(y)quadgk (@(z)p.input(z).*exp(p.l(i).*(z-y)), 0,y), t);
    endfor
  else
    h1 = pararrayfun (7,@(y)quadgk (@(z)p.input(z).*exp(p.l*(z-y)), 0,y), t);
  endif
  h = h1+h0;
  M = p.r + (p.R-p.r).*(tanh (p.mu.*h/2) + 1)/2;
  V = p.input(t) .* M;
endfunction

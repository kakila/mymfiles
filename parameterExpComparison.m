## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

%% comparison of threeways to explore parameters value
nT = 1000; %Time samples
t = linspace(0,1,nT)';
N = 5
Wsize = logspace(1,3,N);

loop = zeros(N,1);
extprod = zeros(N,1);
kronecker = zeros(N,1);

%% load function
kron(0,0);

for iSize = 1:N
    nW = Wsize(iSize);
    
    w = linspace(-pi/2,pi/2,nW);

    wt = zeros(nT,nW);

    %% Loop
    tic
    for i=1:nW
        wt(:,i) = w(i)*t;
    end
    loop(iSize) = toc;

    %% External product
    tic
    wt = t*w;
    extprod(iSize) = toc;

%% Kronecker product
    tic
    wt = kron(t,w);
    kronecker(iSize) = toc;
    
end    
plot(Wsize,loop,Wsize,extprod,Wsize,kronecker)
legend('Loop','External product','Kronecker product')
xlabel('Size of input [nW]')
ylabel('Time [s]')


## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

function ax = hydroplot (t, h, tr, r=[], N=[], pp = 1.5)

  if isempty (r)
    r  = tr;
    tr = t;
  endif

  ax = plotyy(t,h,tr,r);

#  nh = size (h, 2);
#  ax = plotyy(t,h(:,1),tr,r);
#  if nh > 1
#   plot(ax(1), t,h(:,2:end));
#  endif

  set (ax(1), "ycolor","k");

  set (ax(2),"ydir", "reverse");
  set (ax(2), "ycolor","c");
  hold (ax(2), "on")

  if isempty (N)
    N =  length (tr); %round (length(tr)*0.2);
  endif

  tt = linspace(tr(1), tr(end), N);
  rr = interp1 (tr,r, tt,"previous");
  p = bar (ax(2), tt, rr, 0.9);
  set (p, "facecolor", "c", "edgecolor", "none")
  hold (ax(2), "off");
  set (ax(2), "ylim",[0 pp*max(rr)]);
  delete (get(ax(2),"children")(2));

  set (ax, "color", "none");

  axes (ax(1));

endfunction

## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

%% Copyright (c) 2012 Juan Pablo Carbajal <carbajal@ifi.uzh.ch>
%%
%%    This program is free software: you can redistribute it and/or modify
%%    it under the terms of the GNU General Public License as published by
%%    the Free Software Foundation, either version 3 of the License, or
%%    any later version.
%%
%%    This program is distributed in the hope that it will be useful,
%%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%    GNU General Public License for more details.
%%
%%    You should have received a copy of the GNU General Public License
%%    along with this program. If not, see <http://www.gnu.org/licenses/>.

%% Script to test whether max-min is a better estimation of the width
%% of a gaussian distribution in low sampling situations
std_gauss = 0.1;
% Generate many low sample experiments
nsamp = 3; % number of samples per experiment
samp = std_gauss*randn(10e3,nsamp)+0.5;
% Distort
distort = false;
if distort
  samp .*= (abs(samp-min(samp(:)))./(max(samp(:))-min(samp(:)))).^2;
  samp = samp /(max(samp(:))-min(samp(:)));

  figure (1);
  [c x] = hist(samp(:),100);
  bar (x,c);
  axis tight
  v = axis ();
  [~,m] = max (c);
  m = x(m);
  line(m([1 1]),v(3:4),'color','r','linewidth',2);
  hold on
  h = plot(0,0,'or');
  set(h,'visible','off');
  disp('Mark with the mouse the dispersion of the values.')
  fflush(stdout);
  val = [];
  yval = [];
  while length(val) < 2
    [x y] = ginput(1);
    val = [val x];
    yval = [yval y];
    set(h,'xdata',val,'ydata',yval);
    set(h,'visible','on');
  end
  hold off
  clf
  val = sort(val);
  std_real = (max(val)-min(val))/2;
else
  std_real = std_gauss;
end

minmax = (max(samp,[],2) - min(samp,[],2))/2;
std_est = std(samp,[],2);

% Plot
boxplot ({minmax,std_est});
axis tight
v = axis ();
line(v(1:2),std_real([1 1]),'linewidth',2);
set(gca,'xtick',[1 2],'xticklabel',{'minmax','std'});

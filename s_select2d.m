criteria = @(X,a,b,c,d) (a <= X(:,1) & X(:,1) < b) & (c <= X(:,2) & X(:,2) < d);
M = rand (1e4, 2);
np = 60;
p = linspace (0, 1, np);

tic
[A C] = meshgrid (p(1:end-1));
[B D] = meshgrid (p(2:end));
a = A(:).'; b = B(:).'; c = C(:).'; d = D(:).';

tf = criteria(M,a, b, c, d);
# Slicing will give different sizes in general
npar = size (tf, 2);
M_selected = cell(npar, 1);
for i=1:npar
  M_selected{i} = M(tf(:,i),:);
endfor
toc

tic
M_selected2 = cell (np-1, np-1);
for i=1:np-1
  for j=1:np-1
    tf = criteria (M,p(i),p(i+1),p(j),p(j+1));
    M_selected2{j,i} = M(tf,:);
  endfor
endfor
toc

success = cellfun(@(x,y) all((x==y)(:)), M_selected, M_selected2(:));
all (success)

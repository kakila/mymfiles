## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>


function [Kiu Kii Kvu Kvv NC_ii] = kirchhofmat (net)

  n = size (net.KCL,2);
  
  # Input matrices
  idx   = net.sources;        # select source edges
  ns    = length (idx);
  Kiu   = net.KCL(:,idx);
  Kvu   = net.KVL(:,idx);

  # Internal matrices
  idx   = setdiff (1:n,idx);  # select internal edges
  Kii   = net.KCL(:,idx);
  Kvv   = net.KVL(:,idx);
  
  # Nullities
  NC_ii = null (net.KCL)(idx,:);

endfunction

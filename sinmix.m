## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {[@var{w},@var{Iw},@var{Iwe}] =} sinmix (@var{t}, @var{N}=50, @var{A}=[], @var{l}=1,@var{notrend}=true)
## supperposition of sines with @var{N} half integer frequencies.
## @end deftypefn

function [w Iw Iwe] = sinmix (t, N=50, A=[], l=1,notrend=true)
 
  f = (1:N) - 0.5;
  if notrend
   f += 0.5;
  endif

  omega = pi*f;
  omeg2 = omega.^2;
  if isempty(A)
   A     = randn (N,1);
  endif
  T = t(end,:);

  s     = sin (omega.*t) ./ omega;
  c     = cos (omega.*t) ./ omega;

  avgW  = 0;
  if notrend
   avgW    = ( 1 - cos (omega.*T) ) ./ omeg2;
  endif

  w  = ( s - avgW ) * A;
  Iw = Iwe =  zeros (size (w));
  Iw = ( (1 - c) ./ omega - t .* avgW ) * A;
  Iwe = ( ( s + omega .* ( exp (-l*t) - c ) ) ./ (omeg2 + 1) + avgW .* exp (-l*t) - 1 ) * A;
endfunction

## An example of correlations
#
##

## Copyright (C) 2019 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2019-04-01

pkg load signal

## Signals
# Correlation function: what info does it provide?
#
N    = 512;
t    = linspace (0, 1, N).' * 2 * pi;
dt   = t(2) - t(1);
freq = 3;
x    = sin (freq * t);
y    = x.^2;

lag = (0:N).';
# Correlation coefficient on of x(t) vs. y(t+dt)
C   = arrayfun (@(i)corr (x, circshift (y, i)), lag);

##
# Find 1st peak
#
opt = {'MinPeakDistance', 3};
[~, imax] = findpeaks (abs (C), opt{:});
lagmax    = lag(imax);
Cmax      = C(imax);

ydelay      = circshift (y, lagmax(1));

printf ('Maximum of correlation function: %.3f @ %.3f\n', ...
  Cmax(1), dt * lagmax(1));

figure (1), clf
  subplot (1,2,1)
  plot (lag * dt, C);
  axis tight
  grid on
  line (dt * [lagmax lagmax].', ylim, 'color', 'k');
  xlabel ('Delay')
  ylabel ('Correlation')
  subplot (1,2,2)
  plot (t, x, '-;x;', t, y, '-;y;', t, ydelay, '-;y delay;');
  ylabel ('Signal')
  xlabel ('Time')
  axis tight
  grid on

figure (2), clf
  subplot (1, 2, 1)
  plot (x, y, 'o')
  xlabel ('x')
  ylabel ('y');
  grid on
  axis tight
  subplot (1, 2, 2)
  plot (x, ydelay, 'o')
  xlabel ('x')
  ylabel ('y delay');
  grid on
  axis tight

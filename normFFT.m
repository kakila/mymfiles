## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (c) 2012 Juan Pablo Carbajal <carbajal@ifi.uzh.ch>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {[@var{uh} @var{f} @var{uhfull}] =} normFFT (@var{t}, @var{u})
## Computes the normalized FFT of @var{u} sampled on the uniform time interval @var{t}.
##
## The FFT in @var{uh} is normalized and clipped to the Niquist frequency. The
## vecotr of frequencies is given in @var{f}. The third output argument contains
## the full FFT without clipping.
##
## The Normalization is such that @code{sum (abs (@var{uh}).^2) == sum (abs(u).^2)*dt}.
##
## @end deftypefn
function [Uhatc f Uhat] = normFFT(t,u)

  L    = length(u);
  NFFT = 2^nextpow2(L); % Next power of 2 from length of u

  Uhat  = fft(u,NFFT)/L;
  Uhatc = Uhat(1:NFFT/2+1,:);

  Fs = 1./(t(2,:)-t(1,:));
  f  = (Fs/2).*linspace(0,1,NFFT/2+1)';

endfunction

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {@var{v} =} spike2cont (t, @var{spktmps},@var{spkpol}=1, @var{spkid}=[])
## Transform a matrix of spikes into continuous signals.
##
## This is an offline method. 
## @var{spktmps} contains the timestamps of the spiking events (integer, 1us tick).
## @var{spkpol} indicates the polarity of the spike (1 or -1).
## @var{spkid} indicates the id of the spike in cases there are multiple sources.
## @end deftypefn

function [v, count] = spike2cont (t, spktmp, spkpol = 1, spkid=[])

  ## Make sure t is horizontal
  flag_transposed = false;
  if iscolumn (t)
    t               = t.';
    flag_transposed = true;
  endif
  [ncol nrow]     = size (t);

  ## number of spikes
  nS = size (spktmp, 1);

  ## number of spike sources
  ids = unique (spkid);
  nSS = length (ids);

  tt = spktmp*1e-6;
  if nSS > 1
    tt     = arrayfun (@(x)tt(spkid==x), ids, "unif", 0);
    if length (spkpol) > 1
      pol  = arrayfun (@(x)spkpol(spkid==x), ids, "unif", 0);
      v    = cellfun (@(x,y)func(t,x,y), tt, pol, "unif", 0);
    else
      v    = cellfun (@(x)func(t,x,spkpol), tt, "unif", 0);
    endif
    v      = cell2mat (v);
  else
    v = func (t, tt, spkpol);
  endif
  
  if flag_transposed
    v = v.';
  endif

endfunction

function v = func (t,tt,pol)

  ## Compute arguments of each sigmoid
  RC   = 2e-3;
  DUR  = 2e-2;
  a_up = (tt - t)./RC;
  a_dw = (tt+DUR - t)./RC;

  ## Evaluate the sigmoids and mix them
  v = pol .* (1 ./ ( 1 + exp (a_up) ) .* (1 - 1 ./ ( 1 + exp (a_dw) ) ));
  if size(v,1) > 1
    v = sum (v);
  endif

endfunction

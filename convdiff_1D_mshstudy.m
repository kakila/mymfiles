pkg load bim

a  = 1e-4;
b  = 100;
Yo = 1;

# Initial condition
Y0 = @(x) Yo * ones (size (x));

# Sources
c = 10;
f = @(x)Yo + (c - Yo) * exp(-(x-0.5).^2 / 2 / 0.05^2);

# Dynamic equations
function dY = dYdt(Y, t, b, A, msh, f)
 dY      = f - A * Y;
 dY(1)   = 0;
 dY(end) = - b * (Y(end) - Y(end-1)) / (msh(end) - msh(end-1));
endfunction

# Meshes
Nt     = 10;
t      = linspace (0, 10, Nt);
msh{1} = linspace (0, 1, 50).';
msh{2} = linspace (0, 1, 100).';

# Matrices & solution
Nmsh = numel(msh);
for i = 1:Nmsh
  Diff     = bim1a_laplacian (msh{i}, 1, a);
  Conv     = bim1a_advection_upwind (msh{i}, (b / a) * msh{i});
  Reaction = bim1a_reaction (msh{i}, 1, 1);
  Source   = bim1a_rhs (msh{i}, 1, f(msh{i}));

  A = Diff + Conv + Reaction;

  printf ('Solving %d\n', i); fflush (stdout);
  lsode_options ("integration method", "adams");
  #lsode_options ("maximum step size", min(diff(msh{i})) / (b*1.05));

  Y{i} = lsode (@(Y, t) dYdt(Y, t, b, A, msh{i}, Source), Y0(msh{i}), t);
endfor # over meshes

figure (1)
col = flipud (gray (Nt+3))(3:end,:); # colors for time
for i=1:Nmsh
  subplot (2, 1, i);
  h_ = plot (msh{i}, Y{i}.');

  for j=1:Nt
    set(h_(j), 'color', col(j,:));
  endfor # over colors

  ylabel (sprintf('Y%d',i))
endfor # over solutions
xlabel ('x')

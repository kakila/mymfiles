## Copyright (C) 2014 juanpi
##
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {@var{out} =} dampedspring (@var{x0},@var{v0},@var{f0},@var{w},@var{m},@var{k},@var{b})
## @deftypefnx {Function File} {@var{out} =} dampedspring (@var{x0},@var{v0},@var{f0},@var{w},@var{w0},@var{xhi})
##
## @end deftypefn

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2014-11-30

function out = dampedspring (x0,v0,f0,w,varargin)
  if nargin == 4 + 3
    m = varargin{1};
    k = varargin{2};
    b = varargin{3};
    wn  = sqrt (k./m);
    xhi = b ./ ( 2 * sqrt (k.*m) );
    f   = f0 ./ m;
  elseif nargin == 4+2
    wn  = varargin{1};
    xhi = varargin{2};
    f   = f0;
  endif
  wd  = wn .* sqrt (1 - xhi.^2);

  A = x0;
  B = (xhi .* wn .* x0 + v0) ./ wd;
  envelope = @(t) exp (-xhi .* wn .* t);
  oscillat = @(t) A .* cos (wd.*t) + B .* sin (wd.*t);

  x = @(t) exp (-xhi .* wn .* t) .* (A .* cos (wd.*t) + B .* sin (wd.*t));
  v = @(t) exp (-xhi .* wn .* t) .* ( ...
          (wd .* B - xhi .* wn .* A) .* cos (wd.*t) - ...
          (wd .* A + xhi .* wn .* B) .* sin (wd.*t));

  out = struct ("envelope", envelope, "oscillation", oscillat, ...
                "wn", wn, "xhi", xhi, "wd", wd, "x", x, "v", v);

  # Phase
  #  aux    = 2*xhi.*wn.*w;
  #  aux_2  = wn.^2 - w.^2;
  #  aux_3  = wn ./ wd;
  #  denom2 = (wn.^2 + w.^2).^2 + aux.^2
  #  denom  = sqrt (denom);

  #  C1  = x0 + f .* aux ./ denom2;
  #  C2  = v0./ wd + xhi .* aux_3 .* C1 - f .* aux_3 .* aux_2 ./ denom2

  #  phi = atan2 (C1, C2);
  #  psi = atan (aux, aux_2);

endfunction

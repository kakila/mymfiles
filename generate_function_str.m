## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

%% Copyright (c) 2011 Juan Pablo Carbajal <carbajal@ifi.uzh.ch>
%%
%%    This program is free software: you can redistribute it and/or modify
%%    it under the terms of the GNU General Public License as published by
%%    the Free Software Foundation, either version 3 of the License, or
%%    any later version.
%%
%%    This program is distributed in the hope that it will be useful,
%%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%    GNU General Public License for more details.
%%
%%    You should have received a copy of the GNU General Public License
%%    along with this program. If not, see <http://www.gnu.org/licenses/>.

%% -*- texinfo -*-
%% @deftypefn {Function File} {@var{str} = } generate_function_str (@var{name}, @var{oargs}, @var{iargs}, @var{bodystr})
%% Generates a string defining a function.
%%
%% @end deftypefn

function str = generate_function_str(name, oargs, iargs, bodystr)

  striargs = cell2mat ( cellfun (@(x) [x ", "], iargs, "UniformOutput", false));
  striargs = striargs(1:end-2);

  stroargs = cell2mat ( cellfun (@(x) [x ", "], oargs, "UniformOutput", false));
  stroargs = stroargs(1:end-2);

  if !isempty (stroargs)
    str = ["function [" stroargs "] = " name "(" striargs ")\n\n" bodystr ...
           "\n\nendfunction"];
  else
    str = ["function " name "(" striargs ")\n\n" bodystr ...
           "\n\nendfunction"];
  end

endfunction

%!test
%! body = sprintf("  str = %s;" , '"Hello world!\n"');
%! fstr = generate_function_str("helloworld",{"str"},{""},body);
%! eval(fstr);
%! assert("Hello world!\n", helloworld);

%!test
%! body = "  y = x + 1;\n";
%! fstr = generate_function_str("addone",{"y"},{"x"},body);
%! eval(fstr);
%! assert(1,addone(0));
%! assert(6,addone(5));

%!test
%! body = "  y = x * 2;\n";
%! fstr = generate_function_str("bytwo",{"y"},{"x"},body);
%!
%! if !exist("bytwo.m","file")
%!   fid = fopen("bytwo.m","w");
%!   fprintf(fid, "%s", fstr);
%!   fclose(fid);
%!
%!   assert(6,bytwo(3));
%!   delete("bytwo.m");
%! else
%!  warning("cannot save file")
%!  eval(fstr)
%!  assert(6,bytwo(3));
%! end

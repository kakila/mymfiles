## Copyright (C) 2020 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2020-01-10

pkg load bim

# Water
cp  = 1.91e3;  # J/kg/K
rho = 3.31e-1; # kg/m³
k   = 6e-1;    # J/s/m/K
h   = 40;      # J/s/m²/K  # We search for this value
L   = 28;      # m
v   = 20;      # m/s

Ti  = 1;      # inlet water temp
To  = 50;      # outlet water temp

# Mesh
Nx  = 50;
msh = linspace(0, 1, Nx).';
Omega = (2:Nx-1).';
Gamma = [1; Nx];
TGamma = [Ti; To];

Ts = 60;     # pipe surface temp
# Distribution of the surface temperature
#Tsf = @(x) (Ts - (Ti + To)/2) * exp (-(x-0.5).^2 / 2 / 0.1^2) + Ti + (To - Ti) * x;
#Tsf = @(x) Ti - (Ts - Ti) * expm1 (-x * log((Ti-Ts)/(To-Ts)));
Tsf = @(x) Ti + (Ts - Ti) * (tanh((x-0.1)/0.05) + 1) / 2;
TS  = Tsf(msh);


tau  = rho * cp * L^2 / k;   # characteristic diffusion time
a    = 1;                    # diffusion coef
b    = v * tau / L;          # convection coef
Diff = bim1a_advection_diffusion (msh, 1, 1, a, 0);
Conv = bim1a_advection_upwind (msh, b * ones (Nx, 1));
DC   = Diff + Conv;

# True values
c        = h / k * L^2;          # transfer coef (sources)
Reaction = bim1a_reaction (msh, 1, c);
Source   = bim1a_rhs (msh, 1, c .* TS);
A = DC + Reaction;
f = Source(Omega) - A(Omega, Gamma) * TGamma;
T = [Ti; A(Omega, Omega) \ f; To];

# Sample c
Nc = 25;
T_ = zeros (Nx, Nc);
T_(Gamma,:) = TGamma .* ones(1, Nc);
h_ = h .* logspace (-6, 1, Nc);
c_ = h_ / k * L^2;
for i = 1:Nc
  R_ = bim1a_reaction (msh, 1, c_(i));
  S_ = bim1a_rhs (msh, 1, c_(i) .* TS);

  A_ = DC + R_;
  f_ = S_(Omega) - A_(Omega, Gamma) * TGamma; # Can be precomputed: R_(Omega,Gamma) = 0
  T_(Omega,i) = A_(Omega, Omega) \ f_;
endfor

# Number of solution without much cooling
Tcrit = min (max (TGamma), Ts);
okcool = max (T_(1:end-1,:)) <= Tcrit;
Nok = sum(okcool);
col = flipud (autumn (Nok)); # colors for c for ok cool
colbw = gray (Nc - Nok); # colors for c
figure (1);
  clf;
  subplot(2,1,1)
  hold on
  hl = plot (msh, T_, '-', msh, T, '--k', msh, TS, ':b');
  i = iok = 0;
  for j=1:Nc
    #if (min (diff (T_(:,j)) >= 0))
    if (okcool(j))
        iok += 1;
        set(hl(j), 'color', col(iok,:));
    else
        i += 1; 
        set(hl(j), 'color', colbw(i,:));
    endif
  endfor # over colors

  plot (msh(Gamma), TGamma, 'o', 'markerfacecolor', 'auto');
  hold off
  axis tight
  ylabel ('Temp [K]')

  subplot(2,1,2)
  plot (msh, TS);
  axis tight
  ylabel ('T_s [K]')

  xlabel ('x/L [-]')

## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2009 Oliver Woodford
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {@var{map} =} colormap_helper (@var{map})
## @deftypefnx {Function File} {@var{map} =} colormap_helper (@var{map}, @var{len})
## @deftypefnx {Function File} {@var{b} =} colormap_helper (@var{map}, @var{a})
## @deftypefnx {Function File} {@var{b} =} colormap_helper (@var{map}, @var{a}, @var{lims})
## Helper function for colormaps
##
## Given a concise colormap table (i.e. one that contains all the
## information required to create a full colormap, without any redundancy),
## this function can return a colormap of the desired length, or convert a
## real-valued array into truecolor array using the colormap.
##
## @strong{Input}
## @table @var
## @item map
## K-by-J colormap table. @code{J == 3}, except in the non-linear case, when
## @code{J == 4}, the last column giving the relative sizes of the
## inter-color bins.
## @item len
## Scalar length of the output colormap. If @code{len == Inf} the concise
##         table of the colormap is returned. Default: len = size(get(gcf, 'Colormap'), 1);
## @item a
## Non-scalar numeric array of real values to be converted into truecolor.
## @item lims
## 1-by-2 array of saturation limits to be used on @var{a}.
## Default: @code{[min(@var{a}(:)) max(@var{a}(:))]}.
## @end table
##
## @strong{Output}
## @table @var
## @item map
## @var{len}-by-J colormap table. @code{J == 3}, except in the concise case, when
##         @code{J == 4}, the last column giving the relative sizes of the
##         inter-color bins.
## @item b
## @code{size(@var{a})}-by-3 truecolor array.
## @end table
##
## @seealso{colormap, real2rgb}
## @end deftypefn

## Ported to GNU Octave: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function map = colormap_helper(map, len, lims)

  if nargin < 2
     len = size (get (gcf, "Colormap"), 1);
  endif

  if isscalar (len)
      if len == Inf
          % Return the concise colormap table
          return
      endif

      len = 1:len;
      sz = numel (len);
      lims = [1 sz];

  else
      sz = size (len);
      if nargin < 3
          lims = [];
      endif

  end
  map = reshape (real2rgb (len(:), map, lims), [sz 3]);

endfunction

## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2009 Oliver Woodford
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn  {Function File} {@var{map} =} hicontrast ()
## @deftypefnx {Function File} {@var{map} =} hicontrast (@var{len})
## @deftypefnx {Function File} {@var{b} =} hicontrast (@var{a})
## @deftypefnx {Function File} {@var{b} =} hicontrast (@var{a},@var{lims})
## Black-blue-red-magenta-green-cyan-yellow-white colormap
##
## A colormap designed to maximize the range of colors used in order to
## improve contrast between intensity levels, while converting linearly to
## grayscale, such that black & white prints come out nicely.
##
## The function can additionally be used to convert a real valued array into
## a truecolor array using the colormap.
##
## @strong{Input}
## @table @var
## @item len
## Scalar length of the output colormap. If @code{len == Inf} the concise
##         table of the colormap is returned. Default: len = size(get(gcf, 'Colormap'), 1);
## @item a
## Non-scalar numeric array of real values to be converted into truecolor.
## @item lims
## 1-by-2 array of saturation limits to be used on @var{a}.
## Default: @code{[min(@var{a}(:)) max(@var{a}(:))]}.
## @end table
##
## @strong{Output}
## @table @var
## @item map
## @var{len}-by-J colormap table. @code{J == 3}, except in the concise case, when
##         @code{J == 4}, the last column giving the relative sizes of the
##         inter-color bins.
## @item b
## @code{size(@var{a})}-by-3 truecolor array.
## @end table
##
## @seealso{colormap}
## @end deftypefn

## Ported to GNU Octave: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## PKG_ADD: colormap ("register", "hicontrast");
## PKG_DEL: colormap ("unregister", "hicontrast");

function map = hicontrast(varargin)

  map = [0 0 0 114; 0 0 1 185; 1 0 0 114; 1 0 1 174;...
         0 1 0 114; 0 1 1 185; 1 1 0 114; 1 1 1 0];
  map = colormap_helper(map, varargin{:});

endfunction

## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2009 Oliver Woodford
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {@var{b} =} real2rgb (@var{a}, @var{cmap})
## @deftypefnx {Function File} {@var{b} =} real2rgb (@var{a}, @var{cmap}, @var{lims})
## @deftypefnx {Function File} {[@var{b}, @var{lims}, @var{map}]=} real2rgb (@dots{})
## Converts a real-valued matrix into a truecolor image
##
## This function converts a real-valued matrix into a truecolor image (i.e.
## double array with values between 0 and 1) using the colormap specified
## (either user-defined or the name of a colormap function). The output
## image is suitable for display using IMAGE or IMSHOW, exporting using
## IMWRITE, texture mapping a surface etc.
##
## Colormaps specified by name, e.g. 'hot', can be reversed ('-hot'), made
## to convert linearly to grayscale when printed on a black & white printer
## ('hot*'), or both ('-hot*').
##
## Value limits and a colormap table can be output, for generating the
## correct colorbar, e.g.:
## @example
##   [B lims map] = real2rgb (peaks (256), '-hot*');
##   hIm = imshow (B);
##   set (gcf, "Colormap", map);
##   set (gca, "CLim", lims);
##   set (hIm, "CDataMapping", "scaled");
##   colorbar;
## @end example
##
## @strong{Input}
## @table @var
## @item a
## a M-by-N real matrix.
## @item cmap
##  J-by-K user-defined colormap, or a string indicating the name
##  of the colormap to be used. K == 3 or 4. If K == 4 then
##  @code{@var{cmap}(1:end-1,4)} contains the relative widths of the bins between
##  colors. If @var{cmap} is a colormap function name then the prefix @asis{'-'}
##  indicates that the colormap is to be reversed, while the suffix
##  @asis{'*'} indicates that the colormap bins are to be rescaled so that
##  each bin produces the same change in gray level, such that the
##  colormap converts linearly to grayscale when printed in black
##  and white.
## @item lims
## 1-by-2 array of saturation limits to be used on @var{a}.
## Default: @code{[min(@var{a}(:)) max(@var{a}(:))]}.
## @end table
##
## @strong{Output}
## @table @var
## @item b
## M-by-N-by-3 rgb image.
## @item lims
## 1-by-2 array of saturation limits used on @var{a}. Same as input lims, if given.
## @item map
## 256-by-3 colormap similar to that used to generate @var{b}.
##
## @end table
## @seealso{colromap}
## @end deftypefn

## Ported to GNU Octave: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function [B lims map] = real2rgb(A, cmap, lims)

  # Thank you to Peter Nave for reporting a bug whereby colormaps larger than
  # 256 entries long are returned.

  # Don't do much if A is wrong size
  [y x c] = size (A);
  if c > 1
      error ("Octave:invalid-input-argument","real2rgb: A can only have 2 dimensions");
  endif

  if y*x*c == 0
      # Create an empty array with the correct dimensions
      B = zeros (y, x, (c != 0) * 3);
      return
  endif

  # Generate the colormap
  if ischar (cmap)

      # If map starts with a '-' sign, invert the colormap
      reverseMap = cmap(1) == '-';

      # If the map ends with a '*', attempt to make map convert linearly to
      # grayscale
      grayMap = cmap(end) == '*';

      # Extract the map name
      cmap = lower (cmap(reverseMap+1:end-grayMap));

      # Load the map
      try
          # Check for a concise table first
          map = feval (cmap, Inf);
      catch
          map = [];
      end_try_catch

      if invalid_map (map)

          try
              # Just load a large table
              map = feval(cmap, 256);
          catch
              error("real2rgb: Colormap '#s' not found", cmap);
          end_try_catch

          if invalid_map(map)
              error("Octave:invalid-input-argument",
                                                  "real2rgb: Invalid colormap");
          endif

      endif

      if reverseMap
          # Reverse the map
          map = map(end:-1:1,:);

          if size (map, 2) == 4
              # Shift up the bin lengths
              map(1:end-1,4) = map(2:end,4);
          endif
      endif

      if grayMap && size (map, 1) > 2
          # Ensure the map converts linearly to grayscale
          map(1:end-1,4) = abs (diff (map(:,1:3) * [0.299; 0.587; 0.114]));
      endif

  else
      # Table-based colormap given
      map = cmap;
  endif

  # Only work with real doubles
  B = reshape (double (real (A)), y*x, c);

  # Compute limits and scaled values
  maxInd = 1 + (size (map, 1) - 2) * (size (map, 2) ~= 4);
  if nargin < 3
      lims = [];
  endif

  [B lims] = rescale (B, lims, [0 maxInd]);

  # Compute indices and offsets
  if size (map, 2) == 4
      # Non-linear colormap
      bins  = map(1:end-1,4);
      cbins = cumsum(bins);

      bins(bins==0) = 1;

      bins  = cbins(end) ./ bins;
      cbins = [0; cbins(1:end-1) ./ cbins(end); 1+eps];

      [ind ind] = histc(B, cbins);

      B = (B - cbins(ind)) .* bins(ind);
      clear bins cbins
  else
      # Linear colormap
      ind = min(floor(B), maxInd-1);
      B = B - ind;
      ind = ind + 1;
  end

  # Compute the output image
  try
      #B = bsxfun (@times, map(ind,1:3), 1 - B) + bsxfun (@times, map(ind+1,1:3), B);
      B = map(ind,1:3) .* (1 - B) + map(ind+1,1:3) .* B;
  catch
      # If no bsxfun
      B = B(:,[1 1 1]);
      B = map(ind,1:3) .* (1 - B) + map(ind+1,1:3) .* B;
  end_try_catch

  B = min (max (B, 0), 1); # Rounding errors can make values slip outside bounds
  B = reshape (B, y, x, 3);

  if nargout > 2 && (size (map, 1) ~= 256 || size (map, 2) == 4)
      # Generate the colormap (for creating a colorbar with)
      map = reshape (real2rgb (0:255, map, [0 255]), 256, 3);
  endif

endfunction

function notmap = invalid_map (map)

  notmap = isempty (map) || ndims (map) ~= 2 || size (map, 1) < 1 || ...
           size (map, 2) < 3 || size (map, 2) > 4 || ...
            ~all (reshape(map(:,1:3) >= 0 & map (:,1:3) <= 1, [], 1));

endfunction

%!demo
%!   [B lims map] = real2rgb (peaks (256), 'earth');
%!   hIm = imshow (B);
%!   set (gcf, "Colormap", map);
%!   set (hIm, "CDataMapping", "scaled");
%!   colorbar;

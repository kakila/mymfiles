##
# POD exploration
##

# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-08-20

nT  = 500;
t   = linspace (0,1, nT).';
dim = 2;

X       = randn (nT, dim);
X(:,2) += -X(:,1);

[V l B] = pod (X.', 2);
V = V.'; B = B.';

#W = randn(2)^2; W = (W  + W.')/2;
Q = orth (rand (dim));
W = Q * diag (eig (cov (X))) * Q.';
[Vw lw Bw] = pod (X.', 2, 'innerprod', W);
Vw = Vw.'; Bw = Bw.';

figure (1), clf
  subplot (1,2,1)
  hold on
  plot (X(:,1), X(:,2), 'ok');
  quiver (zeros (2,1), zeros (2,1), V(1,:).', V(2,:).', 'g-', 'linewidth', 2);
  quiver (zeros (2,1), zeros (2,1), Vw(1,:).', Vw(2,:).', 'c-', 'linewidth', 2);
  hold off
  axis tight
  axis equal
  
  subplot (1,2,2)
  plot (B(:,1), B(:,2), 'go', Bw(:,1), Bw(:,2), 'cs');
  axis tight


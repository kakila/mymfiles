## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2017-12-08

pkg load gpml
rand ('state',double('BohemianRapsodies2'));
randn ('state',double('BohemianRapsodies2'));

## Nodes
# Interval is [0,T]
T  = 1;
# tie vector
nT = 150;
t = linspace (0, T, nT).';
# inputs
nTi = 25;
ti = T * rand (1,nTi);
# weights
alpha = randn (nTi,1);
# function
s = 1e-1;
K_true =@(x,y=[]) covMaterniso(1,[log(s);0], x, y);
y = @(t) K_true(t,ti(:)) * alpha;
# Samples
yi = y(ti(:));
# Integral to be used as constraint
C = quadgk (y, 0, T);

figure(1)
fplot (y,[0,T]);
hold on
plot (ti,yi,'o');
hold off
axis tight

## Covariance function used for interpolation and its integral
# Assume we do not know the weights nor the cov function
#s = 3e-2;
#covf     = @(x,y=[]) covSEiso([log(s);0],x,y);
#int_covf = @(T,t) s * sqrt(pi/2) * ...
#                    (erf ( (T(2)-t)/sqrt(2)/s ) - erf (T(1)-t/sqrt(2)/s ));
s = 1e-1;
covf  = @(x,y=[]) covRQiso([log(s);0;log(0.005)],x,y);
int_covf = @(a,b) trapz (t, covf(t,ti(:)));

## Sample random functions with same integral
# 

# Integrated basis
kappa = int_covf([0,T],ti);
alpha_ = kappa \ C;
N      = null(kappa);
# Generate random functions
nF = 5;
alpha_rnd = alpha_ + 20* N * randn (size(N,2),nF);
y_ =@(t,i) covf(t(:),ti(:)) * alpha_rnd(:,i);
for i=1:nF
  C_(i) = quadgk (@(t)y_(t,i), 0, T);
endfor
disp ([C C_])

figure(2)
plot (t, y(t),'-;original;');
hold on
plot (t, y_(t,1:nF),'--');
plot (ti,[yi y_(ti,1:nF)],'o');
hold off
axis tight

## Sample random functions with same integral and values
# Assume we do not know the weights nor cov function
Kii = covf(ti(:));
Kti = covf(t, ti(:));
Ktt = covf(t, t);
Ki = cholinv (Kii);
Kn = Ktt - Kti * Ki * Kti.';
nu = trapz (t, Kn);
N  = null (nu);

%Kcii = [Kii zeros(size(Kii,1), length(nu)); kappa nu];
%ab   = Kcii \ [yi; C];
alpha_ = Ki * yi;% ab(1:nTi);
beta_  = nu \ (C - kappa * alpha_); %ab(nTi+1:end);
dY = Kn * (N * 20 * randn (size(N,2), nF));
Y0_ = Kti * alpha_ + Kn * beta_;
Y_ = Y0_ + dY;
for i=1:nF
  C_(i) = trapz (t, Y_(:,i));
endfor
disp ([C C_])

figure(3)
plot (t, y(t),'-;original;', ti, yi,'o');
hold on
plot (t, Y_,'--');
hold off
axis tight


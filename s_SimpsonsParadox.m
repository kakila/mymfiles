## Illustration of Simpson's paradox using Structural Causal Models
#
#
##

# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-08-29

## Parameters
#
nS = 1e3;   % number of samples

## Confounder case
#
Z = 2 * randi ([0 1], nS, 1) - 1;
X = 0.2 * randn (nS, 1) + 0.3 * Z;
Y = - X + 0.5 * randn (nS, 1) + 0.8 * Z;

corr (X, Y)
corr (X(Z==-1), Y(Z==-1))
corr (X(Z==1), Y(Z==1))

figure (1), clf
  subplot (2, 2, [1 2])
  subplot (2, 2, 3)
  plot (X, Y, 'ko');
  ylabel ('Y')
  xlabel ('X')
  axis equal
  subplot (2, 2, 4)
  scatter(X, Y, [], Z ,'filled');
  ylabel ('Y | Z')
  xlabel ('X | Z')
  axis equal

## Collider case
#
X = randn (nS, 1);
Y = randn (nS, 1);
Z = round (X + Y);
z = unique (Z); 
tf = Z == z(ceil(length(z)/2));

corr (X, Y)
corr (X(tf), Y(tf))

figure (2), clf
  subplot (2, 2, [1 2])
  subplot (2, 2, 3)
  plot (X, Y, 'ko');
  ylabel ('Y')
  xlabel ('X')
  axis equal
  subplot (2, 2, 4)
  %plot (X, Y, 'ko', X(tf), Y(tf), 'o', 'markerfacecolor', 'auto');
  scatter(X, Y, [], Z ,'filled');
  ylabel ('Y | Z')
  xlabel ('X | Z')
  colormap (jet)
  axis equal

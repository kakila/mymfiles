## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-11-09

function err = add1interperror (K, dK, x, y, method='linear', errfun=@meansq)
  n = length (K);
  m = length (dK);

  ydK   = interp1 (x(dK), y(dK), x, method);

  S      = zeros (m+1, 1);
  S(1:m) = dK;

  err = zeros (n, 1);
  for i=1:n
    S(end) = K(i);
    % change in interpolation residuals: (y_(k+1) - y) - (y_k - y)
    err(i) = errfun (interp1 (x(S), y(S), x, method) - ydK);
  endfor

endfunction


## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function [addr,t] = drifter2aer (x, varargin)
  # --- Parse arguments --- #
  parser = inputParser ();
  
  # validate device
  known_dev = {"dvs128"};
  chk_device =@(x) ismember (tolower(x),known_dev);
  
  parser.FunctionName = "drifter2aer";
  parser = addParamValue (parser,"Dev", "dvs128", chk_device);
  parser = addParamValue (parser,"FPS", 28, @(x)isscalar(x) && x >0);
  parser = addSwitch (parser,"OffEvents");

  parser = parse(parser,varargin{:});

  eval(sprintf("devmap = @bit_%s;",parser.Results.Dev));
  fps    = parser.Results.FPS;
  offevt = parser.Results.OffEvents;

  clear parser chk_*
  # ------ #

  [nT,dim,n] = size (x);
  
  [tmp,idx] = arrayfun(@(i)devmap(x(:,:,i)), 1:n,"unif",0);
  tmp       = cell2mat(tmp(:));

  # Generate timestamps
  nT       = length (tmp);
  duration = nT/fps * 1e6; # microseconds
  T        = round(linspace (0, duration, nT));
  T        = cell2mat (cellfun (@(x)T(x), idx, "unif", 0))(:);

  addr = tmp;
  t    = T;

  if offevt
    # Add flank down event
    addr = [addr; tmp+1];
    t    = [t; T+(1/fps)*1e6];
  endif
  
  [t, order] = sort (t, "ascend");
  addr       = addr (order);

endfunction

function [addr,idx] = bit_dvs128 (x)
  # map x in [0,1]² to [0,127]² and remove those outside
  x       = round (127 * x);
  # Original data has size(·,1) equal to time vector
  # Make a mask indicating which event did not occur
  tf      = x(:,1)>127 | x(:,2)>127;
  x(tf,:) = [];
  idx     = find (!tf);
  
  n    = size (x,1);
  z    = char (48 * ones (n,16));

  z(:,2:8)    = dec2bin(x(:,2),7); % y : bits 1-7
  z(:,9:15)   = dec2bin(127-x(:,1),7); % x : bits 8-14
  
  addr = bin2dec (z); 
endfunction

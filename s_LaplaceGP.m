## GP regression with Laplace prior
#
##

## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-11-14

## Dependencies
#
pkg load gpml
pkg load stk
## Synthetic data
#
if !exist ('X', 'var') || !exist ('y', 'var')
  N    = 100;
  dimX = 10;
  X    = stk_sampling_maximinlhs (N, dimX).data;
  % sparse mixer
  w_y     = zeros (dimX, 1);
  nz      = randperm (dimX, 2);
  w_y(nz) = 1;
  % output
  y_ = X * w_y;
  sy = 0.5;
  y  = y_ + sy * randn (N, 1);
endif


## Linear regression
#
wlin = X \ y;
ylin = X * wlin;

## GP regression
# We regress the data with the same model (linear) but different priors on the
# weights of the linear combination
#
meanf = {@meanLinear};
covf  = {@covZero};
likf  = {@likGauss};
inff  = @infGaussLik;
hyp0  = struct ('mean', zeros (dimX, 1), 'cov', [], 'lik', log (sy));
## 
# Gaussian prior distribution
priorG = struct ('mean', {cell(dimX, 1)});
sG2 = 5^2;
for i=1:dimX
  priorG.mean{i} = {@priorGauss, 0, sG2};
endfor
argsG = {{@infPrior, inff, priorG}, meanf, covf, likf, X, y};

hypG = minimize (hyp0, @gp, -500, argsG{:});
[yG dyG2] = gp (hypG, argsG{:}, X);

## 
# Laplace prior distribution
priorL = struct ('mean', {cell(dimX, 1)});
sL2 = 0.05^2; % this parameter controls sparseness
for i=1:dimX
  priorL.mean{i} = {@priorLaplace, 0, sL2};
endfor
argsL = {{@infPrior, inff, priorL}, meanf, covf, likf, X, y};

hypL = minimize (hyp0, @gp, -500, argsL{:});
[yL dyL2] = gp (hypL, argsL{:}, X);

## Summary of results
#
fmtstr      = @(n,m) repmat ([' %.2f\t' repmat('%.2f\t', 1, n) '\n'], m);
printresult = @(w) printf (fmtstr(columns (w), dimX), [w_y, w].');
printf ('Regression weights\n')
printf (' True \t\\ \tGuass \tLaplace\n')
printresult ([wlin, hypG.mean, hypL.mean]);
printf ('Correlation coefficient\n')
printf (' True \t\\ \tGuass \tLaplace\n')
printf (fmtstr(3, 1), corr (y, [y, ylin, yG, yL]));

## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {[@var{D1} @var{D2}] =} diffOP (@var{nX}, @var{dx}, @var{bc})
## Three points stencil for 1D 1st and 2nd numerical derivative.
##
## @var{D1} and @var{D2} contain the 1st and 2nd derivative operator, resp.
## The derivatives at the begining and at the end are extrapolated linearly
## if @var{bc} is @asis{"extrap"} (default) or extrapolated and averaged if it
## is @asis{"periodic"}.
## @end deftypefn

function [D1 D2] = diffOP (nX, dx, bcstr="eeee")

    SHIFT = diag (ones (nX-1, 1), -1);
    D{1} = sparse (SHIFT.' - SHIFT);
    D{2} = sparse ( SHIFT + SHIFT.' - 2 * eye(nX) );

    # Stencil for Boundary values
    bc{1,1} = [-2 1 2 -1]; # 1st deriv. left
    bc{1,2} = [1 -2 -1 2]; # 1st deriv. right

    bc{2,1} = [2 -5 4 -1]; # 2nd deriv. left
    bc{2,2} = [-1 4 -5 2]; # 2nd deriv. right

    col{1} = 1:4;
    col{2} = (nX-3):nX;

    if bcstr(1) == "p"

        c = [col{1} col{2}];
        D{1}(1,c) = D{1}(end,c) = [bc{1,1} bc{1,2}] / 2;
        D{2}(1,c) = D{2}(end,c) = [bc{2,1} bc{2,2}] / 2;

    else
      d_idx = [1 1 2 2];
      row{1} = 1;
      row{2} = nX;

      for i = 1:4                 # Loop over BC

        j   = d_idx(i);             # j-th Derivative
        k   = mod(i-1,2)+1;         # start or end position
        c   = col{k};               # column indexes
        r   = row{k};               #row indexes
        val = bc{j,k};              # stencil for BC

        switch bcstr(i)
          case "0"  # Null derivative
            D{j}(r,:) = zeros (1, nX);
          case "e"
            D{j}(r,c)         = val;

  #      case "extrap"
  #        D1(1,1:4)         = bc1_1;
  #        D1(end,end-3:end) = bc1_N;

  #        D2(1,1:4)         = bc2_1;
  #        D2(end,end-3:end) = bc2_N;

  #      case "periodic"
  #        D1(1,[1:4 end-3:end]) = D1(end,[1:4 end-3:end]) = [bc1_1 bc1_N] / 2;
  #        D2(1,[1:4 end-3:end]) = D2(end,[1:4 end-3:end]) = [bc2_1 bc2_N] / 2;
        endswitch
      endfor
  endif

    D1 = D{1} / dx / 2;
    D2 = D{2} / dx.^2;

endfunction

%!demo
%! nT = 50;
%! t  = 2 * pi * linspace (0,1,nT).';
%! dt = t(2) - t(1);
%! p = rand()*pi;
%!
%! x_p  = sin (t+p);
%! x    = (0.1*(t-p).^4 - 3*(t-p).^2) / 16;
%!
%! [D1_p D2_p] = diffOP(nT, dt, bc="pppp");
%! [D1 D2]     = diffOP(nT, dt);
%!
%! dx_p  = D1_p * x_p;
%! d2x_p = D2_p * x_p;
%! dx    = D1 * x;
%! d2x   = D2 * x;
%!
%! close all
%! figure(1)
%! h = plot(t,[cos(t+p) (0.4*(t-p).^3-6*(t-p))/16], t, [dx_p dx],'.');
%! legend (h(3:4),{'Periodic','Extrap'},'Location','NorthOutside','Orientation','Horizontal');
%! axis tight
%! figure(2)
%! h = plot(t,[-sin(t+p) (1.2*(t-p).^2-6)/16], t, [d2x_p d2x],'.');
%! legend (h(3:4),{'Periodic','Extrap'},'Location','NorthOutside','Orientation','Horizontal');
%! axis tight

%!demo
%! nT = 50;
%! t  = linspace (0,1,nT).';
%! dt = t(2) - t(1);
%! x    = exp (-10*t);
%! [D1 D2]     = diffOP(nT, dt,"e0e0");
%! dx    = D1 * x;
%! d2x   = D2 * x;
%!
%! close all
%! figure(1)
%! h = plot (t,[-10*x 100*x], t, [dx d2x],'.');
%! axis tight

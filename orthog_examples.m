## Copyright (C) 2008-2014 Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## 
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
## 
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
## 
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

%
% orthogonalization examples in 2D for illustrative purposes.
%
% svd, qr, and gram-schmidt.
%
% given any set of linearly independent vectors, find orthogonal basis.
%
% Originator: G.J. Hakim, University of Washington
%
% released under GNU General Public License version 3. http://www.gnu.org/licenses/gpl.html
%
% version control:
% $Date: 2009-01-27 22:40:05 -0800 (Tue, 27 Jan 2009) $
% $Revision: 32 $
% $Author: hakim $
% $Id: henon_lyap.m 32 2009-01-28 06:40:05Z hakim $

dim = 2; % 2 is good for plotting

x = rand(2,2); % indepenent vectors in columns
if cond(x) > 100
  stop
end

% svd
[u,s,v] = svd(x); 

% columns of u are the orthogonal basis for span{x_i}.
figure(1); clf
plot([0,x(1,1)],[0,x(2,1)],'b-','linewidth',2); hold on
plot([0,x(1,2)],[0,x(2,2)],'b--','linewidth',2);
plot([0,u(1,1)],[0,u(2,1)],'r-','linewidth',2); hold on
plot([0,u(1,2)],[0,u(2,2)],'r--','linewidth',2);
set(gca,'xlim',[-1.1 1.1],'ylim',[-1.1 1.1],'linewidth',2,'fontweight','bold')
axis square
title('Orthogonalization by SVD: blue: original vectors; red: U','fontweight','bold')
print -dpng orthog_svd.png

% QR
[q,r] = qr(x);

if r(1,1) < 0
  r = -r;
  q = -q;
end

% columns of q are the orthogonal basis for span{x_i}.
figure(2); clf
plot([0,x(1,1)],[0,x(2,1)],'b-','linewidth',2); hold on
plot([0,x(1,2)],[0,x(2,2)],'b--','linewidth',2);
plot([0,q(1,1)],[0,q(2,1)],'r--','linewidth',2); hold on
plot([0,q(1,2)],[0,q(2,2)],'r--','linewidth',2);
% replot first vector
plot([0,x(1,1)],[0,x(2,1)],'b-','linewidth',2); hold on
set(gca,'xlim',[-1.1 1.1],'ylim',[-1.1 1.1],'linewidth',2,'fontweight','bold')
axis square
title('Orthogonalization by QR: blue: original vectors; red: Q','fontweight','bold')
print -dpng orthog_qr.png

%{ 
Gram-Schmidt
y = gs(x);

figure(3); clf
plot([0,x(1,1)],[0,x(2,1)],'b-','linewidth',2); hold on
plot([0,x(1,2)],[0,x(2,2)],'b--','linewidth',2);
plot([0,y(1,1)],[0,y(2,1)],'r--','linewidth',2); hold on
plot([0,y(1,2)],[0,y(2,2)],'r--','linewidth',2);
set(gca,'xlim',[-1.1 1.1],'ylim',[-1.1 1.1],'linewidth',2,'fontweight','bold')
axis square
title('Orthogonalization by GS: blue: original vectors; red: Y','fontweight','bold')
print -dpng orthog_gs.png
%}

% original vectors alone
figure(4); clf
plot([0,x(1,1)],[0,x(2,1)],'b-','linewidth',2); hold on
plot([0,x(1,2)],[0,x(2,2)],'b--','linewidth',2);
set(gca,'xlim',[-1.1 1.1],'ylim',[-1.1 1.1],'linewidth',2,'fontweight','bold')
axis square
title('Original vectors');
print -dpng orthog_orig.png



## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Based on the algorithm by Jonas Lundgren

function z = hilbert_curve_nr (order, alloc=false)

  ####
  # This section generates the pseudo Hilbert curve of given order
  a = 1 + i; b = conj (a);

  if (alloc)
    # algo with allocation
    nz = 4 .^ [0:order];
    z  = complex (zeros (nz(end),1));
    for k = 1:order
      idx          = 1:nz(k);
      w            = i * conj (z(idx));
      z(1:nz(k+1)) = [w-a; z(idx)-b; z(idx)+a; -w+b] / 2;
    endfor

  else

    z = 0;
    for k = 1:order
      w = i * conj (z);
      z = [w-a; z-b; z+a; b-w] / 2;
    endfor

  endif

  z = z(:,end);
endfunction
